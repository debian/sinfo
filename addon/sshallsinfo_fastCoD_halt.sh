#! /bin/sh

for HOST in ` sinfo --filtermarker fastCoD -I `; do 
  echo HOST=${HOST} 
  ssh root@${HOST} halt
done
