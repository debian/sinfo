#! /bin/sh

for HOST in ` sinfo -I `; do 
  echo HOST=${HOST} 
  ssh ${HOST} killall MATLAB
done
