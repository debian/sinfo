#include <string.h>
#include "message.h"


Message::Message(unsigned long length, const char * data)
{
  memorySize=length;
  memory = boost::shared_array<char>(new char[memorySize]);

  if (data)
  {
    startIndex=0;
    endIndex=memorySize;
    memcpy(memory.get(),data,memorySize);
  }
  else
  {
    startIndex=memorySize;
    endIndex=memorySize;
  }
  dontSendFlag=false;
}


Message::Message(const Message& message)
{
  memorySize=message.size();
  memory = boost::shared_array<char>(new char[memorySize]);
  startIndex=0;
  endIndex=memorySize;
  memcpy(memory.get(),message.getDataPtr(),memorySize);
  dontSendFlag=message.dontSendFlag;
}


Message::~Message()
{
}


void Message::setMessage(unsigned long length, const char * data)
{
  memorySize=length;
  memory = boost::shared_array<char>(new char[memorySize]);
  startIndex=0;
  endIndex=length;
  memcpy(memory.get(),data,length);
}


Message& Message::operator=(const Message & message)
{
  // check for self-assignment by comparing the address of the implicit object and the parameter
  if (this == &message)
  {
    return *this;
  }
  setMessage(message.size(),message.getDataPtr());
  dontSendFlag=message.dontSendFlag;
  return *this;
}


const char * Message::getDataPtr() const
{
  return &memory[startIndex];
}


unsigned long Message::size() const
{
  return endIndex-startIndex;
}


void Message::clear()
{
  memorySize=STANDARDBUFFERSIZE;
  memory = boost::shared_array<char>(new char[memorySize]);
  startIndex=memorySize;
  endIndex=memorySize;
  dontSendFlag=false;
}


const char & Message::operator [] (unsigned long idx)
{
  unsigned long localidx=startIndex+idx;
  if (localidx>=memorySize)
  {
    throw MessageException("Message::operator[] failed, localidx>=memorySize");
  }
  return memory[localidx];
}

void Message::pushFrontMemory(const void *src, size_t n)
{
  // increase memory by STANDARDBUFFERINCREMENT or n
  if (startIndex<n)
  {
    long increment=STANDARDBUFFERINCREMENT;
    if (startIndex+STANDARDBUFFERINCREMENT<n)
    {
      increment=n;
    }
    long newMemorySize=memorySize+increment;
    boost::shared_array<char>  newMemory(new char[newMemorySize]);
    long newStartIndex=startIndex+increment;
    long newEndIndex=endIndex+increment;
    memcpy(&newMemory[newStartIndex], &memory[startIndex], endIndex-startIndex);

    memorySize=newMemorySize;
    memory=newMemory;
    startIndex=newStartIndex;
    endIndex=newEndIndex;
  }

  startIndex-=n;

  // without litte=>big endian conversion
  // memcpy(&memory[startIndex],src,n);

  // with little =>big endian conversion
  char * srcchar=(char*)src;
  for (unsigned long k=0; k<n; k++)
  {
    memory[startIndex+(n-1)-k]=srcchar[k];
  }
}


void Message::pushBackMemory(const void *src, size_t n)
{
  // increase memory by STANDARDBUFFERINCREMENT or n
  if (endIndex+n>memorySize)
  {
    long increment=STANDARDBUFFERINCREMENT;
    if (endIndex+n>memorySize+STANDARDBUFFERINCREMENT)
    {
      increment=n;
    }
    unsigned long newMemorySize=memorySize+increment;
    boost::shared_array<char>  newMemory(new char[newMemorySize]);
    unsigned long newStartIndex=startIndex;
    unsigned long newEndIndex=endIndex;
    memcpy(&newMemory[newStartIndex], &memory[startIndex], endIndex-startIndex);

    memorySize=newMemorySize;
    memory=newMemory;
    startIndex=newStartIndex;
    endIndex=newEndIndex;
  }

  // without litte=>big endian conversion
  // memcpy(&newMemory[endIndex],src,n);

  // with little =>big endian conversion
  char * srccrar=(char*)src;
  for (unsigned long k=0; k<n; k++)
  {
    memory[endIndex+(n-1)-k]=srccrar[k];
  }

  endIndex+=n;
}


void Message::popFrontMemory(void *dest, size_t n)
{
  if ( n<=endIndex-startIndex )
  {
    // without big=>litte endian conversion
    // memcpy(dest,&memory[startIndex],n);

    // with little =>big endian conversion
    char * destchar=(char*)dest;
    for (unsigned long k=0; k<n; k++)
    {
      destchar[k]=memory[startIndex+(n-1)-k];
    }

    startIndex+=n;


    // shrink memory
    if (startIndex>=STANDARDBUFFERINCREMENT)
    {
      unsigned long newMemorySize=memorySize-STANDARDBUFFERINCREMENT;
      boost::shared_array<char>  newMemory(new char[newMemorySize]);
      unsigned long newStartIndex=startIndex-STANDARDBUFFERINCREMENT;
      unsigned long newEndIndex=endIndex-STANDARDBUFFERINCREMENT;
      memcpy(&newMemory[newStartIndex], &memory[startIndex], endIndex-startIndex);

      memorySize=newMemorySize;
      memory=newMemory;
      startIndex=newStartIndex;
      endIndex=newEndIndex;
    }
  }
  else
  {
    throw MessageException("Message::popFrontMemory failed, no data available");
  }
}
