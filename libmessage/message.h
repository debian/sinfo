#ifndef _message_h
#define _message_h

#include <stdexcept>
#include <boost/shared_array.hpp>


#define STANDARDBUFFERSIZE 4096
#define STANDARDBUFFERINCREMENT 1024


class MessageException : public std::runtime_error
{
public:
  explicit MessageException(const std::string& what):std::runtime_error(what)
  {}

  virtual ~MessageException() throw() {}
};


class Message
{
private:
  boost::shared_array<char> memory;
  unsigned long memorySize;  // size of allocated memory
  unsigned long startIndex;  // memory[startIndex] is the first valid byte
  unsigned long endIndex;    // memory[endIndex]   is the byte AFTER the last valid byte


public:
  Message(unsigned long length=STANDARDBUFFERSIZE, const char * data=0);
  Message(const Message& message);
  ~Message();

  bool dontSendFlag;

  void setMessage(unsigned long length, const char * data);

  Message & operator= (const Message & message);
  const char & operator [] (unsigned long idx);

  const char * getDataPtr() const;
  unsigned long size() const;
  void clear();

  void pushFrontMemory(const void *src, size_t n);
  void pushBackMemory(const void *src, size_t n);
  void popFrontMemory(void *dest, size_t n);
};


#endif // _message_h
