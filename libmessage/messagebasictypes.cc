#include "messagebasictypes.h"


namespace Msg
{


void pushFrontuint8(Message & message, const uint8 & value)
{
  message.pushFrontMemory(&value, sizeof(uint8));
}


void pushBackuint8(Message & message, const uint8 & value)
{
  message.pushBackMemory(&value, sizeof(uint8));
}


void popFrontuint8(Message & message, uint8 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(uint8));
}


void pushFront(Message & message, const uint8 & value)
{
  pushFrontuint8(message, value);
}


void popFront(Message & message, uint8 & returnValue)
{
  popFrontuint8(message, returnValue);
}


void pushFrontuint16(Message & message, const uint16 & value)
{
  message.pushFrontMemory(&value, sizeof(uint16));
}


void pushBackuint16(Message & message, const uint16 & value)
{
  message.pushBackMemory(&value, sizeof(uint16));
}


void popFrontuint16(Message & message, uint16 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(uint16));
}


void pushFrontuint32(Message & message, const uint32 & value)
{
  message.pushFrontMemory(&value, sizeof(uint32));
}


void pushBackuint32(Message & message, const uint32 & value)
{
  message.pushBackMemory(&value, sizeof(uint32));
}


void popFrontuint32(Message & message, uint32 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(uint32));
}


void pushFrontuint64(Message & message, const uint64 & value)
{
  message.pushFrontMemory(&value, sizeof(uint64));
}


void pushBackuint64(Message & message, const uint64 & value)
{
  message.pushBackMemory(&value, sizeof(uint64));
}


void popFrontuint64(Message & message, uint64 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(uint64));
}


void pushFrontint8(Message & message, const int8 & value)
{
  message.pushFrontMemory(&value, sizeof(int8));
}


void pushBackint8(Message & message, const int8 & value)
{
  message.pushBackMemory(&value, sizeof(int8));
}


void popFrontint8(Message & message, int8 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(int8));
}


void pushFrontint16(Message & message, const int16 & value)
{
  message.pushFrontMemory(&value, sizeof(int16));
}


void pushBackint16(Message & message, const int16 & value)
{
  message.pushBackMemory(&value, sizeof(int16));
}


void popFrontint16(Message & message, int16 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(int16));
}


void pushFrontint32(Message & message, const int32 & value)
{
  message.pushFrontMemory(&value, sizeof(int32));
}


void pushBackint32(Message & message, const int32 & value)
{
  message.pushBackMemory(&value, sizeof(int32));
}


void popFrontint32(Message & message, int32 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(int32));
}


void pushFrontint64(Message & message, const int64 & value)
{
  message.pushFrontMemory(&value, sizeof(int64));
}


void pushBackint64(Message & message, const int64 & value)
{
  message.pushBackMemory(&value, sizeof(int64));
}


void popFrontint64(Message & message, int64 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(int64));
}


void pushFrontfloat32(Message & message, const float32 & value)
{
  message.pushFrontMemory(&value, sizeof(float32));
}


void pushBackfloat32(Message & message, const float32 & value)
{
  message.pushBackMemory(&value, sizeof(float32));
}


void popFrontfloat32(Message & message, float32 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(float32));
}


void pushFrontfloat64(Message & message, const float64 & value)
{
  message.pushFrontMemory(&value, sizeof(float64));
}


void pushBackfloat64(Message & message, const float64 & value)
{
  message.pushBackMemory(&value, sizeof(float64));
}


void popFrontfloat64(Message & message, float64 & returnValue)
{
  message.popFrontMemory(&returnValue, sizeof(float64));
}


void pushFront(Message & message, const float64 & value)
{
  pushFrontfloat64(message, value);
}


void popFront(Message & message, float64 & returnValue)
{
  popFrontfloat64(message, returnValue);
}

}
