#ifndef _messagebasictypes_h
#define _messagebasictypes_h


#include "message.h"

typedef unsigned char      uint8;
typedef unsigned short     uint16;
typedef unsigned int       uint32;
typedef unsigned long long uint64;

typedef char               int8;
typedef short              int16;
typedef int                int32;
typedef long long          int64;

typedef float              float32;
typedef double             float64;


namespace Msg
{
void pushFrontuint8(Message & message, const uint8 & value);
void pushBackuint8(Message & message, const uint8 & value);
void popFrontuint8(Message & message, uint8 & returnValue);
void pushFront(Message & message, const uint8 & value);
void popFront(Message & message, uint8 & returnValue);

void pushFrontuint16(Message & message, const uint16 & value);
void pushBackuint16(Message & message, const uint16 & value);
void popFrontuint16(Message & message, uint16 & returnValue);

void pushFrontuint32(Message & message, const uint32 & value);
void pushBackuint32(Message & message, const uint32 & value);
void popFrontuint32(Message & message, uint32 & returnValue);

void pushFrontuint64(Message & message, const uint64 & value);
void pushBackuint64(Message & message, const uint64 & value);
void popFrontuint64(Message & message, uint64 & returnValue);

void pushFrontint8(Message & message, const int8 & value);
void pushBackint8(Message & message, const int8 & value);
void popFrontint8(Message & message, int8 & returnValue);

void pushFrontint16(Message & message, const int16 & value);
void pushBackint16(Message & message, const int16 & value);
void popFrontint16(Message & message, int16 & returnValue);

void pushFrontint32(Message & message, const int32 & value);
void pushBackint32(Message & message, const int32 & value);
void popFrontint32(Message & message, int32 & returnValue);

void pushFrontint64(Message & message, const int64 & value);
void pushBackint64(Message & message, const int64 & value);
void popFrontint64(Message & message, int64 & returnValue);

void pushFrontfloat32(Message & message, const float32 & value);
void pushBackfloat32(Message & message, const float32 & value);
void popFrontfloat32(Message & message, float32 & returnValue);

void pushFrontfloat64(Message & message, const float64 & value);
void pushBackfloat64(Message & message, const float64 & value);
void popFrontfloat64(Message & message, float64 & returnValue);
void pushFront(Message & message, const float64 & value);
void popFront(Message & message, float64 & returnValue);
}


#endif // _messagebasictypes_h
