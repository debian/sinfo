#include "messagebasictypes.h"
#include "messagecomplextypes.h"


namespace Msg
{


void pushFrontstring(Message & message, const std::string & value)
{
  int charCountDown=value.size();
  bool lastPart=true;
  do
  {
    int partSize=charCountDown;
    if (partSize>127)
    {
      partSize=127;
    }
    int partCountDown=partSize;
    while (partCountDown>0)
    {
      pushFrontuint8(message,uint8(value[charCountDown-1]));
      charCountDown--;
      partCountDown--;
    }
    if (true==lastPart)
    {
      pushFrontuint8(message,partSize );
      lastPart=false;
    }
    else
    {
      pushFrontuint8(message,partSize | 0x80 ); // continue flag
    }
  }
  while (charCountDown>0);
}


void pushBackstring(Message & message, const std::string & value)
{
  int charSize=value.size();
  int charCountUp=0;
  do
  {
    int partSize=charSize-charCountUp;
    if (partSize>127)
    {
      partSize=127;
      pushBackuint8(message,partSize | 0x80 ); // continue flag
    }
    else
    {
      pushBackuint8(message,partSize );
    }
    int partCountUp=0;
    while (partCountUp<partSize)
    {
      pushBackuint8(message,uint8(value[charCountUp]));
      partCountUp++;
      charCountUp++;
    }
  }
  while (charCountUp<charSize);
}


void popFrontstring(Message & message, std::string & returnValue)
{
  returnValue="";
  bool continueFlag=true;
  while (continueFlag)
  {
    uint8 partSize;
    popFrontuint8(message,partSize);
    if (0x80 == (partSize&0x80) )
    {
      continueFlag=true;
      partSize&=0x7f;
    }
    else
    {
      continueFlag=false;
    }
    for (int i=0; i<partSize; i++)
    {
      uint8 stringPart;
      popFrontuint8(message,stringPart);
      returnValue+=char(stringPart);
    }
  }
}


void pushFront(Message & message, const std::string & value)
{
  pushFrontstring(message, value);
}


void popFront(Message & message, std::string & returnValue)
{
  popFrontstring(message, returnValue);
}


}
