#ifndef _messagecomplextypes_h
#define _messagecomplextypes_h


#include <string>
#include "message.h"

namespace Msg
{
void pushFrontstring(Message & message, const std::string & value);
void pushBackstring(Message & message, const std::string & value);
void popFrontstring(Message & message, std::string & returnValue);
void pushFront(Message & message, const std::string & value);
void popFront(Message & message, std::string & returnValue);
}


#endif // _messagecomplextypes_h
