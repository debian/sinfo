#ifndef _messagestlcontainertypes_h
#define _messagestlcontainertypes_h


#include <list>
#include <vector>
#include "messagebasictypes.h"


namespace Msg
{

// forward declarations to allow nested templates
template <class T>
void pushFront(Message & message, const std::list<T>  & value);

template <class T>
void popFront(Message & message, std::list<T>  & returnValue);

template <class T>
void pushFront(Message & message, const std::vector<T>  & value);

template <class T>
void popFront(Message & message, std::vector<T>  & returnValue);
// end of forward declarations


template <class T>
void pushFront(Message & message, const std::list<T>  & value)
{
  typename std::list<T>::const_reverse_iterator it = value.rbegin();  // http://de.wikibooks.org/wiki/C++-Programmierung/_Die_STL/_Iteratoren
  int listCountDown=value.size();
  bool lastPart=true;
  do
  {
    int partSize=listCountDown;
    if (partSize>127)
    {
      partSize=127;
    }
    int partCountDown=partSize;
    while (partCountDown>0)
    {
      pushFront(message, *it);
      it++;
      listCountDown--;
      partCountDown--;
    }
    if (true==lastPart)
    {
      pushFrontuint8(message,partSize );
      lastPart=false;
    }
    else
    {
      pushFrontuint8(message,partSize | 0x80 ); // continue flag
    }
  }
  while (it != value.rend());
}


template <class T>
void popFront(Message & message, std::list<T>  & returnValue)
{
  returnValue.erase(returnValue.begin(),returnValue.end());

  bool continueFlag=true;
  while (continueFlag)
  {
    uint8 partSize;
    popFrontuint8(message,partSize);
    if (0x80 == (partSize&0x80) )
    {
      continueFlag=true;
      partSize&=0x7f;
    }
    else
    {
      continueFlag=false;
    }
    for (int i=0; i<partSize; i++)
    {
      T oneValue;
      popFront(message, oneValue);
      returnValue.push_back(oneValue);
    }
  }
}





template <class T>
void pushFront(Message & message, const std::vector<T>  & value)
{
  typename std::vector<T>::const_reverse_iterator it = value.rbegin();  // http://de.wikibooks.org/wiki/C++-Programmierung/_Die_STL/_Iteratoren
  int listCountDown=value.size();
  bool lastPart=true;
  do
  {
    int partSize=listCountDown;
    if (partSize>127)
    {
      partSize=127;
    }
    int partCountDown=partSize;
    while (partCountDown>0)
    {
      pushFront(message, *it);
      it++;
      listCountDown--;
      partCountDown--;
    }
    if (true==lastPart)
    {
      pushFrontuint8(message,partSize );
      lastPart=false;
    }
    else
    {
      pushFrontuint8(message,partSize | 0x80 ); // continue flag
    }
  }
  while (it != value.rend());
}


template <class T>
void popFront(Message & message, std::vector<T>  & returnValue)
{
  returnValue.erase(returnValue.begin(),returnValue.end());

  bool continueFlag=true;
  while (continueFlag)
  {
    uint8 partSize;
    popFrontuint8(message,partSize);
    if (0x80 == (partSize&0x80) )
    {
      continueFlag=true;
      partSize&=0x7f;
    }
    else
    {
      continueFlag=false;
    }
    for (int i=0; i<partSize; i++)
    {
      T oneValue;
      popFront(message, oneValue);
      returnValue.push_back(oneValue);
    }
  }
}


}


#endif // _messagestlcontainertypes_h
