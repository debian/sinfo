#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include "messagestlcontainertypes.h"
#include <iostream>

using namespace std;
using namespace Msg;


int main()
{

  Message message;

  {
    float x=1.2345;
    pushFrontfloat32(message,x);
  }

  {
    double x=1.2345;
    pushFrontfloat64(message,x);
  }

  {
    unsigned char x=110;
    pushFrontuint8(message,x);
  }

  {
    unsigned short x=30000;
    pushFrontuint16(message,x);
  }

  {
    unsigned int x=30000;
    pushFrontuint32(message,x);
  }

  {
    unsigned long long x=30000;
    pushFrontuint64(message,x);
  }

  {
    char x=10;
    pushFrontint8(message,x);
  }

  {
    short x=-30000;
    pushFrontint16(message,x);
  }

  {
    int x=-30000;
    pushFrontint32(message,x);
  }

  {
    long long x=-30000;
    pushFrontint64(message,x);
  }


  {
    string text("bla bla bla");
    pushFrontstring(message,text);
  }

  {
    string text130("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
    pushFrontstring(message,text130);
  }

  {
    list <uint8> uintlist;
    for (int i=0; i<10; i++)
    {
      uintlist.push_back(uint8(i));
    }
    pushFront(message,uintlist);
  }

  {
    vector <uint8> uintvector;
    for (int i=0; i<10; i++)
    {
      uintvector.push_back(uint8(i));
    }
    pushFront(message,uintvector);
  }

  {
    // nested serialization
    list < vector < string > > vslist;
    {
      vector <string> vs1;
      vs1.push_back("a1");
      vs1.push_back("b1");
      vslist.push_back(vs1);

      vector <string> vs2;
      vs2.push_back("a2");
      vs2.push_back("b2");
      vslist.push_back(vs2);
    }
    pushFront(message,vslist);
  }


// pop

  {
    // nested serialization
    list < vector < string > > vslist;
    popFront(message,vslist);
  }

  {
    vector <uint8> uintvector;
    popFront(message,uintvector);

    std::vector<uint8>::const_iterator it;
    for (it=uintvector.begin(); it!=uintvector.end(); ++it)
    {
      cout << "uintvector " << int(*it) << endl;
    }
  }

  {
    list <uint8> uintlist;
    popFront(message,uintlist);

    std::list<uint8>::const_iterator it;
    for (it=uintlist.begin(); it!=uintlist.end(); ++it)
    {
      cout << "unitlist " << int(*it) << endl;
    }
  }

  {
    string text130;
    popFrontstring(message,text130);
    cout << text130 << endl;
  }

  {
    string text;
    popFrontstring(message,text);
    cout << text << endl;
  }

  {
    long long x;
    popFrontint64(message,x);
    cout << x << endl;
  }

  {
    int x;
    popFrontint32(message,x);
    cout << x << endl;
  }

  {
    short x;
    popFrontint16(message,x);
    cout << x << endl;
  }

  {
    char x;
    popFrontint8(message,x);
    cout << int(x) << endl;
  }

  {
    unsigned long long x;
    popFrontuint64(message,x);
    cout << x << endl;
  }

  {
    unsigned int x;
    popFrontuint32(message,x);
    cout << x << endl;
  }

  {
    unsigned short x;
    popFrontuint16(message,x);
    cout << x << endl;
  }

  {
    unsigned char x;
    popFrontuint8(message,x);
    cout << int(x) << endl;
  }

  {
    double x;
    popFrontfloat64(message,x);
    cout << x << endl;
  }

  {
    float x;
    popFrontfloat32(message,x);
    cout << x << endl;
  }


  cout << "remaining size=" << message.size() << endl;



  cout << "sizeof(float32)=" << sizeof(float32) << endl;
  cout << "sizeof(float64)=" << sizeof(float64) << endl;

  cout << "sizeof(uint8)="   << sizeof(uint8)   << endl;
  cout << "sizeof(uint16)="  << sizeof(uint16)  << endl;
  cout << "sizeof(uint32)="  << sizeof(uint32)  << endl;
  cout << "sizeof(uint64)="  << sizeof(uint64)  << endl;

  cout << "sizeof(int8)="    << sizeof(int8)    << endl;
  cout << "sizeof(int16)="   << sizeof(int16)   << endl;
  cout << "sizeof(int32)="   << sizeof(int32)   << endl;
  cout << "sizeof(int64)="   << sizeof(int64)   << endl;

  return 0;
}
