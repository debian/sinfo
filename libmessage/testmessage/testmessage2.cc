#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include <iostream>

using namespace std;
using namespace Msg;


Message makeMessage()
{
  Message message;
  float x=1.2345;
  pushFrontfloat32(message,x);

  cout << hex << uint64(message.getDataPtr()) << endl;

  return message;
}


int main()
{

  cout << "// the pointer is the same" << endl;
  Message message2=makeMessage();
  cout << "message2.size() " << dec << message2.size() << " " << hex << uint64(message2.getDataPtr()) << endl;
  cout << sizeof(message2.getDataPtr()) << endl;


  cout << "// same data" << endl;
  Message a=message2;
  Message b=message2;
  cout << "a.size() " << dec << a.size() << " " << hex << uint64(a.getDataPtr()) << endl;
  cout << "b.size() " << dec << b.size() << " " << hex << uint64(b.getDataPtr()) << endl;
  float x=1.2345;
  pushFrontfloat32(a,x);
  cout << "a.size() " << dec << a.size() << " " << hex << uint64(a.getDataPtr()) << endl;
  cout << "b.size() " << dec << b.size() << " " << hex << uint64(b.getDataPtr()) << endl;



  return 0;
}
