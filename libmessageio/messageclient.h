#ifndef _messageclient_h
#define _messageclient_h


#include <boost/signals2.hpp>
#include "message.h"
#include "messageioconst.h"


class MessageClient
{
public:
  virtual ~MessageClient() {}
  bool stopAfterOneReceivedMessage;

  boost::signals2::signal<void ()> connectionReadySignal;
  boost::signals2::signal<void ()> connectionNotReadySignal;
  boost::signals2::signal<void (Message & returnMessage)> receivedMessageSignal;
  virtual void queueAndSendMessageSlot(Message & message)=0;
};


#endif // _messageclient_h
