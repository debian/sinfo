#ifndef _messageioconst_h
#define _messageioconst_h

enum { maxMessageIOSize = 65535 }; // maximum message size to handle (without overhead e.g. header size)

enum { maxSendQueueSize = 500 }; // maximum number of messages in queues; drop messages if exceeded


#endif // _messageioconst_h
