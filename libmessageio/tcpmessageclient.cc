#include <iostream>
#include <boost/bind.hpp>
#include "message.h"
#include "messagebasictypes.h"
#include "tcpmessageclient.h"
using namespace std;


TCPMessageClient::TCPMessageClient(boost::asio::io_service& io_service, const char * host, const char * port):
  ioservice(io_service),
  timer(ioservice),
  resolver(ioservice),
  socket(ioservice),
  m_host(host),
  m_port(port)
{
  startResolver();
}

TCPMessageClient::~TCPMessageClient()
{
}


void TCPMessageClient::startResolver()
{
  boost::asio::ip::tcp::resolver::query query(m_host, m_port);
  stopAfterOneReceivedMessage=false;
  sendQueueCurrentlySending=false;
  resolver.async_resolve(query,
                         boost::bind(&TCPMessageClient::handleResolve, this, boost::asio::placeholders::error, boost::asio::placeholders::iterator));
}


void TCPMessageClient::closeAndScheduleResolve()
{
  socket.close();

  boost::posix_time::time_duration expiry_time=boost::posix_time::seconds(3);
  timer.expires_from_now(expiry_time);
  timer.async_wait(boost::bind(&TCPMessageClient::startResolver, this));
}


void TCPMessageClient::handleResolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
{
  if (!err)
  {
    // Attempt a connection to the first endpoint in the list. Each endpoint
    // will be tried until we successfully establish a connection.
    boost::asio::ip::tcp::endpoint endpoint = *endpoint_iterator;
    socket.async_connect(endpoint,
                         boost::bind(&TCPMessageClient::handleConnect, this,
                                     boost::asio::placeholders::error, ++endpoint_iterator));
  }
  else
  {
    std::cout << "TCPMessageClient::handleResolve error: " << err.message() << endl;
    closeAndScheduleResolve();
  }
}


void TCPMessageClient::handleConnect(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
{
  if (!err)
  {
    // start handleReadMessageSize, handleReadMessage Event loop
    boost::asio::async_read(socket,
                            boost::asio::buffer(data, sizeof(uint32)),
                            boost::asio::transfer_at_least(sizeof(uint32)),
                            boost::bind(&TCPMessageClient::handleReadMessageSize, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

    connectionReadySignal();
  }
  else if (endpoint_iterator != boost::asio::ip::tcp::resolver::iterator())
  {
    // The connection failed. Try the next endpoint in the list.
    socket.close();
    boost::asio::ip::tcp::endpoint endpoint = *endpoint_iterator;
    socket.async_connect(endpoint,
                         boost::bind(&TCPMessageClient::handleConnect, this,
                                     boost::asio::placeholders::error, ++endpoint_iterator));
  }
  else
  {
    std::cout << "TCPMessageClient::handleConnect error: " << err.message() << endl;
    connectionNotReadySignal();
    closeAndScheduleResolve();
  }
}


void TCPMessageClient::queueAndSendMessageSlot(Message & message)
{

  if (sendQueue.size()<maxSendQueueSize)
  {
    if (message.size()<=maxMessageIOSize)
    {
      sendQueue.push_back(message);
      Msg::pushFrontint32(sendQueue.back(),message.size());
    }
  }
  startNewTransmission();
}


void TCPMessageClient::startNewTransmission()
{
  if ((false==sendQueueCurrentlySending) && (!sendQueue.empty()))
  {
    Message & message=sendQueue.front();
    sendQueueCurrentlySending=true;
    boost::asio::async_write(socket,boost::asio::buffer(message.getDataPtr(), message.size()),
                             boost::bind(&TCPMessageClient::handleWriteMessage, this, boost::asio::placeholders::error));
  }
}


void TCPMessageClient::handleWriteMessage(const boost::system::error_code& err)
{
  // cout << "UDPMessageClient::handleSendTo" << endl;
  // cout << "sendQueue.front().size()=" << sendQueue.front().size() << endl;

  if (!err)
  {
    sendQueue.pop_front();
    sendQueueCurrentlySending=false;
    startNewTransmission();
  }
  else
  {
    cout << "TCPMessageClient::handleWriteMessage error: " << err.message() << endl;
    connectionNotReadySignal();
    closeAndScheduleResolve();
  }
}


void TCPMessageClient::handleReadMessageSize(const boost::system::error_code& err, size_t length)
{
  if (!err)
  {
    //cout << "handleReadMessageSize length=" << length << endl;
    Message sizeMessage(length,data);
    uint32 messageSizeTmp;
    Msg::popFrontuint32(sizeMessage, messageSizeTmp);
    messageSize=messageSizeTmp;

    boost::asio::async_read(socket,
                            boost::asio::buffer(data, messageSize),
                            boost::asio::transfer_at_least(messageSize),
                            boost::bind(&TCPMessageClient::handleReadMessage, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  }
  else
  {
    std::cout << "TCPMessageClient::handleReadMessageSize error: " << err << endl;
    connectionNotReadySignal();
    closeAndScheduleResolve();
  }
}


void TCPMessageClient::handleReadMessage(const boost::system::error_code& err, size_t length)
{
  if (!err)
  {
    // Write all of the data that has been read so far.
    Message returnMessage(length,data);
    receivedMessageSignal(returnMessage);

    if (false==stopAfterOneReceivedMessage)
    {
      // read next size
      boost::asio::async_read(socket,
                              boost::asio::buffer(data, sizeof(uint32)),
                              boost::asio::transfer_at_least(sizeof(uint32)),
                              boost::bind(&TCPMessageClient::handleReadMessageSize, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    }
  }
  else if (err != boost::asio::error::eof)
  {
    std::cout << "TCPMessageClient::handleReadMessage error: " << err << endl;
    connectionNotReadySignal();
    closeAndScheduleResolve();
  }
}

