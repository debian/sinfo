#ifndef _tcpmessageclient_h
#define _tcpmessageclient_h


#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/signals2.hpp>
#include "messageclient.h"

// class TCPMessageClient
//    actively tries to connect to a server
//    and tries to reconnect to a server every the the  connection is interrupted
//
// There is no interface to start and stop a connection to the upper leyer code.
//
// Availability is signalized to upper layer by the signals
//   connectionReadySignal
// and
//   connectioNotReadySignal
class TCPMessageClient : public MessageClient
{
private:
  boost::asio::io_service & ioservice;

  boost::asio::deadline_timer timer;

  boost::asio::ip::tcp::resolver resolver;
  boost::asio::ip::tcp::socket socket;


  unsigned long messageSize;
  char data[maxMessageIOSize];

  void handleResolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator);
  void handleConnect(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

  void handleReadMessageSize(const boost::system::error_code& err, size_t length);
  void handleReadMessage(const boost::system::error_code& err, size_t length);

  std::list<Message>  sendQueue;
  void startResolver();
  void closeAndScheduleResolve();
  bool sendQueueCurrentlySending;
  void startNewTransmission();
  void handleWriteMessage(const boost::system::error_code& err);

  std::string m_host;
  std::string m_port;

public:
  TCPMessageClient(boost::asio::io_service& io_service, const char * host, const char * port);
  ~TCPMessageClient();

  void queueAndSendMessageSlot(Message & message);
};


#endif // _tcpmessageclient_h
