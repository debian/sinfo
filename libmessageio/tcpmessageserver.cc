#include "tcpmessageserver.h"
#include <boost/bind.hpp>


TCPMessageServer::TCPMessageServer(boost::asio::io_service& io_service, const boost::asio::ip::tcp::endpoint & endpoint, ServerConnectorFactoryBase & serverConnectorFactoryBase)
  : ioservice(io_service),
    acceptor(ioservice),
    serverConnectorFactoryBase(serverConnectorFactoryBase),
    tcpMessageServerConnectionManager(),
    newConnection(new TCPMessageServerConnection(ioservice,
                  tcpMessageServerConnectionManager, serverConnectorFactoryBase))
{
  // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
  acceptor.open(endpoint.protocol());
  if (endpoint.address().is_v6())
  {
    acceptor.set_option(boost::asio::ip::v6_only(true));
  }
  acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
  acceptor.bind(endpoint);
  acceptor.listen();
  acceptor.async_accept(newConnection->socket(),
                        boost::bind(&TCPMessageServer::handleAccept, this, boost::asio::placeholders::error));
}


void TCPMessageServer::run()
{
  // The io_service::run() call will block until all asynchronous operations
  // have finished. While the server is running, there is always at least one
  // asynchronous operation outstanding: the asynchronous accept call waiting
  // for new incoming connections.
  ioservice.run();
}


void TCPMessageServer::stop()
{
  // Post a call to the stop function so that server::stop() is safe to call
  // from any thread.
  ioservice.post(boost::bind(&TCPMessageServer::handleStop, this));
}


void TCPMessageServer::handleAccept(const boost::system::error_code& e)
{
  if (!e)
  {
    tcpMessageServerConnectionManager.start(newConnection);
    newConnection.reset(new TCPMessageServerConnection(ioservice,
                        tcpMessageServerConnectionManager, serverConnectorFactoryBase));

    acceptor.async_accept(newConnection->socket(),
                          boost::bind(&TCPMessageServer::handleAccept, this,
                                      boost::asio::placeholders::error));
  }
}


void TCPMessageServer::handleStop()
{
  // The server is stopped by cancelling all outstanding asynchronous
  // operations. Once all operations have finished the io_service::run() call
  // will exit.
  acceptor.close();
  tcpMessageServerConnectionManager.stopAll();
}
