#ifndef _tcpmessageserver_h
#define _tcpmessageserver_h

#include <boost/asio.hpp>
#include <string>
#include <boost/noncopyable.hpp>
#include "tcpmessageserverconnection.h"


class TCPMessageServer : private boost::noncopyable
{
private:
  boost::asio::io_service & ioservice;
  boost::asio::ip::tcp::acceptor acceptor;

  ServerConnectorFactoryBase & serverConnectorFactoryBase;

  void handleAccept(const boost::system::error_code& e);
  void handleStop();

  TCPMessageServerConnectionManager tcpMessageServerConnectionManager;

  connection_ptr newConnection;

public:
  explicit TCPMessageServer(boost::asio::io_service& io_service, const boost::asio::ip::tcp::endpoint & endpoint, ServerConnectorFactoryBase & serverConnectorFactoryBase);

  void run();

  void stop();
};


#endif // _tcpmessageserver_h
