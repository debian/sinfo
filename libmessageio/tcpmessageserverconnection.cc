#include <vector>
#include <boost/bind.hpp>
#include "tcpmessageserverconnection.h"
#include "messagebasictypes.h"
#include "messageioconst.h"
#include <iostream>
using namespace std;


TCPMessageServerConnection::TCPMessageServerConnection(boost::asio::io_service& io_service,
    TCPMessageServerConnectionManager& manager, ServerConnectorFactoryBase & serverConnectorFactoryBase)
  : socket_(io_service), tcpMessageServerConnectionManager(manager), serverConnectorFactoryBase(serverConnectorFactoryBase)
{
  messageSize=0;
  data[0]=0;
  sendQueueCurrentlySending=false;
}


boost::asio::ip::tcp::socket& TCPMessageServerConnection::socket()
{
  return socket_;
}


void TCPMessageServerConnection::start()
{
  serverConnectorBasePtr=serverConnectorFactoryBase.createServerConnector();

  serverConnectorBasePtr->sendMessageSignal.connect(boost::bind(&TCPMessageServerConnection::queueAndSendMessageSlot, this, _1));  // FIXME: managed signal slot for clean shutdown
  messageSignal.connect(boost::bind(&ServerConnectorBase::receiveMessageSlot, serverConnectorBasePtr, _1));  // FIXME: managed signal slot for clean shutdown

  boost::asio::async_read(socket_,
                          boost::asio::buffer(data, sizeof(uint32)),
                          boost::asio::transfer_at_least(sizeof(uint32)),
                          boost::bind(&TCPMessageServerConnection::handleReadMessageSize, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}


void TCPMessageServerConnection::stop()
{
  socket_.close();
}


void TCPMessageServerConnection::handleReadMessageSize(const boost::system::error_code& err, size_t length)
{
  if (!err)
  {
    // cout << "handleReadMessageSize length=" << length << endl;
    Message sizeMessage(length,data);
    uint32 messageSizeTmp;
    Msg::popFrontuint32(sizeMessage, messageSizeTmp);
    messageSize=messageSizeTmp;

    boost::asio::async_read(socket_,
                            boost::asio::buffer(data, messageSize),
                            boost::asio::transfer_at_least(messageSize),
                            boost::bind(&TCPMessageServerConnection::handleReadMessage, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  }
  else if (err != boost::asio::error::operation_aborted)
  {
    tcpMessageServerConnectionManager.stop(shared_from_this());
  }
}


void TCPMessageServerConnection::handleReadMessage(const boost::system::error_code& err, size_t length)
{
  if (!err)
  {
    Message message(length,data);
//    Message returnMessage; // FIXME

    // call server functions
    // receiveMessageSignal(returnMessage,message); // FIXME
    messageSignal(message);

//    queueAndSendMessageSlot(returnMessage); // FIXME

    // read next size
    boost::asio::async_read(socket_,
                            boost::asio::buffer(data, sizeof(uint32)),
                            boost::asio::transfer_at_least(sizeof(uint32)),
                            boost::bind(&TCPMessageServerConnection::handleReadMessageSize, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  }
  else if (err != boost::asio::error::operation_aborted)
  {
    tcpMessageServerConnectionManager.stop(shared_from_this());
  }
}


void TCPMessageServerConnection::queueAndSendMessageSlot(Message & message)
{

  if (sendQueue.size()<maxSendQueueSize)
  {
    if (message.size()<=maxMessageIOSize)
    {
      sendQueue.push_back(message);
      Msg::pushFrontint32(sendQueue.back(),message.size());
    }
  }
  startNewTransmission();
}


void TCPMessageServerConnection::startNewTransmission()
{
  if ((false==sendQueueCurrentlySending) && (!sendQueue.empty()))
  {
    Message & message=sendQueue.front();
    sendQueueCurrentlySending=true;
    boost::asio::async_write(socket_,boost::asio::buffer(message.getDataPtr(), message.size()),
                             boost::bind(&TCPMessageServerConnection::handleWriteMessage, this, boost::asio::placeholders::error));
  }
}


void TCPMessageServerConnection::handleWriteMessage(const boost::system::error_code& err)
{
  // cout << "UDPMessageClient::handleSendTo" << endl;
  // cout << "sendQueue.front().size()=" << sendQueue.front().size() << endl;

  if (!err)
  {
    sendQueue.pop_front();
    sendQueueCurrentlySending=false;
    startNewTransmission();
  }
  else
  {
    cout << "TCPMessageServerConnection::handleWriteMessage error: " << err.message() << endl;
  }
}


void TCPMessageServerConnectionManager::start(connection_ptr c)
{
  connectionPtrSet.insert(c);
  c->start();
}


void TCPMessageServerConnectionManager::stop(connection_ptr c)
{
  connectionPtrSet.erase(c);
  c->stop();
}


void TCPMessageServerConnectionManager::stopAll()
{
  std::for_each(connectionPtrSet.begin(), connectionPtrSet.end(),
                boost::bind(&TCPMessageServerConnection::stop, _1));
  connectionPtrSet.clear();
}
