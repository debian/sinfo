#ifndef _tcpmessageserverconnection_h
#define _tcpmessageserverconnection_h

#include <set>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "messageioconst.h"
#include "serverconnectorfactorybase.h"


class TCPMessageServerConnectionManager;

class TCPMessageServerConnection
  : public boost::enable_shared_from_this<TCPMessageServerConnection>,
  private boost::noncopyable
{
private:
  boost::asio::ip::tcp::socket socket_;
  TCPMessageServerConnectionManager & tcpMessageServerConnectionManager;
  boost::signals2::signal<void (Message & message)> messageSignal;

  ServerConnectorFactoryBase & serverConnectorFactoryBase;

  boost::shared_ptr<ServerConnectorBase> serverConnectorBasePtr;

  unsigned long messageSize;
  char data[maxMessageIOSize];

  void handleReadMessageSize(const boost::system::error_code& err, size_t length);
  void handleReadMessage(const boost::system::error_code& err, size_t length);

  void queueAndSendMessageSlot(Message & message);
  std::list<Message>  sendQueue;
  bool sendQueueCurrentlySending;
  void startNewTransmission();
  void handleWriteMessage(const boost::system::error_code& err);

public:
  explicit TCPMessageServerConnection(boost::asio::io_service& io_service, TCPMessageServerConnectionManager& manager, ServerConnectorFactoryBase & serverConnectorFactoryBase);

  boost::asio::ip::tcp::socket& socket();

  void start();
  void stop();
};


typedef boost::shared_ptr<TCPMessageServerConnection> connection_ptr;


/// Manages open connections so that they may be cleanly stopped when the server
/// needs to shut down.
class TCPMessageServerConnectionManager: private boost::noncopyable
{
private:
  std::set<connection_ptr> connectionPtrSet;

public:
  /// Add the specified connection to the manager and start it.
  void start(connection_ptr c);

  /// Stop the specified connection.
  void stop(connection_ptr c);

  /// Stop all connections.
  void stopAll();

};


#endif // _tcpmessageserverconnection_h
