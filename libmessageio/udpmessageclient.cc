#include <boost/bind.hpp>
#include <iostream>
#include "udpmessageclient.h"
using namespace std;


UDPMessageClient::UDPMessageClient(boost::asio::io_service& io_service, const char * host, const char * port) : ioservice(io_service), resolver(ioservice), socket(ioservice)
{
  boost::asio::ip::udp::resolver::query query(host, port);
  resolver.async_resolve(query,
                         boost::bind(&UDPMessageClient::handleResolve, this, boost::asio::placeholders::error, boost::asio::placeholders::iterator));
  stopAfterOneReceivedMessage=false;
  sendQueueCurrentlySending=false;
}


UDPMessageClient::~UDPMessageClient()
{
}


void UDPMessageClient::handleResolve(const boost::system::error_code& err, boost::asio::ip::udp::resolver::iterator endpointIterator)
{
  // cout << "UDPMessageClient::handleResolve" << endl;
  if (err)
  {
    cout << "receive error: " << err.message() << endl;
  }
  else
  {
    remoteEndpoint=*endpointIterator;

    boost::asio::ip::udp::endpoint localEndpoint(remoteEndpoint.protocol(), 0);
    socket.open(localEndpoint.protocol());
    if (localEndpoint.address().is_v6())
    {
      socket.set_option(boost::asio::ip::v6_only(true));
    }
    socket.bind(localEndpoint);

    socket.async_receive_from(boost::asio::buffer(data, maxMessageIOSize), remoteEndpoint,
                              boost::bind(&UDPMessageClient::handleReceiveFrom, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    connectionReadySignal();
  }
}


void UDPMessageClient::handleReceiveFrom(const boost::system::error_code& err, size_t length)
{
  //cout << "UDPMessageClient::handle_receive_from" << endl;
  if (err)
  {
    cout << "receive error: " << err.message() << endl;
  }
  else
  {
    Message returnMessage(length, data);
    receivedMessageSignal(returnMessage);

    if (false==stopAfterOneReceivedMessage)
    {
      socket.async_receive_from(boost::asio::buffer(data, maxMessageIOSize), remoteEndpoint,
                                boost::bind(&UDPMessageClient::handleReceiveFrom, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    }
  }
}


void UDPMessageClient::queueAndSendMessageSlot(Message & message)
{
  //cout << "UDPMessageClient::queueAndSendMessageEvent" << endl;
  //cout << "remoteEndpoint=" << remoteEndpoint << endl;

  if (sendQueue.size()<maxSendQueueSize)
  {
    if (message.size()<=maxMessageIOSize)
    {
      sendQueue.push_back(message);
    }
  }
  startNewTransmission();
}


void UDPMessageClient::startNewTransmission()
{
  if ((false==sendQueueCurrentlySending) && (!sendQueue.empty()))
  {
    Message message=sendQueue.front();
    sendQueueCurrentlySending=true;
    socket.async_send_to(boost::asio::buffer(message.getDataPtr(), message.size()), remoteEndpoint,
                         boost::bind(&UDPMessageClient::handleSendTo, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  }
}


void UDPMessageClient::handleSendTo(const boost::system::error_code& err, size_t length)
{
  // cout << "UDPMessageClient::handleSendTo" << endl;
  // cout << "sendQueue.front().size()=" << sendQueue.front().size() << endl;
  // cout << "length=" << length << endl;

  if (!err)
  {
    if (length != sendQueue.front().size())
    {
      cout << "an error that should never happen" << endl;
    }

    sendQueue.pop_front();
    sendQueueCurrentlySending=false;
    startNewTransmission();
  }
  else
  {
    cout << "UDPMessageClient::handleSendTo error: " << err.message() << endl;
  }
}
