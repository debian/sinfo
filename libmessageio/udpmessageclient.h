#ifndef _udpmessageclient_h
#define _udpmessageclient_h


#include <boost/asio.hpp>
#include <list>
#include "messageclient.h"


class UDPMessageClient : public MessageClient
{
private:
  boost::asio::io_service & ioservice;

  boost::asio::ip::udp::resolver resolver;
  boost::asio::ip::udp::endpoint remoteEndpoint;
  void handleResolve(const boost::system::error_code& err, boost::asio::ip::udp::resolver::iterator endpointIterator);


  boost::asio::ip::udp::socket socket;
  char data[maxMessageIOSize];
  void handleReceiveFrom(const boost::system::error_code& err, size_t length);

  std::list<Message>  sendQueue;
  bool sendQueueCurrentlySending;
  void handleSendTo(const boost::system::error_code& err, size_t length);
  void startNewTransmission();

public:
  UDPMessageClient(boost::asio::io_service& io_service, const char * host, const char * port);
  ~UDPMessageClient();

  void queueAndSendMessageSlot(Message & message);
};


#endif // _udpmessageclient_h
