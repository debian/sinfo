#include <boost/bind.hpp>
#include "udpmessagereceiver.h"


UDPMessageReceiver::UDPMessageReceiver(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint & listenEndpoint,
                                       const boost::asio::ip::address& multicast_address )
  :ioservice(io_service),
   sock(io_service)
{
  sock.open(listenEndpoint.protocol());
  if (listenEndpoint.address().is_v6())
  {
    sock.set_option(boost::asio::ip::v6_only(true));
  }
  sock.set_option(boost::asio::ip::udp::socket::reuse_address(true));
  sock.bind(listenEndpoint);

  if (( (multicast_address.is_v4()) && (multicast_address.to_v4().is_multicast())  )
      ||( (multicast_address.is_v6()) && (multicast_address.to_v6().is_multicast())  ) )
  {
    // Join the multicast group.
    sock.set_option(boost::asio::ip::multicast::join_group(multicast_address));
  }

  sock.async_receive_from( boost::asio::buffer(data, maxMessageIOSize), sender_endpoint,
                           boost::bind(&UDPMessageReceiver::handleReceiveFrom, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

}


void UDPMessageReceiver::handleReceiveFrom(const boost::system::error_code& err, size_t length)
{
  if (err)
  {
    // cout << "receive error: " << err.message() << endl;
  }
  else
  {
    //cout << "UDPMessageServer::handle_receive_from received " << length << " bytes" << endl;
    Message message(length, data);
    receiveMessageSignal(message);

    sock.async_receive_from( boost::asio::buffer(data, maxMessageIOSize), sender_endpoint,
                             boost::bind(&UDPMessageReceiver::handleReceiveFrom, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

  }
}

