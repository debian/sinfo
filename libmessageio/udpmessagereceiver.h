#ifndef _udpmessagereceiver_h
#define _udpmessagereceiver_h

#include <boost/asio.hpp>
#include <boost/signals2.hpp>
#include "message.h"
#include "messageioconst.h"


class UDPMessageReceiver
{
private:
  char data[maxMessageIOSize];
  boost::asio::ip::udp::endpoint sender_endpoint;

  boost::asio::io_service & ioservice;
  boost::asio::ip::udp::socket sock;


  void handleReceiveFrom(const boost::system::error_code& err, size_t length);

public:
  UDPMessageReceiver(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint & listenEndpoint,
                     const boost::asio::ip::address& multicast_address);

  boost::signals2::signal<void (Message & message)> receiveMessageSignal;
};


#endif // _udpmessagereceiver_h
