#include <boost/bind.hpp>
#include "udpmessageserver.h"



UDPMessageServer::UDPMessageServer(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint & endpoint )
  :ioservice(io_service),
   sock(io_service)
{
  sock.open(endpoint.protocol());
  if (endpoint.address().is_v6())
  {
    sock.set_option(boost::asio::ip::v6_only(true));
  }
  sock.bind(endpoint);

  sock.async_receive_from( boost::asio::buffer(data, maxMessageIOSize), sender_endpoint,
                           boost::bind(&UDPMessageServer::handleReceiveFrom, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

}


void UDPMessageServer::handleReceiveFrom(const boost::system::error_code& err, size_t length)
{
  if (err)
  {
    // cout << "receive error: " << err.message() << endl;
  }
  else
  {
    //cout << "UDPMessageServer::handle_receive_from received " << length << " bytes" << endl;
    Message message(length, data);
    Message returnMessage;
    receiveMessageSignal(returnMessage, message);

    // FIXME  add a queue (including timer for retransmission) for returnMessage handling - how to handle different sender_endpoints?

    if ((false==returnMessage.dontSendFlag) && (returnMessage.size()>0) && (returnMessage.size()<=maxMessageIOSize))
    {
      //cout << "UDPMessageServer::handle_receive_from sending " << returnMessage.size() << " bytes" << endl;
      sock.send_to(boost::asio::buffer(returnMessage.getDataPtr(), returnMessage.size()), sender_endpoint);
    }

    sock.async_receive_from( boost::asio::buffer(data, maxMessageIOSize), sender_endpoint,
                             boost::bind(&UDPMessageServer::handleReceiveFrom, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

  }
}

