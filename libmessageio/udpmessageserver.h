#ifndef _udpmessageserver_h
#define _udpmessageserver_h

#include <boost/asio.hpp>
#include "messageserver.h"
#include "messageioconst.h"


class UDPMessageServer : public MessageServer
{
private:
  char data[maxMessageIOSize];
  boost::asio::ip::udp::endpoint sender_endpoint;

  boost::asio::io_service & ioservice;
  boost::asio::ip::udp::socket sock;


  void handleReceiveFrom(const boost::system::error_code& err, size_t length);

public:
  UDPMessageServer(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint & endpoint);
};


#endif // _udpmessageserver_h
