#include "udpmessagetransmitter.h"
#include "messageioconst.h"
#include <string>
using namespace std;


UDPMessageTransmitter::UDPMessageTransmitter(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint & senderEndpoint) :ioservice(io_service), senderEndpoint(senderEndpoint)
{
}


void UDPMessageTransmitter::send(const Message & message)
{
  boost::asio::ip::udp::socket sock(ioservice,senderEndpoint.protocol());
  sock.set_option(boost::asio::socket_base::broadcast(true));

  if (message.size()<=maxMessageIOSize)
  {
    sock.send_to(boost::asio::buffer(message.getDataPtr(), message.size()), senderEndpoint);
  }
}
