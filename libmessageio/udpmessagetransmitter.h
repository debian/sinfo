#ifndef _udpmessagetransmitter_h
#define _udpmessagetransmitter_h

#include <boost/asio.hpp>
#include "message.h"


class UDPMessageTransmitter
{
private:
  boost::asio::io_service & ioservice;
  boost::asio::ip::udp::endpoint senderEndpoint;

public:
  UDPMessageTransmitter(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint & senderEndpoint);
  void send(const Message & message);
};


#endif // _udpmessagetransmitter_h
