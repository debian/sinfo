#ifndef _messageserver_h
#define _messageserver_h

#include <boost/signals2.hpp>
#include "message.h"


typedef boost::signals2::signal<void (Message & returnMessage, Message & message)>  ReceiveMessageSignal;


class MessageServer
{
public:
  ReceiveMessageSignal receiveMessageSignal;
};


#endif // _messageserver_h
