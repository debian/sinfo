#ifndef _serverconnectorbase_h
#define _serverconnectorbase_h


#include <boost/signals2.hpp>
#include "message.h"

class ServerConnectorBase
{
public:
  boost::signals2::signal<void (Message message)>  sendMessageSignal;
  virtual void receiveMessageSlot(Message message)=0;
};


#endif // _serverconnectorbase_h
