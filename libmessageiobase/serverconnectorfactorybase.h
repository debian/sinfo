#ifndef _serverconnectorfactorybase_h
#define _serverconnectorfactorybase_h

#include "serverconnectorbase.h"


class ServerConnectorFactoryBase
{
public:
  ServerConnectorFactoryBase();
  ~ServerConnectorFactoryBase();


  virtual boost::shared_ptr<ServerConnectorBase> createServerConnector()=0;
};


#endif // _serverconnectorfactorybase_h
