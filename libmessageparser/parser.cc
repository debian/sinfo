#include <iostream>
#include "parser.h"
#include "messagebasictypes.h"
using namespace std;
using namespace Msg;


void CompositeParser::addParser(int requestID, int replyID, Parser::SPtr sPtr)
{
  myMap.insert(pair<int, ParsingEntry>(requestID, ParsingEntry(requestID,replyID,sPtr)));
}


void CompositeParser::parse(Message & returnMessage, Message & message)
{
  uint8 messageID;
  popFrontuint8(message, messageID);

  // find will return an iterator to the matching element if it is found
  // or to the end of the map if the key is not found
  mapType::const_iterator iter = myMap.find(messageID);
  if ( iter != myMap.end() )
  {
    iter->second.sPtr->parse(returnMessage, message);
    pushFrontuint8(returnMessage,iter->second.replyID);
  }
  else
  {
    cout << "Key is not in myMap" << '\n';
  }
}

