#ifndef _parser_h
#define _parser_h


#include "message.h"
#include <map>
#include <boost/shared_ptr.hpp>


// http://en.wikipedia.org/wiki/Composite_pattern
class Parser
{
public:
  typedef boost::shared_ptr<Parser> SPtr;

  virtual void parse(Message & returnMessage, Message & message) = 0;
  virtual ~Parser() {}
};


class CompositeParser: public Parser
{
private:
  struct ParsingEntry
  {
    int requestID;
    int replyID;
    Parser::SPtr sPtr;

    ParsingEntry(int requestID, int replyID, Parser::SPtr sPtr): requestID(requestID), replyID(replyID), sPtr(sPtr)
    {
    }
  };

  typedef std::map<int, ParsingEntry> mapType;
  mapType myMap;

public:
  void addParser(int requestID, int replyID, Parser::SPtr sPtr);

  void parse(Message & returnMessage, Message & message);
};


#endif // _parser_h
