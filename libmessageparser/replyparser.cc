#include <iostream>
#include "replyparser.h"
#include "messagebasictypes.h"
using namespace std;
using namespace Msg;


void CompositeReplyParser::addReplyParser(int replyID, ReplyParser::SPtr sPtr)
{
  myMap.insert(pair<int, ParsingEntry>(replyID, ParsingEntry(replyID,sPtr)));
}


void CompositeReplyParser::parse(Message & replyMessage)
{
  if (replyMessage.size() > sizeof(uint8))
  {
    uint8 messageID;
    popFrontuint8(replyMessage, messageID);

    // find will return an iterator to the matching element if it is found
    // or to the end of the map if the key is not found
    mapType::const_iterator iter = myMap.find(messageID);
    if ( iter != myMap.end() )
    {
      iter->second.sPtr->parse(replyMessage);
    }
    else
    {
      cout << "Key is not in myMap" << '\n';
    }
  }
}

