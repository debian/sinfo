#ifndef _replyparser_h
#define _replyparser_h


#include <map>
#include <boost/shared_ptr.hpp>
#include "message.h"


// http://en.wikipedia.org/wiki/Composite_pattern
class ReplyParser
{
public:
  typedef boost::shared_ptr<ReplyParser> SPtr;

  virtual void parse(Message & replyMessage) = 0;
  virtual ~ReplyParser() {}
};


class CompositeReplyParser: public ReplyParser
{
private:
  struct ParsingEntry
  {
    int replyID;
    ReplyParser::SPtr sPtr;

    ParsingEntry(int replyID, ReplyParser::SPtr sPtr): replyID(replyID), sPtr(sPtr)
    {
    }
  };

  typedef std::map<int, ParsingEntry> mapType;
  mapType myMap;

public:
  void addReplyParser(int replyID, ReplyParser::SPtr sPtr);

  void parse(Message & replyMessage);
};


#endif // _replyparser_h
