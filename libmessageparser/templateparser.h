#ifndef _templateparser_h
#define _templateparser_h


#include <boost/signals2.hpp>
#include "parser.h"


// sorted by _#ret  then by _#

class TemplateParser_0ret_0 : public Parser
{
public:
  boost::signals2::signal<void ()> callWorkerSignal;

  void parse(Message & /*returnMessage*/, Message & /*message*/)
  {
    callWorkerSignal();
  }
};


template<typename T1>
class TemplateParser_0ret_1 : public Parser
{
public:
  boost::signals2::signal<void (const T1&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & message)
  {
    T1 value1;
    popFront(message, value1);

    callWorkerSignal(value1);
  }
};


template<typename T1ret>
class TemplateParser_1ret_0 : public Parser
{
public:
  boost::signals2::signal<void (T1ret&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & /*message*/)
  {
    T1ret returnValue1;
    callWorkerSignal(returnValue1);

    pushFront(returnMessage, returnValue1);
  }
};


template<typename T1ret, typename T1>
class TemplateParser_1ret_1 : public Parser
{
public:
  boost::signals2::signal<void (T1ret&, const T1&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & message)
  {

    T1 value1;
    popFront(message, value1);

    T1ret returnValue1;
    callWorkerSignal(returnValue1,value1);

    pushFront(returnMessage, returnValue1);
  }
};



template<typename T1ret, typename T1, typename T2>
class TemplateParser_1ret_2 : public Parser
{
public:
  boost::signals2::signal<void (T1ret&, const T1&, const T2&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & message)
  {

    T1 value1;
    T2 value2;
    popFront(message, value1);
    popFront(message, value2);

    T1ret returnValue1;
    callWorkerSignal(returnValue1,value1,value2);

    pushFront(returnMessage, returnValue1);
  }
};



template<typename T1ret, typename T2ret>
class TemplateParser_2ret_0: public Parser
{
public:
  boost::signals2::signal<void (T1ret&, T2ret&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & /*message*/)
  {

    T1ret returnValue1;
    T2ret returnValue2;
    callWorkerSignal(returnValue1,returnValue2);

    pushFront(returnMessage, returnValue2);
    pushFront(returnMessage, returnValue1);
  }
};


template<typename T1ret, typename T2ret, typename T1>
class TemplateParser_2ret_1: public Parser
{
public:
  boost::signals2::signal<void (T1ret&, T2ret&, const T1&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & message)
  {

    T1 value1;
    popFront(message, value1);

    T1ret returnValue1;
    T2ret returnValue2;
    callWorkerSignal(returnValue1,returnValue2, value1);

    pushFront(returnMessage, returnValue2);
    pushFront(returnMessage, returnValue1);
  }
};


template<typename T1ret, typename T2ret, typename T1, typename T2>
class TemplateParser_2ret_2: public Parser
{
public:
  boost::signals2::signal<void (T1ret&, T2ret&, const T1&, const T2&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & message)
  {

    T1 value1;
    T2 value2;
    popFront(message, value1);
    popFront(message, value2);

    T1ret returnValue1;
    T2ret returnValue2;
    callWorkerSignal(returnValue1,returnValue2, value1, value2);

    pushFront(returnMessage, returnValue2);
    pushFront(returnMessage, returnValue1);
  }
};


template<typename T1ret, typename T2ret, typename T3ret, typename T1>
class TemplateParser_3ret_1: public Parser
{
public:
  boost::signals2::signal<void (T1ret&, T2ret&, T3ret&, const T1&)> callWorkerSignal;

  void parse(Message & returnMessage, Message & message)
  {

    T1 value1;
    popFront(message, value1);

    T1ret returnValue1;
    T2ret returnValue2;
    T3ret returnValue3;
    callWorkerSignal(returnValue1,returnValue2,returnValue3, value1);

    pushFront(returnMessage, returnValue3);
    pushFront(returnMessage, returnValue2);
    pushFront(returnMessage, returnValue1);
  }
};


#endif // _templateparser_h
