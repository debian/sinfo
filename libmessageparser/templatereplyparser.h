#ifndef _templatereplyparser_h
#define _templatereplyparser_h


#include <boost/signals2.hpp>
#include "replyparser.h"


class TemplateReplyParser_0 : public ReplyParser
{
public:
  boost::signals2::signal<void ()> callWorkerSignal;

  void parse(Message & /*replyMessage*/)
  {
    callWorkerSignal();
  }
};


template<typename T1>
class TemplateReplyParser_1 : public ReplyParser
{
public:
  boost::signals2::signal<void (const T1&)> callWorkerSignal;

  void parse(Message & replyMessage)
  {
    T1 value1;
    popFront(replyMessage, value1);

    callWorkerSignal(value1);
  }
};


template<typename T1, typename T2>
class TemplateReplyParser_2 : public ReplyParser
{
public:
  boost::signals2::signal<void (const T1&, const T2&)> callWorkerSignal;

  void parse(Message & replyMessage)
  {
    T1 value1;
    T2 value2;
    popFront(replyMessage, value1);
    popFront(replyMessage, value2);

    callWorkerSignal(value1,value2);
  }
};


template<typename T1, typename T2, typename T3>
class TemplateReplyParser_3 : public ReplyParser
{
public:
  boost::signals2::signal<void (const T1&, const T2&, const T3&)> callWorkerSignal;

  void parse(Message & replyMessage)
  {
    T1 value1;
    T2 value2;
    T3 value3;
    popFront(replyMessage, value1);
    popFront(replyMessage, value2);
    popFront(replyMessage, value3);

    callWorkerSignal(value1,value2,value3);
  }
};


#endif // _templatereplyparser_h
