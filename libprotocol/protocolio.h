#ifndef _protocolio_h
#define _protocolio_h

#include "thetimerobjectbase.h"
#include <boost/date_time/posix_time/posix_time.hpp>


/**
* @brief this base class hides, if the actual state machine is used
*        by a simulation or by a real time implementation.
*
*/
class ProtocolIO
{
public:
  virtual boost::posix_time::ptime currentTimeUTC() = 0;
  virtual TheTimerObjectBase * getTimer() = 0;
};


#endif // _protocolio_h
