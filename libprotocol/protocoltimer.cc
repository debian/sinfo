#include "protocoltimer.h"
#include "protocolio.h"
#include "thetimerobjectbase.h"


ProtocolTimer::ProtocolTimer(ProtocolIO & protocolIO) : theTimerObjectBase(protocolIO.getTimer())
{
  myHandlerBase=0;
}


ProtocolTimer::~ProtocolTimer()
{
  delete myHandlerBase;
  myHandlerBase=0;
  delete theTimerObjectBase;
}


void ProtocolTimer::stop()
{
  theTimerObjectBase->stop();
  delete myHandlerBase;
  myHandlerBase=0;
}
