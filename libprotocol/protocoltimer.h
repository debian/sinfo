#ifndef _protocoltimer_h
#define _protocoltimer_h

#include "protocolio.h"
#include "thetimerobjectbase.h"
#include <boost/date_time/posix_time/posix_time.hpp>


/**
* @brief the timer link class to be uses within state machines
*
*/
class ProtocolTimer
{
private:
  TimerEventFunctor * myHandlerBase;
  TheTimerObjectBase * theTimerObjectBase;

public:
  ProtocolTimer(ProtocolIO & protocolIO);
  ~ProtocolTimer();

  template <typename TClass>
  void startAlarmAt(const boost::posix_time::ptime & time, TClass handler)
  {
    if (myHandlerBase)
    {
      delete myHandlerBase;
      myHandlerBase=0;
    }
    myHandlerBase = new TimerEventFunctor(handler);

    theTimerObjectBase->startAlarmAt(time, myHandlerBase);
  }

  template <typename TClass>
  void startAlarmAfter(const boost::posix_time::time_duration & expiry_time, TClass handler)
  {
    if (myHandlerBase)
    {
      delete myHandlerBase;
      myHandlerBase=0;
    }
    myHandlerBase = new TimerEventFunctor(handler);

    theTimerObjectBase->startAlarmAfter(expiry_time, myHandlerBase);
  }

  void stop();

  bool isRunning()
  {
    return theTimerObjectBase->isRunning();
  }
};


#endif // _protocoltimer_h
