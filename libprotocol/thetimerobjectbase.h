#ifndef _thetimerobjectbase_h
#define _thetimerobjectbase_h

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/function.hpp>


typedef boost::function< void() > TimerEventFunctor;


/**
* @brief base class to manipulate a timer.
* This class can be implemented for operating system or simulated timers.
*
*/
class TheTimerObjectBase
{
protected:
  TimerEventFunctor * myFunctorPointer;

public:
  virtual ~TheTimerObjectBase() {};
  virtual void startAlarmAt(const boost::posix_time::ptime & time, TimerEventFunctor * functor)=0;
  virtual void startAlarmAfter(const boost::posix_time::time_duration & expiry_time, TimerEventFunctor * functor)=0;
  virtual void stop()=0;
  virtual bool isRunning()=0;
};


#endif // _thetimerobjectbase_h
