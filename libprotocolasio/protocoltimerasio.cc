#include <boost/bind.hpp>
#include "protocoltimerasio.h"
#include "thetimerobjectasio.h"


ProtocolIOAsio::ProtocolIOAsio(boost::asio::io_service & ioservice) : ioservice(ioservice)
{
}


boost::posix_time::ptime ProtocolIOAsio::currentTimeUTC()
{
  return boost::posix_time::microsec_clock::universal_time();
}


TheTimerObjectBase * ProtocolIOAsio::getTimer()
{
  return new TheTimerObjectAsio(ioservice);
}
