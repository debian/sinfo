#ifndef _protocoltimerasio_h
#define _protocoltimerasio_h


#include "protocoltimer.h"
#include <boost/asio.hpp>


/**
* @brief asio variant of the ProtocolIO base class
*
* Keeps the reference to boost::asio::ioservice in order
* to allow making use of the  asio timers.
*
* This class has to be used in order to make a program use
* the operating system / asio clock.
*/
class ProtocolIOAsio : public ProtocolIO
{
private:
  boost::asio::io_service & ioservice;

public:
  ProtocolIOAsio(boost::asio::io_service & ioservice);

  boost::posix_time::ptime currentTimeUTC();
  TheTimerObjectBase * getTimer();
};


#endif // _protocoltimerasio_h
