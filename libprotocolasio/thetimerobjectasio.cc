#include <boost/bind.hpp>
#include "protocoltimerasio.h"
#include "thetimerobjectasio.h"


void TheTimerObjectAsio::timerExpiredEvent(const boost::system::error_code& err)
{
  if (boost::asio::error::operation_aborted==err)
  {
  }
  else
  {
    timerActive=false;
    if (myFunctorPointer)
    {
      (*myFunctorPointer)();
    }
  }
}


TheTimerObjectAsio::TheTimerObjectAsio(boost::asio::io_service & ioservice) : timer(ioservice)
{
  timerActive=false;
  myFunctorPointer=0;
}


TheTimerObjectAsio::~TheTimerObjectAsio()
{
  stop();
}


void TheTimerObjectAsio::startAlarmAt(const boost::posix_time::ptime & time, TimerEventFunctor * functor)
{
  myFunctorPointer=functor;
  timer.expires_at(time);
  timer.async_wait(boost::bind(&TheTimerObjectAsio::timerExpiredEvent, this, boost::asio::placeholders::error));
  timerActive=true;
}


void TheTimerObjectAsio::startAlarmAfter(const boost::posix_time::time_duration & expiry_time, TimerEventFunctor * functor)
{
  myFunctorPointer=functor;
  timer.expires_from_now(expiry_time);
  timer.async_wait(boost::bind(&TheTimerObjectAsio::timerExpiredEvent, this, boost::asio::placeholders::error));
  timerActive=true;
}


void TheTimerObjectAsio::stop()
{
  if (timerActive)
  {
    timer.cancel();
    timerActive=false;
  }
}

bool TheTimerObjectAsio::isRunning()
{
  return timerActive;
}
