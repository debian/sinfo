#ifndef _thetimerobjectasio_h
#define _thetimerobjectasio_h


#include "protocoltimer.h"


/**
* @brief asio variant of the TheTimerObjectBase base class
*/
class TheTimerObjectAsio : public TheTimerObjectBase
{
private:
  bool timerActive;
  boost::asio::deadline_timer timer;

  void timerExpiredEvent(const boost::system::error_code& err);

public:
  TheTimerObjectAsio(boost::asio::io_service & ioservice);
  ~TheTimerObjectAsio();

  void startAlarmAt(const boost::posix_time::ptime & time, TimerEventFunctor * functor);
  void startAlarmAfter(const boost::posix_time::time_duration & expiry_time, TimerEventFunctor * functor);
  void stop();
  bool isRunning();
};


#endif // _thetimerobjectasio_h
