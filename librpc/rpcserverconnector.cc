#include <boost/bind.hpp>
#include <iostream>
#include "messagebasictypes.h"
#include "messagestlcontainertypes.h"
#include "rpcserverconnector.h"
using namespace std;
using namespace Msg;


RPCServerConnector::RPCServerConnector(ReceiveMessageSignal & receiveMessageSignal): receiveMessageSignal(receiveMessageSignal)
{
}

RPCServerConnector::~RPCServerConnector()
{
}


void RPCServerConnector::receiveMessageSlot(Message message)
{
  cout << "void RPCServerConnector::receiveMessageSlot(Message message)" << endl;


  Message returnMessage;

  // call server functions
  receiveMessageSignal(returnMessage,message);

  sendMessageSignal(returnMessage);
}
