#ifndef _rpcserverconnector_h
#define _rpcserverconnector_h


#include "messageserver.h"
#include "serverconnectorbase.h"


class RPCServerConnector : public ServerConnectorBase
{
private:
  void receiveMessageSlot(Message message);
  ReceiveMessageSignal & receiveMessageSignal; // to RPC

public:
  RPCServerConnector(ReceiveMessageSignal & receiveMessageSignal);
  ~RPCServerConnector();
};


#endif // _rpcserverconnector_h
