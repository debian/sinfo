#include "rpcserverconnectorfactory.h"
#include "rpcserverconnector.h"


RPCServerConnectorFactory::RPCServerConnectorFactory()
{
}


RPCServerConnectorFactory::~RPCServerConnectorFactory()
{
}


boost::shared_ptr<ServerConnectorBase> RPCServerConnectorFactory::createServerConnector()
{
  return boost::shared_ptr<ServerConnectorBase>(new RPCServerConnector(receiveMessageSignal));
}

