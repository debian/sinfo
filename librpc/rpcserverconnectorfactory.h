#ifndef _rpcserverconnectorfactory_h
#define _rpcserverconnectorfactory_h

#include "serverconnectorfactorybase.h"
#include "messageserver.h"


class RPCServerConnectorFactory : public ServerConnectorFactoryBase, public MessageServer
{
public:
  RPCServerConnectorFactory();
  ~RPCServerConnectorFactory();


  virtual boost::shared_ptr<ServerConnectorBase> createServerConnector();
};


#endif // _rpcserverconnectorfactory_h
