#include <stdio.h>


#ifdef __sun__
#include <errno.h>
#include <kstat.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/swap.h>
#endif

#ifdef __FreeBSD__
#include <fcntl.h>
#include <kvm.h>
#endif

#include <iostream>
#include <string.h>

#include "cpuinfometer.h"


bool getCpuinfo(Cpuinfo & cpuinfo)
{
  cpuinfo.cpus = 0;
  cpuinfo.speedmhz = 0;


#ifdef __linux__
  FILE *fcinfo;
  if ( (fcinfo = fopen("/proc/cpuinfo", "r") ) != NULL)
  {
#ifdef CPUNO_ADJUST
    int siblings = 1;
    int cores = 1;
#endif
    while (1)
    {
      char line[401];
      int fsret = fscanf(fcinfo, "%400[^\n]\n",line);
      if (fsret == EOF)
        break;

      char entry[201];
      char value[201];
      fsret = sscanf(line, "%200[^\t:]%*[\t: ]%200[^\n]", entry, value);

//        cout << "entry \"" << entry << "\"" << endl
//             << " value \"" << value << "\"" << endl;

      if (fsret == 2)
      {
        // interpret the fscanned data
        if (strcmp(entry, "processor") == 0)
        {
          cpuinfo.cpus++;
        }

        if (strcmp(entry, "cpu MHz") == 0)
        {
          sscanf(value, "%f", &cpuinfo.speedmhz);
        }

#ifdef CPUNO_ADJUST
        if (strcmp(entry, "siblings") == 0)
        {
          sscanf(value, "%d", &siblings);
        }
        if (strcmp(entry, "cpu cores") == 0)
        {
          sscanf(value, "%d", &cores);
        }
#endif

        // for  PowerPC970, contributed by Francois Thomas <FT@fr.ibm.com>
        if (strcmp(entry, "clock") == 0)
        {
          sscanf(value, "%fMHz", &cpuinfo.speedmhz);
        }
      }
    }

#ifdef CPUNO_ADJUST
    cpuinfo.cpus = cpuinfo.cpus * cores / siblings;
#endif
    fclose(fcinfo);
    return true;
  }
#endif



#ifdef __FreeBSD__

  // found hint to the kernal-variabel "tsc_freq" in
  //   /usr/src/sys/i386/i386/identcpu.c
  // that�s part of the kernel startup function

  kvm_t *kd;

  if ((kd = kvm_open(NULL, NULL, NULL, O_RDONLY, "kvm_open")) == NULL)
  {
    fprintf(stderr, "kvm_open failed\n");
    return false;
  }

  struct nlist nlst[] =
  {
#define X_TSC_FREQ  0
    { "_tsc_freq" },
    { 0 }
  };

  kvm_nlist(kd, nlst);
  if (nlst[0].n_type == 0)
  {
    std::cerr << "kvm_nlist failed: " << std::endl;
    return false;
  }

  unsigned int mytsc_freq;
  if (kvm_read(kd, nlst[X_TSC_FREQ].n_value,
               &mytsc_freq, sizeof(mytsc_freq)) != sizeof(mytsc_freq))
  {
    std::cerr << "kvm_read failed: " << kvm_geterr(kd) << std::endl;
    return false;
  }

  kvm_close(kd);


  cpuinfo.cpus = 1;
  // FIXME: I don�t have acces to a multi processor FreeBSD system
  // so I�ll assume, that there is at least one CPU ....

  std::cout << "mytsc_freq= " << (mytsc_freq + 4999.) / 1e6 << std::endl;
  cpuinfo.speedmhz = (mytsc_freq + 4999.) / 1e6;

  return true;
#endif


#ifdef __sun__
  struct kstat_ctl *kc;
  if ((kc = kstat_open()) == 0)
  {
    fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
            "This library is used for accessing kernel information.\n"
            "The diagnostics are: `%s'.\n\n", strerror(errno));
  }
  else
  {

    // CPUS z�hlen
    for (kstat_t * ksp = kc->kc_chain; ksp != 0; ksp = ksp->ks_next)
    {
      if (strncmp(ksp->ks_name, "cpu_info", 8) == 0)
      {
        cpuinfo.cpus++;

        if (kstat_read(kc, ksp, 0) != -1)
        {
          kstat_named_t *kn = (kstat_named_t *)ksp->ks_data;
          for (int i = 0; i < (int) ksp->ks_ndata; i++)
          {
            std::cout << kn->name << std::endl;
            if (strcmp(kn->name, "clock_MHz") == 0)
            {
              cpuinfo.speedmhz = kn->value.ul;
              break;
            }
            kn++;
          }
        }
      }
    }
    if (kc != 0)
      kstat_close(kc);
    return true;
  }
#endif

  return false;
}
