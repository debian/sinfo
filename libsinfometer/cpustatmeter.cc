#include <stdio.h>
#include <iostream>

#ifdef __sun__
#include <errno.h>
#include <kstat.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/swap.h>
#endif

#ifdef __FreeBSD__
#include <fcntl.h>
#include <kvm.h>
#include <sys/dkstat.h>
#endif


#include "cpustatmeter.h"


CpustatMeter::CpustatMeter()
{
  last_user = 0;
  last_nice = 0;
  last_sys = 0;
  last_idle = 0;
  last_iowait = 0;
  last_irq = 0;
  last_softirq = 0;
}




#ifdef __linux__
bool CpustatMeter::getCpustat(Cpustat & cpustat)
{
  FILE *fstat;
  if ( (fstat = fopen("/proc/stat", "r") ) != NULL)
  {
    long long user, nice, sys, idle, iowait, irq, softirq;
    user = nice = sys = idle = iowait = irq = softirq = 0;

    // no testing of return value should make it work for linux 2.4 and 2.4
    // not 2.4 has only 4 states: user, nice, sys, idle
    // while 2.6 has 7 states: user, nice, sys, idle, iowait, irq, softirq
    fscanf(fstat, "%*s %Ld %Ld %Ld %Ld %Ld %Ld %Ld",
           &user,
           &nice,
           &sys,
           &idle,
           &iowait,
           &irq,
           &softirq);
    fclose(fstat);


    long long d_user = user - last_user;
    long long d_nice = nice - last_nice;
    long long d_sys = sys - last_sys;
    long long d_idle = idle - last_idle;
    long long d_iowait = iowait - last_iowait;
    long long d_irq = irq - last_irq;
    long long d_softirq = softirq - last_softirq;

    long long d_total = d_user + d_sys + d_nice + d_idle + d_iowait + d_irq + d_softirq;

    cpustat.userp = float(d_user) / float(d_total);
    cpustat.sysp = float(d_sys) / float(d_total);
    cpustat.nicep = float(d_nice) / float(d_total);
    cpustat.idlep = float(d_idle) / float(d_total);
    cpustat.iowaitp = float(d_iowait) / float(d_total);
    cpustat.irqp = float(d_irq) / float(d_total);
    cpustat.softirqp = float(d_softirq) / float(d_total);

    if (cpustat.userp>1.)
      cpustat.userp=1.;
    if (cpustat.sysp>1.)
      cpustat.sysp=1.;
    if (cpustat.nicep>1.)
      cpustat.nicep=1.;
    if (cpustat.idlep>1.)
      cpustat.idlep=1.;
    if (cpustat.iowaitp>1.)
      cpustat.iowaitp=1.;
    if (cpustat.irqp>1.)
      cpustat.irqp=1.;
    if (cpustat.softirqp>1.)
      cpustat.softirqp=1.;

    last_user = user;
    last_nice = nice;
    last_sys = sys;
    last_idle = idle;
    last_iowait = iowait;
    last_irq = irq;
    last_softirq = softirq;

    return true;
  }
  return false;
}
#endif


#ifdef __FreeBSD__
bool CpustatMeter::getCpustat(Cpustat & cpustat)
{
  kvm_t *kd;

  if ((kd = kvm_open(NULL, NULL, NULL, O_RDONLY, "kvm_open")) == NULL)
  {
    fprintf(stderr, "kvm_open failed\n");
    return false;
  }

  struct nlist nlst[] =
  {
#define X_CP_TIME 0
    { "_cp_time" },
    { 0 }
  };

  kvm_nlist(kd, nlst);
  if (nlst[0].n_type == 0)
  {
    fprintf(stderr, "kvm_nlist failed\n");
    return false;
  }

  long cp_time[CPUSTATES];
  if (kvm_read(kd, nlst[X_CP_TIME].n_value,
               cp_time, sizeof(cp_time)) != sizeof(cp_time))
  {
    fprintf(stderr, "kvm_read failed\n");
    return false;
  }

  kvm_close(kd);

  long user = cp_time[0];
  long nice = cp_time[1];
  long sys = cp_time[2];
  long irq = cp_time[3];
  long idle = cp_time[4];

  long d_user = user - last_user;
  long d_nice = nice - last_nice;
  long d_sys = sys - last_sys;
  long d_idle = idle - last_idle;
  long d_iowait = 0;
  long d_irq = irq - last_irq;
  long d_softirq = 0;

  long d_total = d_user + d_sys + d_nice + d_idle + d_iowait + d_irq + d_softirq;

  cpustat.userp = float(d_user) / float(d_total);
  cpustat.sysp = float(d_sys) / float(d_total);
  cpustat.nicep = float(d_nice) / float(d_total);
  cpustat.idlep = float(d_idle) / float(d_total);
  cpustat.iowaitp = float(d_iowait) / float(d_total);
  cpustat.irqp = float(d_irq) / float(d_total);
  cpustat.softirqp = float(d_softirq) / float(d_total);

  if (cpustat.userp>1.)
    cpustat.userp=1.;
  if (cpustat.sysp>1.)
    cpustat.sysp=1.;
  if (cpustat.nicep>1.)
    cpustat.nicep=1.;
  if (cpustat.idlep>1.)
    cpustat.idlep=1.;
  if (cpustat.iowaitp>1.)
    cpustat.iowaitp=1.;
  if (cpustat.irqp>1.)
    cpustat.irqp=1.;
  if (cpustat.softirqp>1.)
    cpustat.softirqp=1.;

  last_user = user;
  last_nice = nice;
  last_sys = sys;
  last_idle = idle;
  last_irq = irq;

  return true;
}
#endif


#ifdef __sun__
bool CpustatMeter::getCpustat(Cpustat & cpustat)
{
  struct kstat_ctl *kc;
  if ((kc = kstat_open()) == 0)
  {
    fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
            "This library is used for accessing kernel information.\n"
            "The diagnostics are: `%s'.\n\n", strerror(errno));
  }
  else
  {
    long user = 0;
    long nice = 0;
    long sys = 0;
    long idle = 0;

    // CPUS z�hlen
    int cpus = 0;
    for (kstat_t * ksp = kc->kc_chain; ksp != 0; ksp = ksp->ks_next)
    {
      if (strncmp(ksp->ks_name, "cpu_stat", 8) == 0)
      {
        cpus++;
        cpu_stat_t cstat;
        if (kstat_read(kc, ksp, 0) == -1 ||         // update from kernel
            kstat_read(kc, ksp, &cstat) == -1) // and read into buffer
          fprintf(stderr, "Sorry, I cannot read the CPU statistics entry\n"
                  "from the `kstat' library. The diagnostics are `%1'.\n\n",
                  strerror(errno));

        // fields are: idle user kernel iowait
        idle += cstat.cpu_sysinfo.cpu[0];
        user += cstat.cpu_sysinfo.cpu[1];
        sys += cstat.cpu_sysinfo.cpu[2];
        nice += cstat.cpu_sysinfo.cpu[3];
      }
    }
    kstat_close(kc);

    long d_user = user - last_user;
    long d_nice = nice - last_nice;
    long d_sys = sys - last_sys;
    long d_idle = idle - last_idle;
    long d_iowait = 0;
    long d_irq = 0;
    long d_softirq = 0;

    long d_total = d_user + d_sys + d_nice + d_idle + d_iowait + d_irq + d_softirq;

    cpustat.userp = float(d_user) / float(d_total);
    cpustat.sysp = float(d_sys) / float(d_total);
    cpustat.nicep = float(d_nice) / float(d_total);
    cpustat.idlep = float(d_idle) / float(d_total);
    cpustat.iowaitp = float(d_iowait) / float(d_total);
    cpustat.irqp = float(d_irq) / float(d_total);
    cpustat.softirqp = float(d_softirq) / float(d_total);

    if (cpustat.userp>1.)
      cpustat.userp=1.;
    if (cpustat.sysp>1.)
      cpustat.sysp=1.;
    if (cpustat.nicep>1.)
      cpustat.nicep=1.;
    if (cpustat.idlep>1.)
      cpustat.idlep=1.;
    if (cpustat.iowaitp>1.)
      cpustat.iowaitp=1.;
    if (cpustat.irqp>1.)
      cpustat.irqp=1.;
    if (cpustat.softirqp>1.)
      cpustat.softirqp=1.;

    last_user = user;
    last_nice = nice;
    last_sys = sys;
    last_idle = idle;

    return true;
  }
  return false;
}
#endif
