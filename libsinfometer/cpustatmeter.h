#ifndef _cpustatmeter_h
#define _cpustatmeter_h


#include "cpustat.h"

class CpustatMeter
{
private:
  long long last_user;
  long long last_nice;
  long long last_sys;
  long long last_idle;
  long long last_iowait;
  long long last_irq;
  long long last_softirq;

public:
  CpustatMeter();
  bool getCpustat(Cpustat & cpustat);
};


#endif // _cpustatmeter_h
