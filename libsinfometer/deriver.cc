#include "deriver.h"


Deriver::Deriver()
{
  lastValue=0.;
  lastIsValid=false;
  currentValue=0.;
  currentIsValid=false;
}


void  Deriver::setCurrentValue(boost::posix_time::ptime theCurrentTime, double value)
{
  lastValue=currentValue;
  lastTime=currentTime;
  lastIsValid=currentIsValid;

  currentValue=value;
  currentTime=theCurrentTime;
  currentIsValid=true;
}


double Deriver::getDerivation(boost::posix_time::ptime theCurrentTime)
{
  if ((false==lastIsValid) || (false==currentIsValid))
  {
    return 0.;
  }

  boost::posix_time::time_duration elapsedTime=currentTime-lastTime;
  return (currentValue-lastValue) / (double(elapsedTime.total_microseconds())/1e6);
}


double Deriver::setCurrentValueAndGetDerivation(boost::posix_time::ptime theCurrentTime, double value)
{
  setCurrentValue(theCurrentTime, value);
  return getDerivation(theCurrentTime);
}



DeriverWithTimer::DeriverWithTimer()
{
}


void  DeriverWithTimer::setCurrentValue(double value)
{
  Deriver::setCurrentValue(boost::posix_time::microsec_clock::universal_time(), value);
}


double DeriverWithTimer::getDerivation()
{
  return Deriver::getDerivation(boost::posix_time::microsec_clock::universal_time());
}


double DeriverWithTimer::setCurrentValueAndGetDerivation(double value)
{
  return Deriver::setCurrentValueAndGetDerivation(boost::posix_time::microsec_clock::universal_time(), value);
}
