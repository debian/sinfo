#ifndef _deriver_h
#define _deriver_h


#include <boost/date_time/posix_time/posix_time.hpp>


class Deriver
{
private:
  double lastValue;
  boost::posix_time::ptime lastTime;
  bool lastIsValid;

  double currentValue;
  boost::posix_time::ptime currentTime;
  bool currentIsValid;

public:
  Deriver();

  void   setCurrentValue(boost::posix_time::ptime theCurrentTime, double value);
  double getDerivation(boost::posix_time::ptime theCurrentTime);

  double setCurrentValueAndGetDerivation(boost::posix_time::ptime theCurrentTime, double value);
};



class DeriverWithTimer : private Deriver
{
private:

public:
  DeriverWithTimer();

  void   setCurrentValue(double value);
  double getDerivation();

  double setCurrentValueAndGetDerivation(double value);
};


#endif // _deriver_h
