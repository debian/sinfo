#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include "diskloadmeter.h"
using namespace std;


DiskloadMeter::DiskloadMeter()
{
}


#ifdef __linux__
bool DiskloadMeter::getDiskload(Diskload & diskload)
{
  FILE *fcinfo;
  if ( (fcinfo = fopen("/proc/vmstat", "r") ) != NULL)
  {
    int entriesFound=0;
    long readkbytes=0;
    long writekbytes=0;
    while (true)
    {
      char entry[200];
      long value;

      // FIXME: limit the value of scanned chars to prevent buffer overruns
      int fsret = fscanf(fcinfo, "%[^\t ]%*[\t ]%ld\n", entry, &value);
      if (fsret == EOF)
        break;

      if (fsret == 2)
      {
        // interpret the fscanned data
        if (0 == strcmp(entry, "pgpgin"))
        {
          readkbytes=value;
          entriesFound++;
        }

        if (0 == strcmp(entry, "pgpgout"))
        {
          writekbytes=value;
          entriesFound++;
        }
      }
    }
    fclose(fcinfo);

    if (2==entriesFound)
    {
      diskload.readkbytespersec =readKBytesDeriver.setCurrentValueAndGetDerivation(readkbytes);
      diskload.writekbytespersec=writeKBytesDeriver.setCurrentValueAndGetDerivation(writekbytes);

      return true;
    }
  }
  return false;
}
#endif



#ifdef __sun__
bool DiskloadMeter::getDiskload(Diskload & diskload)
{
  diskload.readkbytespersec = -1.;
  diskload.writekbytespersec = -1.;
  return false;
}
#endif


#ifdef __FreeBSD__
bool DiskloadMeter::getDiskload(Diskload & diskload)
{
  diskload.readkbytespersec = -1.;
  diskload.writekbytespersec = -1.;
  return false;
}
#endif
