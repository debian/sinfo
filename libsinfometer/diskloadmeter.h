#ifndef _diskloadmeter_h
#define _diskloadmeter_h

#include <sys/time.h>
#include "diskload.h"
#include "deriver.h"


class DiskloadMeter
{
private:
  DeriverWithTimer readKBytesDeriver;
  DeriverWithTimer writeKBytesDeriver;

public:
  DiskloadMeter();

  bool getDiskload(Diskload & diskload);
};


#endif // _diskloadmeter_h
