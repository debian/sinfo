#include <stdio.h>

#ifdef __sun__
#include <limits.h>
#include <errno.h>
#include <kstat.h>
#include <string.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/swap.h>
#endif

#ifdef __FreeBSD__
#include <fcntl.h>
#include <kvm.h>
#include <sys/dkstat.h>
#include <sys/time.h>
#include <sys/resource.h>
#endif

#include "loadavgmeter.h"



#ifdef __sun__
float getscaled(kstat_t *ks, const char *name)
{
  // load avgs are scaled by 256
  kstat_named_t *kn = (kstat_named_t *)kstat_data_lookup(ks, (char *)name);
  return kn ? kn->value.ui32 * (1 / 256.0) : 0.0;
}
#endif


bool getLoadavg(Loadavg & loadavg)
{
#ifdef __linux__
  FILE *fload;
  if ( (fload = fopen("/proc/loadavg", "r") ) != NULL)
  {
    fscanf(fload, "%f %f %f",
           &loadavg.load1,
           &loadavg.load5,
           &loadavg.load15);

    fclose(fload);
    return true;
  }
#endif



#ifdef __FreeBSD__
  kvm_t *kd;
  if ((kd = kvm_open(NULL, NULL, NULL, O_RDONLY, "kvm_open")) == NULL)
  {
    fprintf(stderr, "kvm_open failed\n");
    return false;
  }

  struct nlist nlst[] =
  {
#define X_AVENRUN 0
    { "_averunnable" },
    { 0 }
  };

  kvm_nlist(kd, nlst);
  if (nlst[0].n_type == 0)
  {
    fprintf(stderr, "kvm_nlist failed\n");
    return false;
  }

  struct loadavg avernrun;
  if (kvm_read(kd, nlst[X_AVENRUN].n_value,
               &avernrun, sizeof(avernrun)) != sizeof(avernrun))
  {
    fprintf(stderr, "kvm_read failed\n");
    return false;
  }

  kvm_close(kd);

  loadavg.load1 = double(avernrun.ldavg[0]) / double(avernrun.fscale);
  loadavg.load5 = double(avernrun.ldavg[1]) / double(avernrun.fscale);
  loadavg.load15 = double(avernrun.ldavg[2]) / double(avernrun.fscale);

  return true;
#endif



#ifdef __sun__
  struct kstat_ctl *kc;
  if ((kc = kstat_open()) == 0)
  {
    fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
            "This library is used for accessing kernel information.\n"
            "The diagnostics are: `%s'.\n\n", strerror(errno));
  }
  else
  {

    kstat_t *ks = kstat_lookup(kc, "unix", 0, "system_misc");
    if (!ks || kstat_read(kc, ks, 0) == -1)
    {
      perror("kstat_lookup/read");
    }
    else
    {

      loadavg.load1 = getscaled(ks, "avenrun_1min");
      loadavg.load5 = getscaled(ks, "avenrun_5min");
      loadavg.load15 = getscaled(ks, "avenrun_15min");

      kstat_close(kc);
      return true;
    }
  }
#endif

  return false;
}
