#ifndef _loadavgmeter_h
#define _loadavgmeter_h


#include "loadavg.h"


bool getLoadavg(Loadavg & loadavg);


#endif // _loadavgmeter_h
