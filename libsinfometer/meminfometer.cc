#include <stdio.h>
#include <iostream>

#include "meminfometer.h"

#ifdef __sun__
#include <limits.h>
#include <errno.h>
#include <kstat.h>
#include <string.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/swap.h>
#endif

#ifdef __FreeBSD__
#include <fcntl.h>
#include <kvm.h>
#include <sys/dkstat.h>
#include <sys/vmmeter.h>
#endif

#ifdef __linux__
#include <sys/sysinfo.h>
#endif

bool getMeminfo(Meminfo & meminfo)
{
#ifdef __linux__
  /*
    FILE *fmem;
    if ( (fmem = fopen("/proc/meminfo", "r") ) != NULL)
    {
      fscanf(fmem, "%*s %*s %*s %*s %*s %*s");

      fscanf(fmem, "%*s %lu %lu %lu %lu %lu %lu",
             &meminfo.mem_total,
             &meminfo.mem_used,
             &meminfo.mem_free,
             &meminfo.mem_shared,
             &meminfo.mem_buffers,
             &meminfo.mem_cached);

      fscanf(fmem, "%*s %lu %lu %lu",
             &meminfo.swap_total,
             &meminfo.swap_used,
             &meminfo.swap_free);


      fclose(fmem);

      return true;
    }
  */

  // alternative interface
  struct sysinfo info;
  sysinfo(&info);

  meminfo.mem_total = float(info.totalram) * float(info.mem_unit);
  meminfo.mem_free = float(info.freeram) * float(info.mem_unit);
  meminfo.mem_used = meminfo.mem_total - meminfo.mem_free;

  meminfo.swap_total = float(info.totalswap) * float(info.mem_unit);
  meminfo.swap_free = float(info.freeswap) * float(info.mem_unit);
  meminfo.swap_used = meminfo.swap_total - meminfo.swap_free;
  /*
    cout << "meminfo.swap_total " << meminfo.swap_total << endl
    << "meminfo.swap_used " << meminfo.swap_used << endl
    << "meminfo.swao_free " << meminfo.swap_free << endl;
  */
  return true;
#endif


#ifdef __FreeBSD__

  kvm_t *kd;
  if ((kd = kvm_open(NULL, NULL, NULL, O_RDONLY, "kvm_open")) == NULL)
  {
    fprintf(stderr, "kvm_open failed\n");
    return false;
  }

  struct nlist nlst[] =
  {
#define X_CNT 0
    { "_cnt" },
    { 0 }
  };

  kvm_nlist(kd, nlst);
  if (nlst[0].n_type == 0)
  {
    fprintf(stderr, "kvm_nlist failed\n");
    return false;
  }

  struct vmmeter sum;
  if (kvm_read(kd, nlst[X_CNT].n_value,
               &sum, sizeof(sum)) != sizeof(sum))
  {
    fprintf(stderr, "kvm_read failed\n");
    return false;
  }



  struct kvm_swap swapary[1];
  int n = kvm_getswapinfo(kd, swapary, 1, 0);
  if (n < 0 || swapary[0].ksw_total == 0)
    return false;


  kvm_close(kd);
  meminfo.mem_total = float(sum.v_page_size) * float(sum.v_page_count);
  meminfo.mem_free = float(sum.v_page_size) * float(sum.v_free_count);
  meminfo.mem_used = meminfo.mem_total - meminfo.mem_free;

  meminfo.swap_total = float(sum.v_page_size) * float(swapary[0].ksw_total);
  meminfo.swap_used = float(sum.v_page_size) * float(swapary[0].ksw_used);
  meminfo.swap_free = meminfo.swap_total - meminfo.swap_used;
  return true;
#endif

#ifdef __sun__

  struct kstat_ctl *kc;
  if ((kc = kstat_open()) == 0)
  {
    fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
            "This library is used for accessing kernel information.\n"
            "The diagnostics are: `%s'.\n\n", strerror(errno));
  }
  else
  {
    kstat_t * ksp;

    // availrmem = pages of core for user-proc ( == physmem - kernelmem)
    // freemem = no of free pages

    // physmem == total mem in 4KB blocks

    errno = 0;
    if ((ksp = kstat_lookup(kc, "unix", -1, "system_pages")) == 0 ||
        kstat_read(kc, ksp, 0) == -1)
      fprintf(stderr, "Sorry, I cannot read the memory statistics entry\n"
              "from the `kstat' library. The diagnostics are `%s'\n\n", strerror(errno));

    unsigned long physmem = 0;
    unsigned long freemem = 0;
    unsigned long availrmem = 0;
    kstat_named_t *kn = (kstat_named_t *)ksp->ks_data;
    for (int i = 0; i < (int) ksp->ks_ndata; i++)
    {
      if (strcmp(kn->name, "physmem") == 0)
        physmem = kn->value.ul;
      else if (strcmp(kn->name, "freemem") == 0)
        freemem = kn->value.ul;
      else if (strcmp(kn->name, "availrmem") == 0)
        availrmem = kn->value.ul;
      kn++;
    }

    if (physmem == 0)   // sanity check, this should always be > 0
      fprintf(stderr, "Uh uh, there seems to be a problem with my handling\n"
              "of the `kstat' library: I determined 0 physical memory!\n"
              "(Free memory is %ld, available memory is %ld.)\n\n", freemem, availrmem);

    if (kc != 0)
      kstat_close(kc);

    meminfo.mem_total = float(physmem) * 4096.;
    meminfo.mem_free = float(freemem) * 4096.;
    meminfo.mem_used = float(availrmem - freemem) * 4096.;

    /*
      cout << "meminfo.mem_total " << meminfo.mem_total << endl
           << "meminfo.mem_used " << meminfo.mem_used << endl
           << "meminfo.mem_free " << meminfo.mem_free << endl;
    */


    meminfo.swap_total = 0;
    meminfo.swap_used = 0;
    meminfo.swap_free = 0;

    int swapentries;
    if ((swapentries = swapctl(SC_GETNSWP, 0)) == -1)
      fprintf(stderr, "Sorry, cannot determine the number of\n"
              "swap spaces. The diagnostics are `%s'.\n\n", strerror(errno));

    if (swapentries != 0)
    {
      // 2* to get some space for padding??
      swaptbl_t *stbl = (swaptbl_t *) malloc(2 * sizeof(int) + swapentries * sizeof(struct swapent));
      if (stbl == 0)
        fprintf(stderr, "Sorry, I ran out of memory while\n"
                "trying to determine the swap usage.\n");

      char path[PATH_MAX + 1];
      stbl->swt_n = swapentries;
      for (int i = 0; i < swapentries; i++)
        stbl->swt_ent[i].ste_path = path;

      if ((swapentries = swapctl(SC_LIST, stbl)) == -1)
        fprintf(stderr, "Sorry, I cannot determine the swap usage.\n"
                "The diagnostics are `%s'.\n\n", strerror(errno));

      if (swapentries != stbl->swt_n)
        fprintf(stderr, "Strange, I requested information for\n"
                "%ld swap spaces, but only got %ld swap entries back.\n", stbl->swt_n, swapentries);


      for (int i = 0; i < swapentries; i++)
      {
        meminfo.swap_total += float(stbl->swt_ent[i].ste_pages);
        meminfo.swap_free += float(stbl->swt_ent[i].ste_free);
      }
      free(stbl);
    }

    meminfo.swap_total *= 4096.;
    meminfo.swap_free *= 4096.;
    meminfo.swap_used = meminfo.swap_total - meminfo.swap_free;

    return true;
  }

#endif

  return false;
}
