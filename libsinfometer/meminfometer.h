#ifndef _meminfometer_h
#define _meminfometer_h

#include "meminfo.h"


bool getMeminfo(Meminfo & meminfo);


#endif // _meminfometer_h
