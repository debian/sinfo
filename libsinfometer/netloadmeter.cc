#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

#ifdef __sun__
#include <time.h>
#include <limits.h>
#include <errno.h>
#include <kstat.h>
#include <string.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/swap.h>
#endif

#ifdef __FreeBSD__
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#endif

#include "netloadmeter.h"
using namespace std;


NetloadMeter::NetloadMeter(const std::string & _iface)
{
  if (0 == _iface.size())
  {
    iface[0] = 0;
    // cout << "iface not given" << endl;

  }
  else
  {
    strncpy(iface, _iface.c_str(), IFACESIZE);
    iface[IFACESIZE - 1] = 0;
    // cout << "iface given by constructor as " << _iface << endl;
  }
  selectNetIface();
  std::cout << "iface=" << iface << std::endl;
}


#ifdef __linux__
const char * NetloadMeter::selectNetIface()
{
  if (0 == iface[0])
  {
    FILE * f = fopen("/proc/net/dev", "r");
    if (f != 0)
    {
      fscanf(f, "%*[^\n]\n");
      fscanf(f, "%*[^\n]\n");

      unsigned long iface_sum = 0;
      while (1)
      {
        char thisiface[IFACESIZE] = "";
        unsigned long rxbytes, txbytes;
        int num = fscanf(f, "%[^:]:%lu %*u %*u %*u %*u %*u %*u %*u"
                         "%lu %*u %*u %*u %*u %*u %*u %*u\n",
                         thisiface, &rxbytes, &txbytes);
        if (num != 3)
          break;


        unsigned long thisiface_sum = rxbytes + txbytes;
        if (( thisiface_sum > iface_sum)
            && (strcmp(thisiface, "lo") != 0))
        {
          iface_sum = thisiface_sum;
          strncpy(iface, thisiface, IFACESIZE);
          iface[IFACESIZE - 1] = 0;
        }
      }
      fclose (f);
    }
  }
  return iface;
}


#define BUFSIZE 4096
bool NetloadMeter::getNetload(Netload & netload)
{
  // Try to open /proc/net/dev for reading.
  FILE * f = fopen("/proc/net/dev", "r");
  if (f != 0)
  {
    char buf[BUFSIZE];
    /* Read the file into a buffer. */
    int num = fread(buf, 1, BUFSIZE - 1, f);
    fclose(f);

    if (num > 0)
    {
      buf[num] = 0;

      char *pch = strstr(buf, iface);
      if (pch != 0)
      {
        // Skip the interface name.
        pch += strlen(iface);
        // Skip the ":" after the interface name.
        pch++;

        unsigned long rxbytes, rxpkt, txbytes, txpkt;
        num = sscanf(pch, "%lu %lu %*u %*u %*u %*u %*u %*u"
                     "%lu %lu %*u %*u %*u %*u %*u %*u",
                     &rxbytes, &rxpkt, &txbytes, &txpkt);


        if (4 == num)
        {
          netload.rxbytes=rxBytesDeriver.setCurrentValueAndGetDerivation(rxbytes);
          netload.rxpkt=rxPktDeriver.setCurrentValueAndGetDerivation(rxpkt);
          netload.txbytes=txBytesDeriver.setCurrentValueAndGetDerivation(txbytes);
          netload.txpkt=txPktDeriver.setCurrentValueAndGetDerivation(txpkt);
          /*
                    cout << "netload.rxbytes " << netload.rxbytes << endl;
                    cout << "netload.txbytes " << netload.txbytes << endl;
                    cout << "netload.rxpkt " << netload.rxpkt << endl;
                    cout << "netload.txpkt " << netload.txpkt << endl;
          */
          netload.iface=string(iface);
          return true;
        }
      }

    }
  }
  return false;
}
#endif



#ifdef __sun__

const char * NetloadMeter::selectNetIface()
{
  if (0 == iface[0])
  {

    struct kstat_ctl *kc;
    if ((kc = kstat_open()) == 0)
    {
      fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
              "This library is used for accessing kernel information.\n"
              "The diagnostics are: `%s'.\n\n", strerror(errno));
    }
    else
    {
      kstat_t *ks = kstat_lookup(kc, "hme", 0, "hme0");
      if ((ks) && (kstat_read(kc, ks, 0) != -1))
      {
        strncpy(iface, "hme0", IFACESIZE);
      }


      ks = kstat_lookup(kc, "le", 0, "le0");
      if ((ks) && (kstat_read(kc, ks, 0) != -1))
      {
        strncpy(iface, "le0", IFACESIZE);
      }

      kstat_close(kc);
    }

  }
  return iface;
}


bool NetloadMeter::getNetload(Netload & netload, const char * iface)
{
  struct kstat_ctl *kc;
  if ((kc = kstat_open()) == 0)
  {
    fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
            "This library is used for accessing kernel information.\n"
            "The diagnostics are: `%s'.\n\n", strerror(errno));
  }
  else
  {

    char shortiface[IFACESIZE];
    strncpy(shortiface, iface, IFACESIZE);
    shortiface[IFACESIZE - 1] = 0;

    shortiface[strlen(shortiface) - 1] = 0;

    kstat_t *ks = kstat_lookup(kc, shortiface, 0, (char*)iface);
    if (!ks || kstat_read(kc, ks, 0) == -1)
    {
      perror("kstat_lookup/read");
      kstat_close(kc);
    }
    else
    {

      kstat_named_t *kn;

      unsigned long rxbytes=0;
      unsigned long rxpkt=0;
      unsigned long txbytes=0;
      unsigned long txpkt=0;
      kn = (kstat_named_t *)kstat_data_lookup(ks, "rbytes");
      if (kn)
        rxbytes = kn->value.ui32;

      kn = (kstat_named_t *)kstat_data_lookup(ks, "obytes");
      if (kn)
        txbytes = kn->value.ui32;

      kn = (kstat_named_t *)kstat_data_lookup(ks, "ipackets");
      if (kn)
        rxpkt = kn->value.ui32;

      kn = (kstat_named_t *)kstat_data_lookup(ks, "opackets");
      if (kn)
        txpkt = kn->value.ui32;

      kstat_close(kc);

      float elapsed = nlget_elapsed_time();

      netload.rxbytes = (rxbytes - lastrxbytes) / elapsed;
      netload.txbytes = (txbytes - lasttxbytes) / elapsed;
      netload.rxpkt = (rxpkt - lastrxpkt) / elapsed;
      netload.txpkt = (txpkt - lasttxpkt) / elapsed;
      /*
        cout << "netload.rxbytes " << netload.rxbytes << endl;
        cout << "netload.txbytes " << netload.txbytes << endl;
        cout << "netload.rxpkt " << netload.rxpkt << endl;
        cout << "netload.txpkt " << netload.txpkt << endl;
      */
      lastrxbytes = rxbytes;
      lasttxbytes = txbytes;
      lastrxpkt = rxpkt;
      lasttxpkt = txpkt;

      strncpy(netload.iface, iface, IFACESIZE);
      netload.iface[IFACESIZE - 1] = 0;

      return true;
    }
  }

  return false;
}
#endif


#ifdef __FreeBSD__
const char * NetloadMeter::selectNetIface()
{
  struct ifaddrs *ifap, *ifa;
  if (getifaddrs(&ifap))
  {
    std::cerr << "error using getifaddrs" << std::endl;
    return iface;
  }

  unsigned long iface_sum = 0;
  for (ifa = ifap; ifa; ifa = ifa->ifa_next)
  {
    if (ifa->ifa_addr->sa_family != AF_LINK)
      continue;

    struct if_data * if_dataptr = (struct if_data *) ifa->ifa_data;


    unsigned long rxbytes, txbytes;
    rxbytes = if_dataptr->ifi_ibytes;
    txbytes = if_dataptr->ifi_obytes;

    unsigned long thisiface_sum = rxbytes + txbytes;
    if (( thisiface_sum > iface_sum)
        && (strcmp(ifa->ifa_name, "lo0") != 0))
    {
      iface_sum = thisiface_sum;
      strncpy(iface, ifa->ifa_name, IFACESIZE);
      iface[IFACESIZE - 1] = 0;
    }
  }
  freeifaddrs(ifap);


  return iface;
}

bool NetloadMeter::getNetload(Netload & netload)
{
  struct ifaddrs *ifap, *ifa;
  if (getifaddrs(&ifap))
  {
    std::cerr << "error using getifaddrs" << std::endl;
    return false;
  }

  unsigned long rxbytes, rxpkt, txbytes, txpkt;

  for (ifa = ifap; ifa; ifa = ifa->ifa_next)
  {
    if (ifa->ifa_addr->sa_family != AF_LINK)
      continue;

    if (0 == strcmp(iface, ifa->ifa_name))
    {
      struct if_data * if_dataptr = (struct if_data *) ifa->ifa_data;

      rxbytes = if_dataptr->ifi_ibytes;
      txbytes = if_dataptr->ifi_obytes;
      rxpkt = if_dataptr->ifi_ipackets;
      txpkt = if_dataptr->ifi_opackets;
    }
  }
  freeifaddrs(ifap);


  netload.rxbytes=rxBytesDeriver.setCurrentValueAndGetDerivation(rxbytes);
  netload.rxpkt=rxPktDeriver.setCurrentValueAndGetDerivation(rxpkt);
  netload.txbytes=txBytesDeriver.setCurrentValueAndGetDerivation(txbytes);
  netload.txpkt=txPktDeriver.setCurrentValueAndGetDerivation(txpkt);


  return true;
}
#endif
