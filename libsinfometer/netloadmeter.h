#ifndef _netloadmeter_h
#define _netloadmeter_h

#include <string>
#include <sys/time.h>
#include "netload.h"
#include "deriver.h"

#define IFACESIZE 11


class NetloadMeter
{
private:
  char iface[IFACESIZE];

  DeriverWithTimer rxBytesDeriver;
  DeriverWithTimer rxPktDeriver;
  DeriverWithTimer txBytesDeriver;
  DeriverWithTimer txPktDeriver;

  const char * selectNetIface();


public:
  NetloadMeter(const std::string & _iface ="");

  bool getNetload(Netload & netload);
};


#endif // _netloadmeter_h
