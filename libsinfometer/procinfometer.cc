#include <dirent.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string>
#include <fcntl.h>
#include <limits.h>

#ifdef __linux__
#include <asm/param.h> /* get definition of HZ */
#include <linux/limits.h>
#endif

#ifdef __sun__
#include <sys/swap.h>
#include <sys/sysinfo.h>
#include <sys/mkdev.h>
#include <limits.h>
#include <procfs.h>
#endif

#ifdef __FreeBSD__
#include <fcntl.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/sysctl.h>
#include <sys/user.h>
#include <kvm.h>
#endif

#include "procinfometer.h"
using namespace std;


#define COMMSIZE 17
//   synchronize with size of comm in struct task_struct in
//   /usr/include/linux/sched.h ... or equivalent on sun or BSD

#define USERNAMEMAX 10
// maximum length for user names

#define PROC_BASE    "/proc"

#define cmdlineReadBufferSizeMAX 16*1024
// MAXimum ReadBufferSize for command lines

ProcinfoMeter::ProcinfoMeter(bool cmdlinemode, std::list < std::string > ignoreList) : cmdlinemode(cmdlinemode), ignoreList(ignoreList)
{
#ifdef __linux__
  cmdlineReadBufferSize = sysconf(_SC_ARG_MAX);

  // Limit bufferSize to cmdlineReadBufferSizeMAX,
  // because linux (tested with 2.6.32 and some more)
  // might return a large number that is not available as memory,
  // if the maximum stack size is unlimited:
  //    ulimit -s unlimited
  //
  //  e.g.
  //     On a 32Bit System with
  //       ulimit -s unlimited
  //       sysconf(_SC_ARG_MAX)
  //     returns
  //       1073741823
  //     which is appriximately 2^30
  //
  //     On a 62Bit System with
  //       ulimit -s unlimited
  //       sysconf(_SC_ARG_MAX)
  //     returns
  //       4611686018427387903
  //     which is appriximately 2^62
  //
  if (cmdlineReadBufferSize>cmdlineReadBufferSizeMAX)
  {
    cmdlineReadBufferSize=cmdlineReadBufferSizeMAX;
  }
  cmdlineReadBuffer = new char[cmdlineReadBufferSize];
#endif
#ifdef __sun__
  getCpuinfo(cpuinfo);
#endif
}


ProcinfoMeter::~ProcinfoMeter()
{
#ifdef __linux__
  delete [] cmdlineReadBuffer;
#endif
}


void ProcinfoMeter::unmarkProcinfoInternalList()
{
  for (std::list < ProcinfoInternal > ::iterator it = procinfoInternalList.begin();
       it != procinfoInternalList.end();
       ++it)
  {
    it->updated = false;
  }
}


bool operator<(const ProcinfoMeter::ProcinfoInternal& a, const ProcinfoMeter::ProcinfoInternal& b)
{
  return (a.procinfo.cpupercent > b.procinfo.cpupercent);
}


std::list < ProcinfoMeter::ProcinfoInternal > ::iterator ProcinfoMeter::getProcinfoInternalList(int pid)
{
  std::list < ProcinfoInternal > ::iterator pit;
  bool found = false;
  for (pit = procinfoInternalList.begin();
       pit != procinfoInternalList.end();
       ++pit)
  {
    if (pit->procinfo.pid == pid)
    {
      found = true;
      break;
    }
  }
  if (!found)
  {
    ProcinfoInternal newentry;
    pit = procinfoInternalList.insert(pit, newentry);
    pit->procinfo.pid = pid;
    pit->uid = -1;
  }
  pit->updated = true;

  return pit;
}


#ifdef __linux__
bool ProcinfoMeter::readCmdline(std::string & cmdline, int pid)
{
  char path[PATH_MAX + 1];
  snprintf(path, PATH_MAX + 1, "%s/%d/cmdline", PROC_BASE, pid);
  int fd=open(path, O_RDONLY);
  if (-1 != fd)
  {
    int bufferStartIdx=0;
    while (true)
    {
      int readRet=read(fd, &cmdlineReadBuffer[bufferStartIdx], cmdlineReadBufferSize-bufferStartIdx);
      if (readRet<=0)
      {
        // no (more) (valid) data
        break;
      }
      bufferStartIdx+=readRet;
      if (bufferStartIdx>=cmdlineReadBufferSize)
      {
        // full buffer
        break;
      }
    }
    close(fd);


    // convert 0 to ' '
    for (int idx=0; idx<bufferStartIdx; idx++)
    {
      if (cmdlineReadBuffer[idx]==0)
      {
        cmdlineReadBuffer[idx]=' ';
      }
    }
    cmdline=string(cmdlineReadBuffer, bufferStartIdx);
    return true;
  }
  return false;
}


bool ProcinfoMeter::readProcinfo(ProcinfoMeter::ProcinfoInternal & pii)
{
  bool retval = true;

  char path[PATH_MAX + 1];
  snprintf(path, PATH_MAX + 1, "%s/%d/stat", PROC_BASE, pii.procinfo.pid);
  FILE *file;
  if ((file = fopen(path, "r")))
  {
    if (pii.uid < 0)
    {
      char pathuid[PATH_MAX + 1];
      snprintf(pathuid, PATH_MAX + 1, "%s/%d", PROC_BASE, pii.procinfo.pid);
      struct stat st;
      if (stat(pathuid, &st) < 0)
      {
        perror(path);
        retval = false;
      }
      pii.uid = st.st_uid;
    }


    char commandInStat[COMMSIZE];
    int stat_utime;
    int stat_stime;
    if (fscanf(file, "%*d (%[^)]) %c %*d %*d %*d %*d %*d %*u "
               "%*u %*u %*u %*u %d %d %*d %*d %*d"
               "%d",
               &commandInStat[0],
               &pii.procinfo.state,

               &stat_utime,
               &stat_stime,

               &pii.procinfo.priority
              ) != 5)
    {
      retval = false;
      fprintf(stderr, "badly formated /proc/#/stat\n");
      pii.procinfo.command="";
    }
    else
    {
      if (0==pii.procinfo.command.size())
      {
        if (true==cmdlinemode)
        {
          readCmdline(pii.procinfo.command, pii.procinfo.pid);
          if (0==pii.procinfo.command.size())
          {
            pii.procinfo.command="["+string(commandInStat)+"]";
          }
        }
        else
        {
          pii.procinfo.command=string(commandInStat);
        }

        pii.ignoreListMatch = false;
        for (list < string > ::iterator it = ignoreList.begin();
             it != ignoreList.end();
             ++it)
        {
          if ( pii.procinfo.command == (*it) )
          {
            pii.ignoreListMatch=true;
          }
        }
      }

      pii.procinfo.cpupercent=(pii.utimeDeriver.setCurrentValueAndGetDerivation(double(stat_utime)/double(HZ))
                               +pii.stimeDeriver.setCurrentValueAndGetDerivation(double(stat_stime)/double(HZ)))*100.;
    }

    fclose(file);
  }
  else
  {
    retval = false;
  }

  return retval;
}


void ProcinfoMeter::updateProcinfoInternalList()
{
  DIR *dir;
  if (!(dir = opendir(PROC_BASE)))
  {
    perror(PROC_BASE);
  }
  else
  {
    struct dirent *de;
    while ((de = readdir(dir)))
    {
      pid_t pid;
      if ((pid = atoi(de->d_name)))
      {
        std::list < ProcinfoInternal > ::iterator pit = getProcinfoInternalList(pid);

        readProcinfo(*pit);
      }
    }
    closedir(dir);
  }
}
#endif


#ifdef __sun__
bool ProcinfoMeter::readProcinfo(ProcinfoMeter::ProcinfoInternal & pii)
{
  char path[PATH_MAX + 1];
  snprintf(path, PATH_MAX + 1, "%s/%d/psinfo", PROC_BASE, pii.procinfo.pid);
  psinfo_t psi;

  int fd;
  if ((fd = open(path, O_RDONLY)) < 0)
    return false;
  if (read(fd, (void *)&psi, sizeof(psi)) < (int)sizeof(psi))
  {
    close(fd);
    return false;
  }
  close(fd);

  pii.uid = psi.pr_uid;
  strncpy(pii.procinfo.command, psi.pr_fname, COMMSIZE);
  pii.procinfo.command[COMMSIZE - 1] = 0;
  pii.procinfo.state = psi.pr_lwp.pr_sname;
  pii.procinfo.priority = psi.pr_lwp.pr_nice;
  pii.procinfo.cpupercent = ((float)psi.pr_pctcpu) / 0x8000 * cpuinfo.cpus * 100.;

  return true;
}


void ProcinfoMeter::updateProcinfoInternalList()
{
  DIR *dir;
  if (!(dir = opendir(PROC_BASE)))
  {
    perror(PROC_BASE);
  }
  else
  {
    struct dirent *de;
    while (de = readdir(dir))
    {
      pid_t pid;
      if (pid = atoi(de->d_name))
      {
        std::list < ProcinfoInternal > ::iterator pit = getProcinfoInternalList(pid);
        readProcinfo(*pit);
      }
    }
    closedir(dir);
  }
}
#endif


#ifdef __FreeBSD__
void ProcinfoMeter::updateProcinfoInternalList()
{
  kvm_t *kd;
  if ((kd = kvm_open(NULL, NULL, NULL, O_RDONLY, "kvm_open")) == NULL)
  {
    fprintf(stderr, "kvm_open failed\n");
  }
  else
  {
    int nentries;
    struct kinfo_proc *kp;    /* defines in include/sys/user.h */
    if ((kp = kvm_getprocs(kd, KERN_PROC_ALL, 0, &nentries)) == 0)
    {
      fprintf(stderr, "kvm_getprocs failed\n");
    }
    else
    {
      for (int i = nentries; --i >= 0; ++kp)
      {
        /* struct proc is defined in include/sys/proc.h */


#ifdef HAVE_STRUCT_KINFO_PROC_KI_PID
        if ((kp->ki_flag & P_CONTROLT ) != 0)
        {
          std::list < ProcinfoInternal > ::iterator pit = getProcinfoInternalList(kp->ki_pid);

          pit->procinfo.command=string(kp->ki_comm);
          pit->procinfo.priority = kp->ki_nice;
          pit->uid = kp->ki_ruid;
          pit->procinfo.cpupercent = 100.0 * double(kp->ki_pctcpu) / double(FSCALE);
          switch (kp->ki_stat)
#else
        if ((kp->kp_proc.p_flag & P_CONTROLT ) != 0)
        {
          std::list < ProcinfoInternal > ::iterator pit = getProcinfoInternalList(kp->kp_proc.p_pid);

          pit->procinfo.command=string(kp->kp_proc.p_comm);
          pit->procinfo.priority = kp->kp_proc.p_nice;
          pit->uid = kp->kp_eproc.e_pcred.p_ruid;
          pit->procinfo.cpupercent = 100.0 * double(kp->kp_proc.p_pctcpu) / double(FSCALE);
          switch (kp->kp_proc.p_stat)
#endif

          {
          case SSTOP:
            pit->procinfo.state = 'T';
            break;
          case SSLEEP:
            pit->procinfo.state = 'D';
            break;
          case SRUN:
          case SIDL:
            pit->procinfo.state = 'R';
            break;
          case SZOMB:
            pit->procinfo.state = 'Z';
            break;
          default:
            pit->procinfo.state = '?';
          }
        }
      }
    }
    kvm_close(kd);
  }
}
#endif


void ProcinfoMeter::cleanupProcinfoInternalList()
{
  for (std::list < ProcinfoInternal > ::iterator pit = procinfoInternalList.begin();
       pit != procinfoInternalList.end();
      )
  {
    if (pit->updated)
    {

      /*
            cout << "pid:" << pit->procinfo.pid

                  << " cmd:" << pit->procinfo.command
            << " cpupercent:" << pit->procinfo.cpupercent
            << endl;
       */

      ++pit;
    }
    else
    {
      pit = procinfoInternalList.erase(pit);
    }
  }
}


bool ProcinfoMeter::getTopList(int nr, std::list < Procinfo > & returnProcinfoList)
{
  unmarkProcinfoInternalList();

  updateProcinfoInternalList();

  procinfoInternalList.sort();

  cleanupProcinfoInternalList();

  returnProcinfoList.erase(returnProcinfoList.begin(),returnProcinfoList.end());

  int i = 0;
  for (std::list < ProcinfoInternal > ::iterator pit = procinfoInternalList.begin();
       ((pit != procinfoInternalList.end()) && (i < nr));
       ++pit)
  {

#ifdef SIMPLE_USER_CACHE
    if (0 == pit->procinfo.username.size())
    {
      // do some kind of name-caching to prevent (NIS/NIS+) name-lookups
      for (std::list < ProcinfoInternal > ::iterator ppit = procinfoInternalList.begin();
           ppit != procinfoInternalList.end();
           ++ppit)
      {
        if ((pit->uid == ppit->uid)
            && (ppit->procinfo.username.size() != 0 ))
        {
          pit->procinfo.username=ppit->procinfo.username;
          break;
        }
      }
    }
#endif

    if (0 == pit->procinfo.username.size())
    {
      struct passwd * passwdent = getpwuid(pit->uid);
      if (passwdent != 0)
      {
        pit->procinfo.username=string(passwdent->pw_name);
      }
      else
      {
        char username[USERNAMEMAX];
        snprintf(username, USERNAMEMAX, "%d", pit->uid);
        pit->procinfo.username=string(username);
      }
    }

    if (false==pit->ignoreListMatch)
    {
      returnProcinfoList.push_back(pit->procinfo);
      i++;
    }
  }
  if (i == nr)
    return true;

  return false;
}
