#ifndef _procinfometer_h
#define _procinfometer_h


#include <list>
#include "procinfo.h"
#include "deriver.h"

#ifdef __sun__
// we need the number of cpus for propper scaling of percent cpu-usage
#include <sinfo/cpuinfo.h>
#endif



class ProcinfoMeter
{
public:
  struct ProcinfoInternal
  {
    Procinfo procinfo;

    int uid;
    bool ignoreListMatch;

#ifdef __linux__
    DeriverWithTimer utimeDeriver;
    DeriverWithTimer stimeDeriver;
#endif

    bool updated;  // set to true if updated or new, false if process terminated
  };

private:
  bool cmdlinemode;
  std::list < std::string > ignoreList;
  std::list < ProcinfoInternal > procinfoInternalList;

  void unmarkProcinfoInternalList();  // set alle "updated" flags to false
  std::list < ProcinfoInternal > ::iterator getProcinfoInternalList(int pid);  // and set "updated"

#ifdef __linux__
  char * cmdlineReadBuffer;
  long cmdlineReadBufferSize;
  bool readCmdline(std::string & cmdline, int pid);
  bool readProcinfo(ProcinfoInternal & pii);
#endif
#ifdef __sun__
  Cpuinfo cpuinfo;
  bool readProcinfo(ProcinfoInternal & pii);
#endif
  void updateProcinfoInternalList();  // different implementations for Linux, Solaris, FreeBSD available

  void cleanupProcinfoInternalList();  // erase all entries that are not "updated"


public:
  ProcinfoMeter(bool cmdlinemode, std::list < std::string > ignoreList);
  ~ProcinfoMeter();
  bool getTopList(int nr, std::list < Procinfo > & returnProcinfoList);  // Update and return the top nr entries of procinfoInternalList
}
;

bool operator<(const ProcinfoMeter::ProcinfoInternal& a, const ProcinfoMeter::ProcinfoInternal&b);


#endif // _procinfometer_h
