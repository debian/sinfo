#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/utsname.h>


#include "unamemeter.h"


Unameinfo getUnameinfo()
{
  Unameinfo unameinfo;

  struct utsname suname;
  uname(&suname);

  unameinfo.sysname=suname.sysname;
  unameinfo.nodename=suname.nodename;
  unameinfo.release=suname.release;
  unameinfo.version=suname.version;
  unameinfo.machine=suname.machine;

  return unameinfo;
}
