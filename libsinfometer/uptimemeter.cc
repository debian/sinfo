#include <stdio.h>

#ifdef __sun__
#include <time.h>
#include <limits.h>
#include <errno.h>
#include <kstat.h>
#include <string.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/swap.h>
#endif

#ifdef __FreeBSD__
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/time.h>
#endif


#include "uptimemeter.h"


bool getUptime(Uptime & uptime)
{
#ifdef __linux__
  long readtime=0;
  FILE *fup;
  if ( (fup = fopen("/proc/uptime", "r") ) != NULL)
  {

    fscanf(fup, "%ld",
           &readtime);

    uptime.days=readtime/(24*60*60);
    uptime.seconds=readtime%(24*60*60);
    fclose(fup);
    return true;
  }
#endif


#ifdef __FreeBSD__
  int mib[2];
  mib[0] = CTL_KERN;
  mib[1] = KERN_BOOTTIME;

  struct timeval boottime;
  size_t bt_size = sizeof(boottime);
  if (sysctl(mib, 2, &boottime, &bt_size, NULL, 0) != -1
      && boottime.tv_sec != 0)
  {
    long readtime = time(NULL) - boottime.tv_sec;
    uptime.days=readtime/(24*60*60);
    uptime.seconds=readtime%(24*60*60);
    return true;
  }
  else
  {
    uptime.seconds = 0;
  }
#endif


#ifdef __sun__
  struct kstat_ctl *kc;
  if ((kc = kstat_open()) == 0)
  {
    fprintf(stderr, "Sorry, I cannot initialise the `kstat' library.\n"
            "This library is used for accessing kernel information.\n"
            "The diagnostics are: `%s'.\n\n", strerror(errno));
  }
  else
  {

    kstat_t *ks = kstat_lookup(kc, "unix", 0, "system_misc");
    if (!ks || kstat_read(kc, ks, 0) == -1)
    {
      perror("kstat_lookup/read");
    }
    else
    {

      kstat_named_t *kn;
      kn = (kstat_named_t *)kstat_data_lookup(ks, "boot_time");
      if (kn)
      {
        long readtime = time(NULL) - kn->value.ui32;
        uptime.days=readtime/(24*60*60);
        uptime.seconds=readtime%(24*60*60);
      }
      kstat_close(kc);
      return true;
    }
  }
#endif

  return false;
}
