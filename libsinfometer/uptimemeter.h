#ifndef _uptimemeter_h
#define _uptimemeter_h


#include "uptime.h"


bool getUptime(Uptime & uptime);


#endif // _uptimemeter_h
