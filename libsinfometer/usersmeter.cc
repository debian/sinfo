#include <stdio.h>
#include <sys/types.h>
#include <time.h> // nust be included before utmp.h for FreeBSD
#include <utmp.h>


#include "usersmeter.h"


Users getUsers()
{
  Users users;
  users.number = 0;

#ifdef __FreeBSD__
  FILE *ut;
  if ((ut = fopen(_PATH_UTMP, "r")) != NULL)
  {
    struct utmp usr;
    /* only entries with both name and line fields */
    while (fread((char *)&usr, sizeof(usr), 1, ut) == 1)
      if (*usr.ut_name && *usr.ut_line)
        users.number++;
    fclose(ut);
  }
#else
  // __linux__ and __sun__

  // ideas out of the program "uptime"
  setutent();
  struct utmp *utmpstruct;
  while ((utmpstruct = getutent()))
  {
    if ((utmpstruct->ut_type == USER_PROCESS) &&
        (utmpstruct->ut_name[0] != '\0'))
      users.number++;
  }
  endutent();
#endif

  return users;
}
