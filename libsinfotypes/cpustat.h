#ifndef _cpustat_h
#define _cpustat_h


struct Cpustat
{
  float userp;
  float nicep;
  float sysp;
  float idlep;
  float iowaitp;
  float irqp;
  float softirqp;
};


#endif // _cpustat_h
