#ifndef _loadavg_h
#define _loadavg_h

struct Loadavg
{
  float load1;
  float load5;
  float load15;
};


#endif // _loadavg_h
