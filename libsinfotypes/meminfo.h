#ifndef _meminfo_h
#define _meminfo_h

struct Meminfo
{
  float mem_total;
  float mem_used;
  float mem_free;

  float swap_total;
  float swap_used;
  float swap_free;
};


#endif // _meminfo_h
