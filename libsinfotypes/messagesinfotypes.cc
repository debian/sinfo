#include <string.h>
#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include "messagesinfotypes.h"
#include "messagestlcontainertypes.h"


namespace Msg
{


void pushFrontLoadavg(Message & message, const Loadavg & value)
{
  pushFrontfloat32(message, value.load15);
  pushFrontfloat32(message, value.load5);
  pushFrontfloat32(message, value.load1);
}


// void pushBackLoadavg(Message & message, const Loadavg & value)
// {
//   pushBackfloat32(message, value.load1);
//   pushBackfloat32(message, value.load5);
//   pushBackfloat32(message, value.load15);
// }


void popFrontLoadavg(Message & message, Loadavg & returnValue)
{
  popFrontfloat32(message,returnValue.load1);
  popFrontfloat32(message,returnValue.load5);
  popFrontfloat32(message,returnValue.load15);
}


void pushFrontMeminfo(Message & message, const Meminfo & value)
{
  pushFrontfloat32(message, value.swap_free);
  pushFrontfloat32(message, value.swap_used);
  pushFrontfloat32(message, value.swap_total);

  pushFrontfloat32(message, value.mem_free);
  pushFrontfloat32(message, value.mem_used);
  pushFrontfloat32(message, value.mem_total);
}


// void pushBackMeminfo(Message & message, const Meminfo & value)
// {
//   pushBackfloat32(message, value.mem_total);
//   pushBackfloat32(message, value.mem_used);
//   pushBackfloat32(message, value.mem_free);
//
//   pushBackfloat32(message, value.swap_total);
//   pushBackfloat32(message, value.swap_used);
//   pushBackfloat32(message, value.swap_free);
// }


void popFrontMeminfo(Message & message, Meminfo & returnValue)
{
  popFrontfloat32(message,returnValue.mem_total);
  popFrontfloat32(message,returnValue.mem_used);
  popFrontfloat32(message,returnValue.mem_free);

  popFrontfloat32(message,returnValue.swap_total);
  popFrontfloat32(message,returnValue.swap_used);
  popFrontfloat32(message,returnValue.swap_free);
}


void pushFrontCpustat(Message & message, const Cpustat & value)
{
  pushFrontfloat32(message, value.softirqp);
  pushFrontfloat32(message, value.irqp);
  pushFrontfloat32(message, value.iowaitp);
  pushFrontfloat32(message, value.idlep);
  pushFrontfloat32(message, value.sysp);
  pushFrontfloat32(message, value.nicep);
  pushFrontfloat32(message, value.userp);
}


// void pushBackCpustat(Message & message, const Cpustat & value)
// {
//   pushBackfloat32(message, value.userp);
//   pushBackfloat32(message, value.nicep);
//   pushBackfloat32(message, value.sysp);
//   pushBackfloat32(message, value.idlep);
//   pushBackfloat32(message, value.iowaitp);
//   pushBackfloat32(message, value.irqp);
//   pushBackfloat32(message, value.softirqp);
// }


void popFrontCpustat(Message & message, Cpustat & returnValue)
{
  popFrontfloat32(message, returnValue.userp);
  popFrontfloat32(message, returnValue.nicep);
  popFrontfloat32(message, returnValue.sysp);
  popFrontfloat32(message, returnValue.idlep);
  popFrontfloat32(message, returnValue.iowaitp);
  popFrontfloat32(message, returnValue.irqp);
  popFrontfloat32(message, returnValue.softirqp);
}


void pushFrontCpuinfo(Message & message, const Cpuinfo & value)
{
  pushFrontfloat32(message, value.speedmhz);
  pushFrontint32(message, value.cpus);
}


// void pushBackCpuinfo(Message & message, const Cpuinfo & value)
// {
//   pushBackint32(message, value.cpus);
//   pushBackfloat32(message, value.speedmhz);
// }


void popFrontCpuinfo(Message & message, Cpuinfo & returnValue)
{
  popFrontint32(message, returnValue.cpus);
  popFrontfloat32(message, returnValue.speedmhz);
}


void pushFrontUptime(Message & message, const Uptime & value)
{
  pushFrontint32(message, value.seconds);
  pushFrontint32(message, value.days);
}


// void pushBackUptime(Message & message, const Uptime & value)
// {
//   pushBackint32(message, value.days);
//   pushBackint32(message, value.seconds);
// }


void popFrontUptime(Message & message, Uptime & returnValue)
{
  popFrontint32(message, returnValue.days);
  popFrontint32(message, returnValue.seconds);
}


void pushFrontUsers(Message & message, const Users & value)
{
  pushFrontint32(message, value.number);
}


// void pushBackUsers(Message & message, const Users & value)
// {
//   pushBackint32(message, value.number);
// }


void popFrontUsers(Message & message, Users & returnValue)
{
  popFrontint32(message, returnValue.number);
}


void pushFrontProcinfo(Message & message, const Procinfo & value)
{
  pushFrontfloat32(message, value.cpupercent);
  pushFrontstring(message, value.username);
  pushFrontint32(message, value.priority);
  pushFrontint8(message, value.state);
  pushFrontstring(message, value.command);
  pushFrontint32(message, value.pid);
}


// void pushBackProcinfo(Message & message, const Procinfo & value)
// {
//   pushBackint32(message, value.pid);
//   pushBackstring(message, value.command);
//   pushBackint8(message, value.state);
//   pushBackint32(message, value.priority);
//   pushBackstring(message, value.username);
//   pushBackfloat32(message, value.cpupercent);
// }


void popFrontProcinfo(Message & message, Procinfo & returnValue)
{
  popFrontint32(message, returnValue.pid);
  popFrontstring(message, returnValue.command);
  popFrontint8(message, returnValue.state);
  popFrontint32(message, returnValue.priority);
  popFrontstring(message, returnValue.username);
  popFrontfloat32(message, returnValue.cpupercent);
}


void pushFront(Message & message, const Procinfo & value)
{
  pushFrontProcinfo(message, value);
}


void popFront(Message & message, Procinfo & returnValue)
{
  popFrontProcinfo(message, returnValue);
}


void pushFrontUnameinfo(Message & message, const Unameinfo & value)
{
  pushFrontstring(message, value.machine);
  pushFrontstring(message, value.version);
  pushFrontstring(message, value.release);
  pushFrontstring(message, value.nodename);
  pushFrontstring(message, value.sysname);
}


// void pushBackUnameinfo(Message & message, const Unameinfo & value)
// {
//   pushBackstring(message, value.sysname);
//   pushBackstring(message, value.nodename);
//   pushBackstring(message, value.release);
//   pushBackstring(message, value.version);
//   pushBackstring(message, value.machine);
// }


void popFrontUnameinfo(Message & message, Unameinfo & returnValue)
{
  popFrontstring(message, returnValue.sysname);
  popFrontstring(message, returnValue.nodename);
  popFrontstring(message, returnValue.release);
  popFrontstring(message, returnValue.version);
  popFrontstring(message, returnValue.machine);
}


void pushFrontNetload(Message & message, const Netload & value)
{
  pushFrontfloat32(message, value.txpkt);
  pushFrontfloat32(message, value.rxpkt);
  pushFrontfloat32(message, value.txbytes);
  pushFrontfloat32(message, value.rxbytes);
  pushFrontstring(message, value.iface);
}


// void pushBackNetload(Message & message, const Netload & value)
// {
//   pushBackstring(message, value.iface);
//   pushBackfloat32(message, value.rxbytes);
//   pushBackfloat32(message, value.txbytes);
//   pushBackfloat32(message, value.rxpkt);
//   pushBackfloat32(message, value.txpkt);
// }


void popFrontNetload(Message & message, Netload & returnValue)
{
  popFrontstring(message, returnValue.iface);
  popFrontfloat32(message, returnValue.rxbytes);
  popFrontfloat32(message, returnValue.txbytes);
  popFrontfloat32(message, returnValue.rxpkt);
  popFrontfloat32(message, returnValue.txpkt);
}


void pushFrontDiskload(Message & message, const Diskload & value)
{
  pushFrontfloat32(message, value.writekbytespersec);
  pushFrontfloat32(message, value.readkbytespersec);
}


// void pushBackDiskload(Message & message, const Diskload & value)
// {
//   pushBackfloat32(message, value.readkbytespersec);
//   pushBackfloat32(message, value.writekbytespersec);
// }


void popFrontDiskload(Message & message, Diskload & returnValue)
{
  popFrontfloat32(message,returnValue.readkbytespersec);
  popFrontfloat32(message,returnValue.writekbytespersec);
}


void pushFrontWsinfo(Message & message, const Wsinfo & value)
{
  pushFrontstring(message, value.marker);
  pushFrontDiskload(message, value.diskload);
  pushFrontNetload(message, value.netload);
  pushFrontUnameinfo(message, value.unameinfo);
  pushFront(message, value.procinfoList);
  pushFrontUsers(message, value.users);
  pushFrontUptime(message, value.uptime);
  pushFrontCpuinfo(message, value.cpuinfo);
  pushFrontCpustat(message, value.cpustat);
  pushFrontMeminfo(message, value.meminfo);
  pushFrontLoadavg(message, value.loadavg);
  pushFrontstring(message, value.inetaddr);
  pushFrontstring(message, value.name);
}


// void pushBackWsinfo(Message & message, const Wsinfo & value)
// {
//   pushBackstring(message, value.name);
//   pushBackstring(message, value.inetaddr);
//   pushBackLoadavg(message, value.loadavg);
//   pushBackMeminfo(message, value.meminfo);
//   pushBackCpustat(message, value.cpustat);
//   pushBackCpuinfo(message, value.cpuinfo);
//   pushBackUptime(message, value.uptime);
//   pushBackUsers(message, value.users);
//   pushBack(message, value.procinfoList);
//   pushBackUnameinfo(message, value.unameinfo);
//   pushBackNetload(message, value.netload);
//   pushBackDiskload(message, value.diskload);
//   pushBackstring(message, value.marker);
// }


void popFrontWsinfo(Message & message, Wsinfo & returnValue)
{
  popFrontstring(message, returnValue.name);
  popFrontstring(message, returnValue.inetaddr);
  popFrontLoadavg(message, returnValue.loadavg);
  popFrontMeminfo(message, returnValue.meminfo);
  popFrontCpustat(message, returnValue.cpustat);
  popFrontCpuinfo(message, returnValue.cpuinfo);
  popFrontUptime(message, returnValue.uptime);
  popFrontUsers(message, returnValue.users);
  popFront(message, returnValue.procinfoList);
  popFrontUnameinfo(message, returnValue.unameinfo);
  popFrontNetload(message, returnValue.netload);
  popFrontDiskload(message, returnValue.diskload);
  popFrontstring(message, returnValue.marker);
}


void pushFront(Message & message, const Wsinfo & value)
{
  pushFrontWsinfo(message, value);
}


void popFront(Message & message, Wsinfo & returnValue)
{
  popFrontWsinfo(message, returnValue);
}

}
