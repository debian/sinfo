#ifndef _messagesinfotypes_h
#define _messagesinfotypes_h

#include "sinfo.h"
#include "message.h"

namespace Msg
{
void pushFrontLoadavg(Message & message, const Loadavg & value);
//void pushBackLoadavg(Message & message, const Loadavg & value);
void popFrontLoadavg(Message & message, Loadavg & returnValue);

void pushFrontMeminfo(Message & message, const Meminfo & value);
//void pushBackMeminfo(Message & message, const Meminfo & value);
void popFrontMeminfo(Message & message, Meminfo & returnValue);

void pushFrontCpustat(Message & message, const Cpustat & value);
//void pushBackCpustat(Message & message, const Cpustat & value);
void popFrontCpustat(Message & message, Cpustat & returnValue);

void pushFrontCpuinfo(Message & message, const Cpuinfo & value);
//void pushBackCpuinfo(Message & message, const Cpuinfo & value);
void popFrontCpuinfo(Message & message, Cpuinfo & returnValue);

void pushFrontUptime(Message & message, const Uptime & value);
//void pushBackUptime(Message & message, const Uptime & value);
void popFrontUptime(Message & message, Uptime & returnValue);

void pushFrontUsers(Message & message, const Users & value);
//void pushBackUsers(Message & message, const Users & value);
void popFrontUsers(Message & message, Users & returnValue);

void pushFrontProcinfo(Message & message, const Procinfo & value);
//void pushBackProcinfo(Message & message, const Procinfo & value);
void popFrontProcinfo(Message & message, Procinfo & returnValue);
void pushFront(Message & message, const Procinfo & value);
void popFront(Message & message, Procinfo & returnValue);

void pushFrontUnameinfo(Message & message, const Unameinfo & value);
//void pushBackUnameinfo(Message & message, const Unameinfo & value);
void popFrontUnameinfo(Message & message, Unameinfo & returnValue);

void pushFrontNetload(Message & message, const Netload & value);
//void pushBackNetload(Message & message, const Netload & value);
void popFrontNetload(Message & message, Netload & returnValue);

void pushFrontDiskload(Message & message, const Diskload & value);
//void pushBackDiskload(Message & message, const Diskload & value);
void popFrontDiskload(Message & message, Diskload & returnValue);

void pushFrontWsinfo(Message & message, const Wsinfo & value);
//void pushBackWsinfo(Message & message, const Wsinfo & value);
void popFrontWsinfo(Message & message, Wsinfo & returnValue);
void pushFront(Message & message, const Wsinfo & value);
void popFront(Message & message, Wsinfo & returnValue);

}


#endif // _messagesinfotypes_h
