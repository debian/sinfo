#ifndef _netload_h
#define _netload_h

#include <string>

struct Netload
{
  std::string iface;
  float rxbytes;
  float txbytes;

  float rxpkt;
  float txpkt;
};


#endif // _netload_h
