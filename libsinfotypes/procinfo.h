#ifndef _procinfo_h
#define _procinfo_h

#include <list>
#include <string>


struct Procinfo
{
  int pid;                           // process ID
  std::string command;               // the name of the running process
  char state;                        // the process state 'R'=running, ...
  int priority;
  std::string username;              // translated UID
  float cpupercent;                  // CPU usage of this process
};


typedef std::list < Procinfo > ProcinfoList;


#endif // _procinfo_h
