#ifndef _sinfo_h
#define _sinfo_h

#include <string>
#include <time.h>
#include <netinet/in.h>
#include <list>

#include "cpustat.h"
#include "cpuinfo.h"
#include "loadavg.h"
#include "meminfo.h"
#include "netload.h"
#include "diskload.h"
#include "uname.h"
#include "uptime.h"
#include "users.h"
#include "procinfo.h"


/*
#The Well Known Ports are those from 0 through 1023.
#
#The Registered Ports are those from 1024 through 49151
#
#The Dynamic and/or Private Ports are those from 49152 through 65535
*/
// port for udp broadcast and tcp queries
#define SINFO_REQUEST_PORT 60002
#define SINFO_REQUEST_PORT_STRING "60002"
#define SINFO_BROADCAST_PORT 60003
#define SINFO_BROADCAST_PORT_STRING "60003"


struct Wsinfo
{
  // set in broadcastreceiver
  time_t lastheard;
  std::string inetaddr;
  std::string name;
  time_t name_time;

  // transmitted
  Loadavg loadavg;
  Meminfo meminfo;
  Cpustat cpustat;
  Cpuinfo cpuinfo;
  Uptime uptime;
  Users users;
  ProcinfoList procinfoList;
  Unameinfo unameinfo;
  Netload netload;
  Diskload diskload;
  std::string marker;
};


bool operator<(const Wsinfo& a, const Wsinfo& b);


#endif // _sinfo_h
