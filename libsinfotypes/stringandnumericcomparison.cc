#include "stringandnumericcomparison.h"
using namespace std;


bool stringAndNumericLessThan(const string & a, const string & b)
{
  unsigned long aidx=0;
  unsigned long bidx=0;
  while ((aidx<a.size()) && (bidx<b.size()))
  {
    if ( (0!=isdigit(a[aidx])) && (0!=isdigit(b[bidx])) )
    {
      // fetch numbers
      long anumber=0;
      long bnumber=0;
      while ( (0!=isdigit(a[aidx])) && (aidx<a.size()) )
      {
        anumber*=10;
        anumber+=a[aidx]-'0';
        aidx++;
      }
      while ( (0!=isdigit(b[bidx])) && (bidx<b.size()) )
      {
        bnumber*=10;
        bnumber+=b[bidx]-'0';
        bidx++;
      }

      // the numeric comparison
      if (anumber<bnumber)
      {
        return true;
      }
      if (anumber>bnumber)
      {
        return false;
      }
    }
    else
    {
      // fetch character
      char achar=a[aidx++];
      char bchar=b[bidx++];

      // the character comparison
      if (achar<bchar)
      {
        return true;
      }
      if (achar>bchar)
      {
        return false;
      }
    }
  }
  // strings seem to be identical up to their common size
  // last criterion: size of strings
  return (a.size()<b.size());
}

