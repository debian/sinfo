#ifndef _stringandnumericcomparison_h
#define _stringandnumericcomparison_h

#include <string>



bool stringAndNumericLessThan(const std::string & a, const std::string & b);


#endif // _stringandnumericcomparison_h
