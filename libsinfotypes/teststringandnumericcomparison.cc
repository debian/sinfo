#include <iostream>
#include <string>
#include "stringandnumericcomparison.h"
using namespace std;



void comparetest(const string & a, const string & b)
{
  cout << "comparing " << a << " and " << b
       << " "
       << "<" << (a<b)
       << " "
       << "new" << stringAndNumericLessThan(a,b)
       << endl;
}


int main()
{
  comparetest("a","a");
  comparetest("a","b");
  comparetest("b","a");

  comparetest("aa","a");
  comparetest("a","aa");
  comparetest("a1","aa");

  comparetest("a10","a2");
  comparetest("a2","a10");

  comparetest("a01","a1");
  comparetest("a1","a01");
  comparetest("a1b","a01a");

  comparetest("node9","node10");
  comparetest("node10","node9");

  comparetest("node9a","node10a");
  comparetest("node10a","node9a");
}
