#ifndef _uname_h
#define _uname_h

#include <string>


struct Unameinfo
{
  std::string sysname;
  std::string nodename;
  std::string release;
  std::string version;
  std::string machine;
};


#endif // _uname_h
