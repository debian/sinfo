#include "sinfo.h"
#include "stringandnumericcomparison.h"

bool operator<(const Wsinfo& a, const Wsinfo& b)
{
  return stringAndNumericLessThan(a.name, b.name);
}

