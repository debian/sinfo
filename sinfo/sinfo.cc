#include <boost/asio.hpp>
#include <list>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <getopt.h>
#include "sinfoviewconfig.h"
#include "sinfocurses.h"
#include "sinfoprint.h"
#include "sinfohtml.h"
#include "sinforequester.h"
#include "sinfodata.h"
#include "udpmessageclient.h"
#include "tcpmessageclient.h"
#include "protocoltimerasio.h"
using namespace std;


void printHelpAndExit(const char * plaint = "Usage:-")
{
  cout << plaint << endl
       << endl
       << "sinfo [-q][-V] [-W][-H][-I][-L] [-h host[:port]] [-u user] [-d delay ] [-s][-o][-i process][-n][-D][-t count][-c scale]" << endl
       << endl
       << "OPTIONS (for full descriptions see \"man sinfo\")" << endl
       << "-q/--quiet" << endl
       << "  be quiet - don't display startup informations." << endl
       << "-V/--version" << endl
       << "  Print the version number and exit." << endl
       << "--W/--wwwmode" << endl
       << "  Web-mode:  List all hosts and their informations in html." << endl
       << "-H/--hostsmode" << endl
       << "  List all host names of nodes running a sinfod." << endl
       << "-I/--ipmode" << endl
       << "  List all IPs of nodes running a sinfod." << endl
       << "-L/--loadmode" << endl
       << "  List all known host names with the current system load." << endl
       << "-h/--host <host[:port]>" << endl
       << "  Fetch information from the sinfod on <host[:port]>." << endl
       << "--udp / --tcp" << endl
       << " Use UDP(default) or TCP to connect to sinfod." << endl
       << "-u/--user <user>" << endl
       << "  Highlight all processes of <user>." << endl
       << "-d/--delay <delay>" << endl
       << "  Set  the delay (in seconds) between two updates." << endl
       << "-s/--systeminfo" << endl
       << "  Show system information (resolved by a uname -a call) in  the list." << endl
       << "-o/--own" << endl
       << "  Only display  your own processes." << endl
       << "-i/--ignore <process>" << endl
       << "  don't display <process> ; --ignore may be set multiple times." << endl
       << "-n/--netload" << endl
       << "  Display network load." << endl
       << "-D/--diskload" << endl
       << "  Display disk load (buffer allocation rate)." << endl
       << "-t/--top <count>" << endl
       << "  Restrict the number of processes displayed to <count>." << endl
       << "-c/--scale <scale>" << endl
       << "  Set the scaling of the CPU load bars to \"log\", \"lin\" or full." << endl
       << endl;
  exit(1);
}


void exit1()
{
  exit(1);
}


int main(int argc, char * argv[])
{
  // Controller
  boost::asio::io_service ioservice;

  // Model - the data
  SinfoData sinfoData;

  // View - requester
  std::string requestHost; // FIXME : make it work again   i.e. multiple requester objects or one
  ProtocolIOAsio protocolIO(ioservice);
  SinfoRequester sinfoRequester(protocolIO, sinfoData);

  // other Views - SinfoHTML, SinfoCurses   created later


  bool useUDPConnection=true;

  // configuration of Views
  SinfoViewConfig sinfoViewConfig;
  enum PrintMode {printHosts, printIP, printHostsAndIP, printHostsLoad, printInteractive, printHTML} printMode;
  printMode = printInteractive;   // default: interactive

  static const struct option long_options[] =
  {
    { "quiet", no_argument, 0, 'q' },
    { "version", no_argument, 0, 'V' },

    { "wwwmode", no_argument, 0, 'W' },
    { "hostsmode", no_argument, 0, 'H' },
    { "ipmode", no_argument, 0, 'I' },
    { "hostsandipmode", no_argument, 0, 1001 },
    { "loadmode", no_argument, 0, 'L' },

    { "host", required_argument, 0, 'h'},
    { "user", required_argument, 0, 'u'},
    { "delay", required_argument, 0, 'd' },

    { "filtermarker", required_argument, 0, 1000 },
    { "udp", no_argument, 0, 2001 },
    { "tcp", no_argument, 0, 2000 },

    { "systeminfo", no_argument, 0, 's' },
    { "own", no_argument, 0, 'o' },
    { "ignore", required_argument, 0, 'i' },
    { "netload", no_argument, 0, 'n' },
    { "diskload", no_argument, 0, 'D' },
    { "top", required_argument, 0, 't' },
    { "scale", required_argument, 0, 'c' },


    { "help", no_argument, 0, '?' },

    { 0, 0, 0, 0 }
  };

  int c;
  while ((c = getopt_long(argc, argv, "qVWHILh:u:d:soi:nDt:c:?", long_options, NULL)) != -1)
  {
    switch (c)
    {
    case 'q':
      sinfoViewConfig.quiet = true;
      break;
    case 'V':
      cout << argv[0] << " " << VERSION
           << "  (compiled " __DATE__ " " __TIME__ ")"
           << endl;
      exit(0);
      break;
    case 'W':   // web-mode
      printMode = printHTML;
      break;
    case 'H':
      printMode = printHosts;
      break;
    case 'I':
      printMode = printIP;
      break;
    case 1001:
      printMode = printHostsAndIP;
      break;
    case 'L':
      printMode = printHostsLoad;
      break;

    case 'h':
    {
      requestHost=string(optarg);
    }
    break;
    case 2000:
    {
      useUDPConnection=false;
    }
    break;
    case 2001:
    {
      useUDPConnection=true;
    }
    break;
    case 'u':
      sinfoViewConfig.username = string(optarg);
      break;
    case 'd':
      sinfoRequester.delay_millisec = atol(optarg)*1000;
      break;

    case 1000:
      sinfoRequester.filterMarker=string(optarg);
      break;


    case 's':
      sinfoViewConfig.systeminfo = true;
      break;
    case 'o':
      sinfoViewConfig.ownprocesses = true;
      break;
    case 'i':
      sinfoViewConfig.ignoreList.push_back(string(optarg));
      break;
    case 'n':
      sinfoViewConfig.shownetload = true;
      break;
    case 'D':
      sinfoViewConfig.showdiskload = true;
      break;
    case 't':
      sinfoViewConfig.listtop = atoi(optarg);
      break;
    case 'c':
      if (0==strcmp(optarg,"log"))
      {
        sinfoViewConfig.dispScale=SinfoViewConfig::log;
      }
      else if (0==strcmp(optarg,"lin"))
      {
        sinfoViewConfig.dispScale=SinfoViewConfig::lin;
      }
      else if (0==strcmp(optarg,"full"))
      {
        sinfoViewConfig.dispScale=SinfoViewConfig::full;
      }
      else
      {
        cerr << "--scale=" << optarg << " is unknown  use \"lin\", \"log\" or \"full\"" << endl;
        exit(1);
      }
      break;
    case '?':
      printHelpAndExit();
      break;

    default:
      printHelpAndExit("No such option");
    }
  }

  if (!sinfoViewConfig.quiet)
    cerr << argv[0] << " " VERSION "  (compiled " __DATE__ " " __TIME__ ")" << endl;



  if (0 == requestHost.size())
  {
    requestHost=string("127.0.0.1");
//    requestHost=string("::1");
  }

  MessageClient * messageClientPtr;
  if (true==useUDPConnection)
  {
    messageClientPtr = new UDPMessageClient(ioservice, requestHost.c_str(), SINFO_REQUEST_PORT_STRING);
  }
  else
  {
    messageClientPtr = new  TCPMessageClient(ioservice, requestHost.c_str(), SINFO_REQUEST_PORT_STRING);
  }

  // MessageClient <> SinfoRequester
  messageClientPtr->connectionReadySignal.connect(boost::bind(&SinfoRequester::connectionReadySlot, &sinfoRequester));
  sinfoRequester.queueAndSendMessageSignal.connect(boost::bind(&MessageClient::queueAndSendMessageSlot, messageClientPtr, _1));
  messageClientPtr->receivedMessageSignal.connect(boost::bind(&SinfoRequester::receivedMessageSlot, &sinfoRequester, _1));

  switch (printMode)
  {

  case printHosts:
  {
    // SinfoRequester <> User
    sinfoRequester.timeoutSignal.connect(boost::bind(exit1));

    messageClientPtr->stopAfterOneReceivedMessage=true;
    sinfoRequester.stopAfterOneRequest=true;
    ioservice.run();
    sinfoPrintHosts(sinfoData.wsinfoList, sinfoViewConfig.systeminfo);
  }
  break;

  case printIP:
  {
    // SinfoRequester <> User
    sinfoRequester.timeoutSignal.connect(boost::bind(exit1));

    messageClientPtr->stopAfterOneReceivedMessage=true;
    sinfoRequester.stopAfterOneRequest=true;
    ioservice.run();
    sinfoPrintIP(sinfoData.wsinfoList, sinfoViewConfig.systeminfo);
  }
  break;

  case printHostsAndIP:
  {
    // SinfoRequester <> User
    sinfoRequester.timeoutSignal.connect(boost::bind(exit1));

    messageClientPtr->stopAfterOneReceivedMessage=true;
    sinfoRequester.stopAfterOneRequest=true;
    ioservice.run();
    sinfoPrintHostsAndIP(sinfoData.wsinfoList, sinfoViewConfig.systeminfo);
  }
  break;

  case printHostsLoad:
  {
    // SinfoRequester <> User
    sinfoRequester.timeoutSignal.connect(boost::bind(exit1));

    messageClientPtr->stopAfterOneReceivedMessage=true;
    sinfoRequester.stopAfterOneRequest=true;
    ioservice.run();
    sinfoPrintHostsLoad(sinfoData.wsinfoList, sinfoViewConfig.systeminfo, sinfoViewConfig.quiet);
  }
  break;

  case printHTML:
  {
    // SinfoRequester <> User
    sinfoRequester.timeoutSignal.connect(boost::bind(exit1));

    messageClientPtr->stopAfterOneReceivedMessage=true;
    sinfoRequester.stopAfterOneRequest=true;
    ioservice.run();
    SinfoHTML sinfoHTML(sinfoViewConfig, sinfoData);
    sinfoHTML.printHTML();
  }
  break;

  case printInteractive:
  {
    SinfoCurses sinfoCurses(ioservice, sinfoViewConfig, sinfoData);
    sinfoCurses.printInit();
    while (ioservice.run_one())
    {
      // space for queue handling e.g. event handling by variable inspection
    }
  }
  break;
  };
  delete messageClientPtr;


  return 0;
}
