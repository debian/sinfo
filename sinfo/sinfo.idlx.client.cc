/*******************************************************************************
** File gererated from sinfo.idlx
**
** Created: Tue Dec 16 15:03:43 2014
**      by: idlxcompiler version 0.0.1
**
** WARNING!
** All changes made in this file will be lost when recompiling idlx file.
*******************************************************************************/

#include <boost/bind.hpp>
#include "sinfo.idlx.client.h"
#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include "messagestlcontainertypes.h"
using namespace Msg;
#include "templatereplyparser.h"

enum
{
  GetSinfoListRequest=1,
  GetSinfoListReply=2,
  GetFilteredSinfoListRequest=3,
  GetFilteredSinfoListReply=4
};

SinfoClientRequest::SinfoClientRequest()
{
}

void SinfoClientRequest::getSinfoListRequest()
{
  Message message;
  pushFrontuint8(message,GetSinfoListRequest);
  sinfoClientRequestMessageSignal(message);
};

void SinfoClientRequest::getFilteredSinfoListRequest(const std::string & filterMarker)
{
  Message message;
  /**/pushFront(message, (std::string)filterMarker);
  pushFrontuint8(message,GetFilteredSinfoListRequest);
  sinfoClientRequestMessageSignal(message);
};

SinfoClientReply::SinfoClientReply()
{
  TemplateReplyParser_1< std::list < Wsinfo > > * getSinfoListReplyParserPtr = new TemplateReplyParser_1< std::list < Wsinfo > >;
  getSinfoListReplyParserPtr->callWorkerSignal.connect(boost::bind(&SinfoClientReply::getSinfoListReply, this, _1));
  compositeReplyParser.addReplyParser(GetSinfoListReply, ReplyParser::SPtr(getSinfoListReplyParserPtr));

  TemplateReplyParser_1< std::list < Wsinfo > > * getFilteredSinfoListReplyParserPtr = new TemplateReplyParser_1< std::list < Wsinfo > >;
  getFilteredSinfoListReplyParserPtr->callWorkerSignal.connect(boost::bind(&SinfoClientReply::getFilteredSinfoListReply, this, _1));
  compositeReplyParser.addReplyParser(GetFilteredSinfoListReply, ReplyParser::SPtr(getFilteredSinfoListReplyParserPtr));

}
