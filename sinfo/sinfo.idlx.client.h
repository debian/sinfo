#ifndef _sinfo_idlx_client_h
#define _sinfo_idlx_client_h

/*******************************************************************************
** File gererated from sinfo.idlx
**
** Created: Tue Dec 16 15:03:43 2014
**      by: idlxcompiler version 0.0.1
**
** WARNING!
** All changes made in this file will be lost when recompiling idlx file.
*******************************************************************************/

#include <boost/signals2.hpp>
#include "messagesinfotypes.h"
#include "replyparser.h"

class SinfoClientRequest
{
public:
  SinfoClientRequest();

  void getSinfoListRequest();
  void getFilteredSinfoListRequest(const std::string & filterMarker);

  boost::signals2::signal<void (Message & message)> sinfoClientRequestMessageSignal;
};


class SinfoClientReply
{
public:
  SinfoClientReply();

  CompositeReplyParser compositeReplyParser;
  virtual void getSinfoListReply(const std::list < Wsinfo > & wsinfoList) = 0;
  virtual void getFilteredSinfoListReply(const std::list < Wsinfo > & wsinfoList) = 0;

};

#endif
