#include <unistd.h>
#include <boost/bind.hpp>
#include <signal.h>
#include "sinfocurses.h"
using namespace std;


#define HOSTNAMEMAXSIZE 30


void SinfoCurses:: printInteractiveOneHost(int maxy, int maxx, int & y, int & x, bool colormode, const Wsinfo & wsinfo, float speedMin, float speedMax)
{
  int attr_normal;
  int attr_wsname;
  int attr_wrkbar;
  int attr_idlebar;
  int attr_usermark;
  int attr_warn;
  int attr_my_computer;

  if (colormode)
  {
    attr_normal = A_NORMAL;
    attr_wsname = A_REVERSE | A_BOLD | COLOR_PAIR(1);
    attr_wrkbar = A_REVERSE | A_BOLD | COLOR_PAIR(2);
    attr_idlebar = A_NORMAL | COLOR_PAIR(3);
    attr_usermark = A_BOLD | COLOR_PAIR(4);
    attr_warn = A_BOLD | A_BLINK | COLOR_PAIR(4);
    attr_my_computer = A_REVERSE | A_BOLD | COLOR_PAIR(5);
  }
  else
  {
    attr_normal = A_NORMAL;
    attr_wsname = A_REVERSE | A_BOLD;
    attr_wrkbar = A_REVERSE | A_BOLD;
    attr_idlebar = A_NORMAL;
    attr_usermark = A_BOLD;
    attr_warn = A_BOLD | A_BLINK;
    attr_my_computer = A_REVERSE | A_BOLD;
  }


  if ((y > 0) && (y < maxy))
  {
    // first line: hostname and load-bar
    if ((config.hostname.size()>0) && (wsinfo.name==string(config.hostname)))
    {
      wattrset(win, attr_my_computer);
    }
    else
    {
      wattrset(win, attr_wsname);
    }
    mvwprintw(win, y, 0, "%s", wsinfo.name.c_str());
    wattrset(win, attr_normal);

    x = wsinfo.name.size();
    while (x < HOSTNAMEMAXSIZE)
    {
      wprintw(win, " ");
      x++;
    }

    //    if (d_total != 0)
    {
      // variable length of bar-display
      long lengthmax = maxx - 32;
      // thim minimum is a 80-col display
      if (lengthmax < 48)
        lengthmax = 48;

      long length = lengthmax;
      if ((wsinfo.cpuinfo.cpus > 0) && (speedMin!=speedMax))
      {
        float currentspeed=wsinfo.cpuinfo.cpus * wsinfo.cpuinfo.speedmhz;
        switch (config.dispScale)
        {
        case SinfoViewConfig::log:
        {
          length = 4 + long(rint( (length-4) * log10( (currentspeed-speedMin)*(10.-1.) / (speedMax-speedMin)  +1. ) ));
        }
        break;

        case SinfoViewConfig::lin:
        {
          length = 4 + long(rint( (length-4) * (currentspeed-speedMin) / (speedMax-speedMin) ));
        }
        break;

        case SinfoViewConfig::full:
          break;
        }
      }

      int p = 0;
      wattrset(win, attr_wrkbar);

      while (p < long( wsinfo.cpustat.userp * length ))
      {
        wprintw(win, "u");
        p++;
      }
      while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp) * length ))
      {
        wprintw(win, "s");
        p++;
      }
      while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep) * length ))
      {
        wprintw(win, "n");
        p++;
      }
      while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep+wsinfo.cpustat.iowaitp) * length ))
      {
        wprintw(win, "w");
        p++;
      }
      while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep+wsinfo.cpustat.iowaitp+wsinfo.cpustat.irqp+wsinfo.cpustat.softirqp) * length ))
      {
        wprintw(win, "q");
        p++;
      }
      wattrset(win, attr_idlebar);
      while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep+wsinfo.cpustat.idlep+wsinfo.cpustat.iowaitp+wsinfo.cpustat.irqp+wsinfo.cpustat.softirqp) * length ))
      {
        wprintw(win, "i");
        p++;
      }
      wattrset(win, attr_normal);
      while (p < lengthmax)
      {
        wprintw(win, "-");
        p++;
      }
      wclrtoeol(win);
    }
  }
  y++;


  // second line: meminfo and   cpu-utilization in percent
  //  if (d_total != 0)
  {
    if ((y > 0) && (y < maxy))
    {
      // variable length of bar-display
      long lengthmax = maxx - 32;
      // thim minimum is a 80-col display
      if (lengthmax < 48)
        lengthmax = 48;

      mvwprintw(win, y, 0, "(%2d) ", wsinfo.users.number);

      if (wsinfo.meminfo.mem_total == 0)
      {
        wprintw(win, "mem:  ---  ");
      }
      else
      {
        wprintw(win, "mem:%5.1f%% ",
                float(wsinfo.meminfo.mem_used) / float(wsinfo.meminfo.mem_total)*100.);
      }

      if (float(wsinfo.meminfo.swap_used) / float(wsinfo.meminfo.swap_total) > 0.25)
        wattrset(win, attr_warn);
      if (wsinfo.meminfo.swap_total == 0)
      {
        wprintw(win, "swap:  ---    ");
      }
      else
      {
        wprintw(win, "swap:%5.1f%%   ",
                float(wsinfo.meminfo.swap_used) / float(wsinfo.meminfo.swap_total)*100.);
      }

      wattrset(win, attr_normal);

      long lengthmax_spaces = lengthmax - (9+9+9+9+9);
      int p = 0;
      wprintw(win, "us:%5.1f%%",
              wsinfo.cpustat.userp*100.);
      for (; p < lengthmax_spaces / 4; p++)
        wprintw(win, " ");
      wprintw(win, "sy:%5.1f%%",
              wsinfo.cpustat.sysp*100.);
      for (; p < lengthmax_spaces*2 / 4; p++)
        wprintw(win, " ");
      wprintw(win, "ni:%5.1f%%",
              wsinfo.cpustat.nicep*100.);
      for (; p < lengthmax_spaces*3 / 4; p++)
        wprintw(win, " ");
      wprintw(win, "wa:%5.1f%%",
              wsinfo.cpustat.iowaitp*100.);
      for (; p < lengthmax_spaces; p++)
        wprintw(win, " ");
      wprintw(win, "id:%5.1f%%",
              wsinfo.cpustat.idlep*100.);
      wclrtoeol(win);
    }
    y++;
  }

  if (config.shownetload)
  {
    if ( (wsinfo.netload.rxbytes != 0) ||
         (wsinfo.netload.txbytes != 0) ||
         (wsinfo.netload.rxpkt != 0) ||
         (wsinfo.netload.txpkt != 0) )
    {
      if ((y > 0) && (y < maxy))
      {
        mvwprintw(win, y, 0, "   net: %10s RX:%11.1f pkt/s   %11.1f Byte/s",
                  wsinfo.netload.iface.c_str(),
                  wsinfo.netload.rxpkt,
                  wsinfo.netload.rxbytes);
        wclrtoeol(win);
      }
      y++;
      if ((y > 0) && (y < maxy))
      {
        mvwprintw(win, y, 0, "        %10s TX:%11.1f pkt/s   %11.1f Byte/s",
                  wsinfo.netload.iface.c_str(),
                  wsinfo.netload.txpkt,
                  wsinfo.netload.txbytes);
        wclrtoeol(win);
      }
      y++;
    }
  }

  if (config.showdiskload)
  {
    if ( (wsinfo.diskload.readkbytespersec >= 0) ||
         (wsinfo.diskload.writekbytespersec >= 0) )
    {
      if ((y > 0) && (y < maxy))
      {
        mvwprintw(win, y, 0, "   disk:  read:%11.1f kByte/s   write:%11.1f kByte/s",
                  wsinfo.diskload.readkbytespersec,
                  wsinfo.diskload.writekbytespersec);
        wclrtoeol(win);
      }
      y++;
    }
  }

  if (config.systeminfo)
  {
    if (wsinfo.unameinfo.sysname[0] != 0)
    {
      if ((y > 0) && (y < maxy))
      {
        mvwprintw(win, y, 0, "   %s", wsinfo.inetaddr.c_str());
        wprintw(win, "   %s", wsinfo.unameinfo.nodename.c_str());
        wprintw(win, " %s", wsinfo.unameinfo.machine.c_str());
        wprintw(win, " %s", wsinfo.unameinfo.sysname.c_str());
        wprintw(win, " %s", wsinfo.unameinfo.release.c_str());
        wprintw(win, " %s", wsinfo.unameinfo.version.c_str());
        wclrtoeol(win);
      }
      y++;
    }

    if (wsinfo.cpuinfo.cpus != 0)
    {
      if ((y > 0) && (y < maxy))
      {
        if (wsinfo.marker.size()>0)
        {
          mvwprintw(win, y, 0, "   cpus: %i  MHz: %6.1f  marker: %s", wsinfo.cpuinfo.cpus, wsinfo.cpuinfo.speedmhz, wsinfo.marker.c_str());
        }
        else
        {
          mvwprintw(win, y, 0, "   cpus: %i  MHz: %6.1f", wsinfo.cpuinfo.cpus, wsinfo.cpuinfo.speedmhz);
        }
        wclrtoeol(win);
      }
      y++;
    }

    if ((y > 0) && (y < maxy))
    {
      mvwprintw(win, y, 0, "   RAM: %.1f MByte   "
                "swap: %.1f Mbyte",
                wsinfo.meminfo.mem_total / 1048576.,
                wsinfo.meminfo.swap_total / 1048576.);
      wclrtoeol(win);
    }
    y++;

    if ((y > 0) && (y < maxy))
    {
      mvwprintw(win, y, 0, "   load 1min:%5.1f   load 5min:%5.1f   load 15min:%5.1f",
                wsinfo.loadavg.load1,
                wsinfo.loadavg.load5,
                wsinfo.loadavg.load15);
      wclrtoeol(win);
    }
    y++;

    if ((y > 0) && (y < maxy))
    {
      long sec = wsinfo.uptime.seconds;
      long hour = long(sec / (60 * 60));
      sec -= hour * 60 * 60;

      long min = long(sec / 60);
      sec -= min * 60;

      mvwprintw(win, y, 0, "   uptime     %ld days, %2ld:%02ld:%02ld",
                wsinfo.uptime.days, hour, min, sec);
      wclrtoeol(win);
    }
    y++;
  } // if (systeminfo)


  {
    ProcinfoList::const_iterator plit;
    int ii = 0;
    for (plit=wsinfo.procinfoList.begin();
         plit!=wsinfo.procinfoList.end()
         && ((ii < config.listtop) || (config.ownprocesses));
         ++plit, ++ii)
    {
      bool own = false;
      if ( (config.username.size()>0) && (plit->username == config.username) )
        own = true;

      // ignore some processes on the presented list
      // FIXME: not my favorite solution to suppress some processes, because this will
      // change the number of displyed processes, depending on the sorting at the
      // demon side....
      bool ignore = false;
      for (list < string > ::iterator it = config.ignoreList.begin();
           it != config.ignoreList.end();
           ++it)
      {
        if ( plit->command == (*it) )
        {
          ignore=true;
        }
      }

      if ((!ignore) && ((!config.ownprocesses) || ((config.ownprocesses) && (own))))
      {
        if ((y > 0) && (y < maxy))
        {
          if ( (config.username.size()>0) && (plit->username == config.username) )
            wattrset(win, attr_usermark);
          else
            wattrset(win, attr_normal);

          mvwprintw(win, y, 0, "  %10s %c %5.1f %3d %s",
                    plit->username.c_str(),
                    plit->state,
                    plit->cpupercent,
                    plit->priority,
                    plit->command.c_str());
          wclrtoeol(win);
        }
        y++;
      }
    }
  }
}


void SinfoCurses::printInteractiveStatusLine(long cpuCount, float speedUsed, float speedSum)
{
  // keep the status line!
  {
    wattrset(win, A_NORMAL);

    switch (barState)
    {
    case 0:
      mvwprintw(win, 0, 0, "-");
      break;
    case 1:
      mvwprintw(win, 0, 0, "\\");
      break;
    case 2:
      mvwprintw(win, 0, 0, "|");
      break;
    case 3:
      mvwprintw(win, 0, 0, "/");
      break;
    }

    mvwprintw(win, 0, 1, " %d nodes, %ld CPUs   "
              "total CPU utilization: %5.1f%% ( %5.3f GHz / %5.3f GHz )",
              wsinfoList.size(),
              cpuCount,
              speedUsed / speedSum*100,
              speedUsed / 1000,
              speedSum / 1000.);


    wclrtoeol(win);
  }
  y++;
}


static void atexitFunction()
{
  static bool enwin_already_called = false;

  if (!enwin_already_called)
  {
    endwin();
    enwin_already_called = true;
  }
}


static void finish(int sig)
{
  // call the normal atexit-chain for propper screen restoration
  exit(0);
}


void SinfoCurses::printInit()
{
  signal(SIGINT, finish);                  // exit program with Strg-C
  atexit(atexitFunction);                  // every other exit (on socket-errors or similar!)

  initscr();
  noecho();
  cbreak();
  nonl();      // disable newline translation
  curs_set(0); // cursor invisible

  colormode = has_colors();
  if (colormode)
  {
    start_color();
    int my_bg = COLOR_BLACK;
#ifdef HAVEDEFAULTCOLORS
    if (use_default_colors() == OK)
      my_bg = -1;
#endif
    init_pair(1, COLOR_BLUE, COLOR_WHITE);
    init_pair(2, COLOR_RED, COLOR_WHITE);
    init_pair(3, COLOR_BLACK, COLOR_GREEN);
    init_pair(4, COLOR_MAGENTA, my_bg);
    init_pair(5, COLOR_CYAN, COLOR_WHITE);
  }

  win = newwin(0, 0, 0, 0);
  keypad(win, true);
  wtimeout(win,10); // wgetch with delay: 10ms


  starty = 0;
  barState = 0;
//  printAll();
}


void SinfoCurses::printAll()
{
  if (config.listtop < 0)
  {
    // ATTENTION: only called on init!
    int y;
    int dummy;
    getmaxyx(win, y, dummy);

    // one line for the header... two lines are default for each node
    y -= 1 + wsinfoList.size() * 2;
    if ((y < int(wsinfoList.size())) || (wsinfoList.empty()))
      config.listtop = 1;
    else
      config.listtop = y / wsinfoList.size();

    long procinfoListMax = sinfoData.getProcinfoListMax();
    if (config.listtop > procinfoListMax)
      config.listtop = procinfoListMax;
  }

  int maxx;
  getmaxyx(win, maxy, maxx);
  y = starty;
  int x = 0;

  // clear screen if the number of entries has changed!
  static unsigned int lastWsinfoListCount = 0;
  if (lastWsinfoListCount != wsinfoList.size())
  {
    lastWsinfoListCount = wsinfoList.size();
    wclear(win);
  }

  float speedSum, speedUsed, speedMin, speedMax = 0;
  long cpuCount=0;
  sinfoData.calcSpeedInfo(cpuCount, speedSum, speedUsed, speedMin, speedMax);
  printInteractiveStatusLine(cpuCount, speedUsed, speedSum);
  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
    printInteractiveOneHost(maxy, maxx, y, x, colormode, *it, speedMin, speedMax);

  wattrset(win, A_NORMAL);
  wclrtobot(win);
  mvwprintw(win, maxy - 1, maxx - 8, "UP/DOWN");

  wrefresh(win);
}


void SinfoCurses::keyCodeEvent(int key)
{
  switch (key)
  {
  case KEY_RESIZE:
    werase(win);
    break;

  case KEY_HOME:
  case KEY_BEG:
    starty = 0;
    break;

  case KEY_DOWN:
  case 'd':
    if (y >= maxy)
    {
      starty--;
    }
    break;

  case KEY_NPAGE:
    if (y >= maxy)
    {
      starty -= (maxy - 1);
    }
    break;


  case KEY_UP:
  case 'u':
    if (starty < 0)
    {
      starty++;
    }
    break;

  case KEY_PPAGE:
    starty += (maxy - 1);
    if (starty > 0)
      starty = 0;
    break;

  case 's':
    config.systeminfo = !config.systeminfo;
    break;

  case 'c':
    switch (config.dispScale)
    {
    case SinfoViewConfig::log:
      config.dispScale=SinfoViewConfig::lin;
      break;
    case SinfoViewConfig::lin:
      config.dispScale=SinfoViewConfig::full;
      break;
    case SinfoViewConfig::full:
      config.dispScale=SinfoViewConfig::log;
      break;
    }
    break;

  case 'o':
    config.ownprocesses = !config.ownprocesses;
    break;

  case 'n':
    config.shownetload = !config.shownetload;
    break;

  case 'D':
    config.showdiskload = !config.showdiskload;
    break;

  case 't':
    if (config.ownprocesses)
    {
      config.ownprocesses = false;
    }
    else
    {
      long procinfoListMax = sinfoData.getProcinfoListMax();
      config.listtop = (config.listtop + 1) % (procinfoListMax + 1);
    }
    break;

  case 'q':
    exit(0);
    break;
  }
}


void SinfoCurses::wsinfoListChangedSlot()
{
  barState = (barState + 1) % 4;
  printAll();
}


void SinfoCurses::handle_read(const boost::system::error_code& e,
                              std::size_t bytes_transferred)
{
  if (!e)
  {
    int key;
    while (ERR !=(key = wgetch(win)) )
    {
      keyCodeEvent(key);
    }

    printAll();
    instream.async_read_some(boost::asio::null_buffers(),
                             boost::bind(&SinfoCurses::handle_read, this,
                                         boost::asio::placeholders::error,
                                         boost::asio::placeholders::bytes_transferred));
  }
}


SinfoCurses::SinfoCurses(boost::asio::io_service& io_service, SinfoViewConfig & _config, SinfoData & _sinfoData)
  :ioservice(io_service) , config(_config), sinfoData(_sinfoData), wsinfoList(_sinfoData.wsinfoList), instream(ioservice,dup(0))
{
  barState=0;

  instream.async_read_some(boost::asio::null_buffers(),
                           boost::bind(&SinfoCurses::handle_read, this,
                                       boost::asio::placeholders::error,
                                       boost::asio::placeholders::bytes_transferred));

  sinfoData.changedSignal.connect(boost::bind(&SinfoCurses::wsinfoListChangedSlot, this));
}
