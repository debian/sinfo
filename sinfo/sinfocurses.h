#ifndef _sinfocurses_h
#define _sinfocurses_h


#include <list>
#include <string>
#include <boost/asio.hpp>
#ifdef HAVE_CURSES_H
#include <curses.h>
#endif
#ifdef HAVE_NCURSES_H
#include <ncurses.h>
#endif
#include "sinfodata.h"
#include "sinfoviewconfig.h"


class SinfoCurses
{
private:
  boost::asio::io_service & ioservice;
  SinfoViewConfig & config;
  SinfoData & sinfoData;
  std::list < Wsinfo > & wsinfoList; // located within sinfoData - to minimize code change

  bool colormode;
  int barState;
  WINDOW * win;
  int starty;
  int maxy;
  int y;

  void printInteractiveStatusLine(long cpuCount, float speedUsed, float speedSum);
  void printInteractiveOneHost(int maxy, int maxx, int & y, int & x, bool colormode, const Wsinfo & wsinfo, float speedMin, float speedMax);

  // keyboard handling   stdin => curses
  void handle_read(const boost::system::error_code& e, std::size_t bytes_transferred); // Handle completion of a read operation.
  boost::asio::posix::stream_descriptor instream;                                    // stream on stdin

  void keyCodeEvent(int key);
  void wsinfoListChangedSlot();


  void printAll();


public:
  SinfoCurses(boost::asio::io_service& io_service, SinfoViewConfig & _config, SinfoData & _sinfoData);

  void printInit();
};


#endif // _sinfocurses_h
