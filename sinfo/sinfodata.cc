#include <list>
#include "sinfodata.h"
using namespace std;


SinfoData::SinfoData()
{
}


void SinfoData::calcSpeedInfo(long & cpuCount, float & speedSum, float & speedUsed, float & speedMin, float & speedMax)
{
  // search for the fastest machine
  cpuCount = 0;
  speedSum = 0.;
  speedUsed = 0.;
  speedMin = 1e200;
  speedMax = 0;


  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {
    cpuCount+=it->cpuinfo.cpus;
    if (it->cpuinfo.cpus * it->cpuinfo.speedmhz > speedMax)
    {
      speedMax = it->cpuinfo.cpus * it->cpuinfo.speedmhz;
    }

    if (it->cpuinfo.cpus * it->cpuinfo.speedmhz < speedMin)
    {
      speedMin = it->cpuinfo.cpus * it->cpuinfo.speedmhz;
    }

    speedSum += it->cpuinfo.cpus * it->cpuinfo.speedmhz;

    speedUsed += (1.-it->cpustat.idlep)
                 * it->cpuinfo.cpus * it->cpuinfo.speedmhz;
  }
}


long SinfoData::getProcinfoListMax()
{
  unsigned long procinfoListMax=0;
  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {
    if (it->procinfoList.size()>procinfoListMax)
    {
      procinfoListMax=it->procinfoList.size();
    }
  }
  return procinfoListMax;
}

