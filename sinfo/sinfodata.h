#ifndef _sinfodata_h
#define _sinfodata_h


#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <list>
#include "sinfo.h"

class SinfoData
{
public:
  std::list < Wsinfo > wsinfoList;


public:
  SinfoData();

  void calcSpeedInfo(long & cpuCount, float & speedSum, float & speedUsed, float & speedMin, float & speedMax);
  long getProcinfoListMax();

  boost::signals2::signal<void ()> changedSignal;
};


#endif // _sinfodata_h
