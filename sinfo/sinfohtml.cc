#include <math.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include "sinfohtml.h"
using namespace std;


#define HOSTNAMEMAXSIZE 30


string SinfoHTML::quoteHTML(const string & str)
{
  string ret = "";
  for (unsigned int i = 0; i < str.size(); i++)
  {
    switch (str[i])
    {
    case ' ':
      ret += "&nbsp;";
      break;

    case '<':
      ret += "&lt;";
      break;

    case '>':
      ret += "&gt;";
      break;

    case '&':
      ret += "&amp;";
      break;

    default:
      ret += char(str[i]);
    }
  }
  return ret;
}


void SinfoHTML::printHTMLStatusLine(float speedUsed, float speedSum)
{
  const int LINESIZE = 81;
  char line[LINESIZE];
  snprintf(line, LINESIZE, " %d nodes       total CPU utilization: %5.1f%% ( %5.3f GHz / %5.3f GHz )",
           wsinfoList.size(),
           speedUsed / speedSum*100,
           speedUsed / 1000,
           speedSum / 1000.);
  cout << quoteHTML(line)
       << "<BR>"
       << endl;
}


void SinfoHTML::printHTMLOneHost(int maxx, const Wsinfo & wsinfo, float speedMin, float speedMax)
{
  const int LINESIZE = 81;
  char line[LINESIZE];

  // first line: hostname and load-bar
  ////////////////////////////////////
  if ((config.hostname.size()>0) && (wsinfo.name==string(config.hostname)))
    cout << "<SPAN class=\"myhostname\">";
  else
    cout << "<SPAN class=\"hostname\">";
  cout << wsinfo.name;
  cout << "</SPAN>";
  for (unsigned int i = wsinfo.name.size(); i < HOSTNAMEMAXSIZE; i++)
    cout << "&nbsp;";

  //  if (d_total != 0)
  {
    // variable length of bar-display
    long lengthmax = maxx - 32;
    // thim minimum is a 80-col display
    if (lengthmax < 48)
      lengthmax = 48;


    long length = lengthmax;
    if ((wsinfo.cpuinfo.cpus > 0) && (speedMin!=speedMax))
    {
      float currentspeed=wsinfo.cpuinfo.cpus * wsinfo.cpuinfo.speedmhz;
      switch (config.dispScale)
      {
      case SinfoViewConfig::log:
      {
        length = 4 + long(rint( (length-4) * log10( (currentspeed-speedMin)*(10.-1.) / (speedMax-speedMin)  +1. ) ));
      }
      break;

      case SinfoViewConfig::lin:
      {
        length = 4 + long(rint( (length-4) * (currentspeed-speedMin) / (speedMax-speedMin) ));
      }
      break;

      case SinfoViewConfig::full:
        break;
      }
    }

    int p = 0;
    cout << "<SPAN class=\"cpuuser\">";
    while (p < long( wsinfo.cpustat.userp * length ))
    {
      cout << "u";
      p++;
    }
    cout << "</SPAN>";

    cout << "<SPAN class=\"cpusystem\">";
    while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp) * length ))
    {
      cout << "s";
      p++;
    }
    cout << "</SPAN>";

    cout << "<SPAN class=\"cpunice\">";
    while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep) * length ))
    {
      cout << "n";
      p++;
    }
    cout << "</SPAN>";

    cout << "<SPAN class=\"cpuiowait\">";
    while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep+wsinfo.cpustat.iowaitp) * length ))
    {
      cout << "w";
      p++;
    }
    cout << "</SPAN>";

    cout << "<SPAN class=\"cpuirq\">";
    while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep+wsinfo.cpustat.iowaitp+wsinfo.cpustat.irqp+wsinfo.cpustat.softirqp) * length ))
    {
      cout << "q";
      p++;
    }
    cout << "</SPAN>";

    cout << "<SPAN class=\"cpuidle\">";
    while (p < long( (wsinfo.cpustat.userp+wsinfo.cpustat.sysp+wsinfo.cpustat.nicep+wsinfo.cpustat.idlep+wsinfo.cpustat.iowaitp+wsinfo.cpustat.irqp+wsinfo.cpustat.softirqp) * length ))
    {
      cout << "i";
      p++;
    }
    cout << "</SPAN>";

    while (p < lengthmax)
    {
      cout << "-";
      p++;
    }
  }
  cout << "<BR>" << endl;



  // second line: meminfo and   cpu-utilization in percent
  //  if (d_total != 0)
  {
    // variable length of bar-display
    long lengthmax = maxx - 32;
    // thim minimum is a 80-col display
    if (lengthmax < 48)
      lengthmax = 48;

    snprintf(line, LINESIZE, "(%2d) ", wsinfo.users.number);
    cout << quoteHTML(line);

    if (wsinfo.meminfo.mem_total == 0)
    {
      cout << quoteHTML("mem:  ---  ");
    }
    else
    {
      snprintf(line, LINESIZE, "mem:%5.1f%% ",
               double(wsinfo.meminfo.mem_used) / double(wsinfo.meminfo.mem_total)*100.);
      cout << quoteHTML(line);
    }

    if (double(wsinfo.meminfo.swap_used) / double(wsinfo.meminfo.swap_total) > 0.25)
      cout << "<SPAN class=\"swapwarn\">";
    if (wsinfo.meminfo.swap_total == 0)
    {
      cout << quoteHTML("swap:  ---    ");
    }
    else
    {
      snprintf(line, LINESIZE, "swap:%5.1f%%   ",
               double(wsinfo.meminfo.swap_used) / double(wsinfo.meminfo.swap_total)*100.);
      cout << quoteHTML(line);
    }
    if (double(wsinfo.meminfo.swap_used) / double(wsinfo.meminfo.swap_total) > 0.25)
      cout << "</SPAN>";

    long lengthmax_spaces = lengthmax - (9+9+9+9+9);
    int p = 0;
    snprintf(line, LINESIZE, "us:%5.1f%%",
             wsinfo.cpustat.userp*100.);
    cout << quoteHTML(line);
    for (; p < lengthmax_spaces / 4; p++)
      cout << "&nbsp;";
    snprintf(line, LINESIZE, "sy:%5.1f%%",
             wsinfo.cpustat.sysp*100.);
    cout << quoteHTML(line);
    for (; p < lengthmax_spaces*2 / 4; p++)
      cout << "&nbsp;";
    snprintf(line, LINESIZE, "ni:%5.1f%%",
             wsinfo.cpustat.nicep*100.);
    cout << quoteHTML(line);
    for (; p < lengthmax_spaces*3 / 4; p++)
      cout << "&nbsp;";
    snprintf(line, LINESIZE, "wa:%5.1f%%",
             wsinfo.cpustat.iowaitp*100.);
    cout << quoteHTML(line);
    for (; p < lengthmax_spaces; p++)
      cout << "&nbsp;";
    snprintf(line, LINESIZE, "id:%5.1f%%",
             wsinfo.cpustat.idlep*100.);
    cout << quoteHTML(line);
    cout << "<BR>" << endl;
  }



  if (config.shownetload)
  {
    if ( (wsinfo.netload.rxbytes != 0) ||
         (wsinfo.netload.txbytes != 0) ||
         (wsinfo.netload.rxpkt != 0) ||
         (wsinfo.netload.txpkt != 0) )
    {

      snprintf(line, LINESIZE, "   net: %10s RX:%11.1f pkt/s   %11.1f Bytes/s",
               wsinfo.netload.iface.c_str(),
               wsinfo.netload.rxpkt,
               wsinfo.netload.rxbytes);
      cout << quoteHTML(line) << "<BR>" << endl;


      snprintf(line, LINESIZE, "        %10s TX:%11.1f pkt/s   %11.1f Bytes/s",
               wsinfo.netload.iface.c_str(),
               wsinfo.netload.txpkt,
               wsinfo.netload.txbytes);
      cout << quoteHTML(line) << "<BR>" << endl;
    }
  }


  if (config.showdiskload)
  {
    if ( (wsinfo.diskload.readkbytespersec >= 0) ||
         (wsinfo.diskload.writekbytespersec >= 0) )
    {
      snprintf(line, LINESIZE,  "   disk:  read:%11.1f kByte/s   write:%11.1f kByte/s",
               wsinfo.diskload.readkbytespersec,
               wsinfo.diskload.writekbytespersec);
      cout << quoteHTML(line) << "<BR>" << endl;
    }
  }


  if (config.systeminfo)
  {
    if (wsinfo.unameinfo.sysname[0] != 0)
    {
      snprintf(line, LINESIZE, "   %s""   %s"" %s"" %s"" %s"" %s",
               wsinfo.inetaddr.c_str(),
               wsinfo.unameinfo.nodename.c_str(),
               wsinfo.unameinfo.machine.c_str(),
               wsinfo.unameinfo.sysname.c_str(),
               wsinfo.unameinfo.release.c_str(),
               wsinfo.unameinfo.version.c_str());
      cout << quoteHTML(line) << "<BR>" << endl;
    }

    if (wsinfo.cpuinfo.cpus != 0)
    {
      snprintf(line, LINESIZE, "   cpus: %i  MHz: %6.1f", wsinfo.cpuinfo.cpus, wsinfo.cpuinfo.speedmhz);
      cout << quoteHTML(line) << "<BR>" << endl;
    }

    {
      snprintf(line, LINESIZE, "   RAM: %.lf MByte   swap: %.lf Mbyte",
               wsinfo.meminfo.mem_total / 1048576,
               wsinfo.meminfo.swap_total / 1048576);
      cout << quoteHTML(line) << "<BR>" << endl;
    }

    {
      snprintf(line, LINESIZE, "   load 1min:%5.1f   load 5min:%5.1f   load 15min:%5.1f",
               wsinfo.loadavg.load1,
               wsinfo.loadavg.load5,
               wsinfo.loadavg.load15);
      cout << quoteHTML(line) << "<BR>" << endl;
    }

    {
      long sec = wsinfo.uptime.seconds;
      long hour = long(sec / (60 * 60));
      sec -= hour * 60 * 60;

      long min = long(sec / 60);
      sec -= min * 60;

      snprintf(line, LINESIZE, "   uptime     %d days, %2ld:%02ld:%02ld",
               wsinfo.uptime.days, hour, min, sec);
      cout << quoteHTML(line) << "<BR>" << endl;
    }
  } // if (systeminfo)


  {
    ProcinfoList::const_iterator plit;
    int ii = 0;
    for (plit=wsinfo.procinfoList.begin();
         plit!=wsinfo.procinfoList.end()
         && ((ii < config.listtop) || (config.ownprocesses));
         ++plit, ++ii)
    {
      bool own = false;
      if ( (config.username.size()>0) && (plit->username == config.username) )
        own = true;

      if ((!config.ownprocesses) || ((config.ownprocesses) && (own)))
      {
        snprintf(line, LINESIZE, "  %10s %c %5.1f %3d %s",
                 plit->username.c_str(),
                 plit->state,
                 plit->cpupercent,
                 plit->priority,
                 plit->command.c_str());
        if ( (config.username.size()>0) && (plit->username == config.username) )
          cout << "<SPAN class=\"myprocess\">" << quoteHTML(line) << "</SPAN><BR>" << endl;
        else
          cout << quoteHTML(line) << "<BR>" << endl;
      }
    }
  }
}


void SinfoHTML::printHTML()
{
  float speedSum, speedUsed, speedMin, speedMax = 0;
  long cpuCount=0;
  sinfoData.calcSpeedInfo(cpuCount, speedSum, speedUsed, speedMin, speedMax);

  printHTMLStatusLine(speedUsed, speedSum);
  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
    printHTMLOneHost(80, *it, speedMin, speedMax);
}


SinfoHTML::SinfoHTML(SinfoViewConfig & _config, SinfoData & _sinfoData)
  :config(_config), sinfoData(_sinfoData), wsinfoList(_sinfoData.wsinfoList)
{
}

