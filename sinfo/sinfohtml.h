#ifndef _sinfohtml_h
#define _sinfohtml_h


#include <list>
#include <string>
#include "sinfodata.h"
#include "sinfoviewconfig.h"
#include "message.h"


class SinfoHTML
{
private:
  SinfoViewConfig & config;
  SinfoData & sinfoData;
  std::list < Wsinfo > & wsinfoList; // located within sinfoData - to minimize code change

  std::string quoteHTML(const std::string & str);
  void printHTMLStatusLine(float speedUsed, float speedSum);
  void printHTMLOneHost(int maxx, const Wsinfo & wsinfo, float speedMin, float speedMax);


public:
  SinfoHTML(SinfoViewConfig & _config, SinfoData & _sinfoData);

  void printHTML();
};


#endif // _sinfohtml_h
