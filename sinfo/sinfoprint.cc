#include <iostream>
#include <iomanip>
#include "sinfoprint.h"
using namespace std;


#define HOSTNAMEMAXSIZE 30


void sinfoPrintHosts(list < Wsinfo > & wsinfoList, bool systeminfo)
{
  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {
    cout << it->name << endl;
    if (systeminfo)
    {
      if (it->unameinfo.sysname[0] != 0)
        cout << "  " << it->unameinfo.sysname;
      if (it->cpuinfo.cpus != 0)
        cout << "  "
             << "cpus: " << it->cpuinfo.cpus
             << "  "
             << "MHz: " << setw(6) << setprecision(4) << it->cpuinfo.speedmhz
             << endl;

    }
  }
}


void sinfoPrintIP(list < Wsinfo > & wsinfoList, bool systeminfo)
{
  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {
    cout << it->inetaddr << endl;
    if (systeminfo)
    {
      if (it->unameinfo.sysname[0] != 0)
        cout << "  " << it->unameinfo.sysname;
      if (it->cpuinfo.cpus != 0)
        cout << "  "
             << "cpus: " << it->cpuinfo.cpus
             << "  "
             << "MHz: " << setw(6) << setprecision(4) << it->cpuinfo.speedmhz
             << endl;

    }
  }
}


void sinfoPrintHostsAndIP(list < Wsinfo > & wsinfoList, bool systeminfo)
{
  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {
    cout << it->name
         << " "
         << it->inetaddr
         << endl;
    if (systeminfo)
    {
      if (it->unameinfo.sysname[0] != 0)
        cout << "  " << it->unameinfo.sysname;
      if (it->cpuinfo.cpus != 0)
        cout << "  "
             << "cpus: " << it->cpuinfo.cpus
             << "  "
             << "MHz: " << setw(6) << setprecision(4) << it->cpuinfo.speedmhz
             << endl;

    }
  }
}


void sinfoPrintHostsLoad(list < Wsinfo > & wsinfoList, bool systeminfo, bool quiet)
{
  if (!quiet)
  {
    cerr << "hostname                       ";
    if (systeminfo)
    {
      cerr << "cpus speed ";
    }
    cerr << "system_load" << endl;
  }

  for (list < Wsinfo > ::iterator it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {

    cout << it->name << " ";
    for (unsigned int i = it->name.size(); i < HOSTNAMEMAXSIZE; i++)
      cout << " ";
    if (systeminfo)
    {
      cout <<  it->cpuinfo.cpus
           << "  "
           <<  setw(6) << setprecision(4) << it->cpuinfo.speedmhz
           << "  ";
    }
    cout << 1.-it->cpustat.idlep << endl;
  }
}
