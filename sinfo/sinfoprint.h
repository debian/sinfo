#ifndef _sinfoprint_h
#define _sinfoprint_h


#include <list>
#include "sinfo.h"


void sinfoPrintHosts(std::list < Wsinfo > & wsinfoList, bool systeminfo);
void sinfoPrintIP(std::list < Wsinfo > & wsinfoList, bool systeminfo);
void sinfoPrintHostsAndIP(std::list < Wsinfo > & wsinfoList, bool systeminfo);
void sinfoPrintHostsLoad(std::list < Wsinfo > & wsinfoList, bool systeminfo, bool quiet);


#endif // _sinfoprint_h
