#include <string>
#include <boost/bind.hpp>
#include "sinforequester.h"
#include "messagebasictypes.h"
using namespace std;
using namespace Msg;


SinfoRequester::SinfoRequester(ProtocolIO & protocolIO, SinfoData & _sinfoData)
  : sinfoData(_sinfoData), waitForConnectionReadyTimer(protocolIO), requestUpdateTimer(protocolIO), periodicRequestTimer(protocolIO)
{
  delay_millisec=1000;
  timeout_millisec=1000;
  stopAfterOneRequest=false;

  waitForConnectionReadyTimer.startAlarmAfter(boost::posix_time::millisec(timeout_millisec), boost::bind(&SinfoRequester::waitForConnectionReadyTimerExpiredEvent, this));

  sinfoClientRequestMessageSignal.connect(boost::bind(&SinfoRequester::sinfoClientRequestMessageSlot, this,_1));
}



void SinfoRequester::sinfoClientRequestMessageSlot(Message & message)
{
  pushFrontuint8(message,2);
  queueAndSendMessageSignal(message);
}


void SinfoRequester::waitForConnectionReadyTimerExpiredEvent()
{
  //cout << "SinfoRequester::waitForConnectionReadyTimerExpiredEvent()" << endl;
  timeoutSignal();
  // no new Event queued
}


void SinfoRequester::connectionReadySlot()
{
  waitForConnectionReadyTimer.stop();
  requestUpdate();
}


void SinfoRequester::getSinfoListReply(const std::list< Wsinfo > & wsinfoList)
{
  requestUpdateTimer.stop();

  std::list < Wsinfo > & theWsinfoList=sinfoData.wsinfoList;

  // append data
  theWsinfoList=wsinfoList;

  theWsinfoList.sort();
  // inform all observers that sinfoData has been changed
  sinfoData.changedSignal();

  if (false==stopAfterOneRequest)
  {
    periodicRequestTimer.startAlarmAfter(boost::posix_time::millisec(delay_millisec), boost::bind(&SinfoRequester::periodicRequestTimerExpiredEvent, this));
  }
}


void SinfoRequester::getFilteredSinfoListReply(const std::list< Wsinfo > & wsinfoList)
{
  getSinfoListReply(wsinfoList);
}


void SinfoRequester::requestUpdate()
{
  if (filterMarker.size()>0)
  {
    getFilteredSinfoListRequest(filterMarker);
  }
  else
  {
    getSinfoListRequest();
  }

  requestUpdateTimer.startAlarmAfter(boost::posix_time::millisec(timeout_millisec), boost::bind(&SinfoRequester::requestUpdateTimerExpiredEvent, this));
}


void SinfoRequester::requestUpdateTimerExpiredEvent()
{
  //cout << "SinfoRequester::requestUpdateTimerExpiredEvent()" << endl;
  timeoutSignal();
  if (false==stopAfterOneRequest)
  {
    periodicRequestTimer.startAlarmAfter(boost::posix_time::millisec(delay_millisec), boost::bind(&SinfoRequester::periodicRequestTimerExpiredEvent, this));
  }
}


void SinfoRequester::receivedMessageSlot(Message & returnMessage)
{
  try
  {
    if (returnMessage.size() > 0)
    {
      uint8 messageID;
      popFrontuint8(returnMessage,messageID);
      if (2!=messageID)
        return;

      //cout << "messageID=" << int(messageID) << endl;
      compositeReplyParser.parse(returnMessage);
    }
  }
  catch (MessageException & ex)
  {
    // to ignore errors
    std::cerr << ex.what() << std::endl;
  }
}


void SinfoRequester::periodicRequestTimerExpiredEvent()
{
  requestUpdate();
}
