#ifndef _sinforequester_h
#define _sinforequester_h


#include <boost/signals2.hpp>
#include "sinfodata.h"
#include "protocoltimer.h"
#include "sinfo.idlx.client.h"


class SinfoRequester : public SinfoClientRequest, SinfoClientReply
{
private:
  SinfoData & sinfoData;

  ProtocolTimer waitForConnectionReadyTimer;
  void waitForConnectionReadyTimerExpiredEvent();

  void requestUpdate();


  ProtocolTimer requestUpdateTimer;
  void requestUpdateTimerExpiredEvent();

  ProtocolTimer periodicRequestTimer;
  void periodicRequestTimerExpiredEvent();

  virtual void getSinfoListReply(const std::list< Wsinfo > & wsinfoList);
  virtual void getFilteredSinfoListReply(const std::list< Wsinfo > & wsinfoList);

  void sinfoClientRequestMessageSlot(Message & message);

public:
  unsigned long delay_millisec;
  unsigned long timeout_millisec;
  std::string filterMarker;
  bool stopAfterOneRequest;

  SinfoRequester(ProtocolIO & protocolIO, SinfoData & _sinfoData);

  void connectionReadySlot();
  void receivedMessageSlot(Message & returnMessage);
  boost::signals2::signal<void ()> timeoutSignal;

  boost::signals2::signal<void (Message & message)> queueAndSendMessageSignal;
};


#endif // _sinforequester_h
