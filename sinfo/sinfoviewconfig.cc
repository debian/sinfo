#include <string>
#include <stdlib.h>
#include "sinfoviewconfig.h"
using namespace std;


SinfoViewConfig::SinfoViewConfig()
{
  quiet = false;
  char * lognamePtr=getenv("LOGNAME");
  if (NULL!=lognamePtr)
  {
    username=string(lognamePtr);
  }
  char * hostnamePtr=getenv("HOSTNAME");
  if (NULL!=hostnamePtr)
  {
    hostname = string(hostnamePtr);
  }
  listtop = -1;
  systeminfo = false;
  dispScale= SinfoViewConfig::log;
  ownprocesses = false;
  shownetload = false;
  showdiskload=false;

  std::list < std::string > ignoreList;
}

