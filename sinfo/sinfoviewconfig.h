#ifndef _sinfoviewconfig_h
#define _sinfoviewconfig_h


#include <list>
#include <string>
#include "sinfo.h"


class SinfoViewConfig
{
public:

  enum DispScale { log, lin, full };

  bool quiet;
  std::string username;
  std::string hostname;
  int listtop;
  bool systeminfo;
  DispScale dispScale;
  bool ownprocesses;
  bool shownetload;
  bool showdiskload;
  std::list < std::string > ignoreList;

public:
  SinfoViewConfig();
};


#endif // _sinfoviewconfig_h
