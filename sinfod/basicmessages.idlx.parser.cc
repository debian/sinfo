/*******************************************************************************
** File gererated from basicmessages.idlx
**
** Created: Tue Dec 16 15:03:43 2014
**      by: idlxcompiler version 0.0.1
**
** WARNING!
** All changes made in this file will be lost when recompiling idlx file.
*******************************************************************************/

#include <boost/bind.hpp>
#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include "messagestlcontainertypes.h"
using namespace Msg;
#include "templateparser.h"
#include "basicmessages.idlx.parser.h"
using namespace std;


enum
{
  GetSoftwareDescriptionRequest=1,
  GetSoftwareDescriptionReply=2,
  GetSoftwareVersionRequest=3,
  GetSoftwareVersionReply=4,
  GetCompileDateRequest=5,
  GetCompileDateReply=6
};

BasicmessagesParser::BasicmessagesParser(boost::shared_ptr<BasicmessagesWorker> basicmessagesWorkerPtr)
{
  TemplateParser_1ret_0< std::string > * getSoftwareDescriptionParserPtr = new TemplateParser_1ret_0< std::string >;
  getSoftwareDescriptionParserPtr->callWorkerSignal.connect(boost::bind(&BasicmessagesWorker::getSoftwareDescription, basicmessagesWorkerPtr, _1));
  addParser(GetSoftwareDescriptionRequest, GetSoftwareDescriptionReply, Parser::SPtr(getSoftwareDescriptionParserPtr));

  TemplateParser_1ret_0< std::string > * getSoftwareVersionParserPtr = new TemplateParser_1ret_0< std::string >;
  getSoftwareVersionParserPtr->callWorkerSignal.connect(boost::bind(&BasicmessagesWorker::getSoftwareVersion, basicmessagesWorkerPtr, _1));
  addParser(GetSoftwareVersionRequest, GetSoftwareVersionReply, Parser::SPtr(getSoftwareVersionParserPtr));

  TemplateParser_1ret_0< std::string > * getCompileDateParserPtr = new TemplateParser_1ret_0< std::string >;
  getCompileDateParserPtr->callWorkerSignal.connect(boost::bind(&BasicmessagesWorker::getCompileDate, basicmessagesWorkerPtr, _1));
  addParser(GetCompileDateRequest, GetCompileDateReply, Parser::SPtr(getCompileDateParserPtr));

};

