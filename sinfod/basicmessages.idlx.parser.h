#ifndef _basicmessages_idlx_parser_h
#define _basicmessages_idlx_parser_h

/*******************************************************************************
** File gererated from basicmessages.idlx
**
** Created: Tue Dec 16 15:03:43 2014
**      by: idlxcompiler version 0.0.1
**
** WARNING!
** All changes made in this file will be lost when recompiling idlx file.
*******************************************************************************/

#include "basicmessagesworker.h"
#include "parser.h"

class BasicmessagesParser: public CompositeParser
{
public:
  BasicmessagesParser(boost::shared_ptr<BasicmessagesWorker> basicmessagesWorkerPtr);
};

#endif
