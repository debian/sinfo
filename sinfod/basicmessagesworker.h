#ifndef _basicmessagesworker_h
#define _basicmessagesworker_h


#include <string>


class BasicmessagesWorker
{
public:
  void getSoftwareDescription(std::string & returnstring)
  {
    returnstring=std::string(PACKAGE_NAME);
  }


  void getSoftwareVersion(std::string & returnstring)
  {
    returnstring=std::string(PACKAGE_VERSION);
  }


  void getCompileDate(std::string & returnstring)
  {
    returnstring=std::string( __DATE__ " " __TIME__ );
  }
};


#endif // _basicmessagesworker_h
