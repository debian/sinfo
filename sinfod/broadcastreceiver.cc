#include <boost/bind.hpp>
#include <iostream>
#include <netdb.h>
#include <arpa/inet.h>
#include "broadcastreceiver.h"
#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include "messagesinfotypes.h"
#include "messagestlcontainertypes.h"
#include "sinfo.h"
#include "sinfobroadcastids.h"
using namespace std;
using namespace Msg;


BroadcastReceiver::BroadcastReceiver(boost::asio::io_service& io_service, bool & _restartCounterEventFlag, std::list < Wsinfo > & _wsinfoList)
  :ioservice(io_service),
   sock(io_service, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), SINFO_BROADCAST_PORT) ), restartCounterEventFlag(_restartCounterEventFlag), wsinfoList(_wsinfoList)
// FIXME not finished
// #ifdef ENABLEIPv6
//    sock(io_service, asio::ip::udp::endpoint(asio::ip::udp::v6(), SINFO_BROADCAST_PORT) )
//#else
//    sock(io_service, asio::ip::udp::endpoint(asio::ip::udp::v4(), SINFO_BROADCAST_PORT) )
//#endif
{
  sock.async_receive_from( boost::asio::buffer(data, max_length), sender_endpoint,
                           boost::bind(&BroadcastReceiver::handle_receive_from, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

}


void BroadcastReceiver::receiveMessage(std::string inetaddrString, unsigned long inetaddrHostByteOrder, Message & message)
{
  cout << "BroadcastReceiver::receive" << endl;

  time_t now= time(NULL);

  std::list < Wsinfo > ::iterator it;
  Wsinfo * wsinfo = 0;
  for (it = wsinfoList.begin();
       it != wsinfoList.end();
       ++it)
  {
    if (it->inetaddr == inetaddrString)
    {
      wsinfo = &*it;
      break;
    }
  }
  cout << "BroadcastReceiver::receive after search" << endl;

  // insert new entry in list
  if (0 == wsinfo)
  {
    Wsinfo newWsinfo;
    wsinfoList.push_front(newWsinfo);

    it = wsinfoList.begin();
    wsinfo = &*it;

    wsinfo->inetaddr = inetaddrString;
    wsinfo->name_time=0;
  }

  // name lookup every 30 minutes
  if (now -wsinfo->name_time > 30*60)
  {
    // determine hostname
    in_addr inaddr;
    inaddr.s_addr=htonl(inetaddrHostByteOrder);
    hostent * hostentry = gethostbyaddr((const char *) & inaddr, 4, AF_INET);
    if (0 != hostentry)
    {
      wsinfo->name=hostentry->h_name;
    }
    else
    {
      wsinfo->name=inetaddrString;
    }
    wsinfo->name_time=now;
  }


  wsinfo->lastheard = now;

  cout << "BroadcastReceiver::receive start while" << endl;

  while (message.size() > 0)
  {
    uint8 messageID;
    popFrontuint8(message,messageID);

    switch (messageID)
    {
    case restartFlag:
    {
      std::cout << "restartFlag received from "
                << inetaddrString
                << "(" << wsinfo->name << ")"
                << std::endl;
      restartCounterEventFlag=true;
    }
    break;

    case sigtermFlag:
    {
      std::cout << "sigtermFlag received from "
                << inetaddrString
                << "(" << wsinfo->name << ")"
                << std::endl;

      // zu letzt geh�rte Zeit auf Nul setzen!
      wsinfo->lastheard = 0;
    }
    break;

    case meminfoFlag:
      popFrontMeminfo(message,wsinfo->meminfo);
      break;

    case loadavgFlag:
      popFrontLoadavg(message,wsinfo->loadavg);
      break;

    case cpustatFlag:
      popFrontCpustat(message,wsinfo->cpustat);
      break;

    case netloadFlag:
      popFrontNetload(message,wsinfo->netload);
      break;

    case diskloadFlag:
      popFrontDiskload(message,wsinfo->diskload);
      break;

    case cpuinfoFlag:
      popFrontCpuinfo(message,wsinfo->cpuinfo);
      break;

    case unameinfoFlag:
      popFrontUnameinfo(message,wsinfo->unameinfo);
      break;

    case uptimeFlag:
      popFrontUptime(message,wsinfo->uptime);
      break;

    case usersFlag:
      popFrontUsers(message,wsinfo->users);
      break;

    case procinfoFlag:
    {
      popFront(message,wsinfo->procinfoList);
    }
    break;

    case markerFlag:
      popFrontstring(message,wsinfo->marker);
      break;

    default:
    {
      std::cerr << "bad frame-format from " << wsinfo->name << "!" << std::endl
                << "   Possible conflict on UDP-Port " << SINFO_BROADCAST_PORT << std::endl
                << "   or update of sinfod necessary!" << std::endl;
    }
    break;

    } // switch
  } // while

  if (0==wsinfo->lastheard)
  {
    wsinfoList.erase(it);
  }
}


void BroadcastReceiver::handle_receive_from(const boost::system::error_code& err, size_t length)
{
  if (err)
  {
    std::cout << "receive error: " << err.message() << std::endl;
  }
  else
  {
    cout << "BroadcastReceiver::handle_receive_from " << length << " bytes" << endl;

    string inetaddrString=sender_endpoint.address().to_string();
    unsigned long inetaddrHostByteOrder=sender_endpoint.address().to_v4().to_ulong();

    Message message(length, data);
    try
    {
      receiveMessage(inetaddrString, inetaddrHostByteOrder,  message);
    }
    catch (MessageException ex)
    {
      cout << "MessageException caught: " << ex.what() << endl;
    }
  }
  sock.async_receive_from( boost::asio::buffer(data, max_length), sender_endpoint,
                           boost::bind(&BroadcastReceiver::handle_receive_from, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

