#ifndef _broadcastreceiver_h
#define _broadcastreceiver_h

#include <boost/asio.hpp>
#include "message.h"
#include "sinfo.h"


class BroadcastReceiver
{
private:
  enum { max_length = 65535 }; // maximum UDP packet size
  char data[max_length];
  boost::asio::ip::udp::endpoint sender_endpoint;

  boost::asio::io_service & ioservice;
  boost::asio::ip::udp::socket sock;

  bool & restartCounterEventFlag;
  std::list < Wsinfo > & wsinfoList;

  void  receiveMessage(std::string inetaddrString, unsigned long inetaddrHostByteOrder, Message & message);

public:
  BroadcastReceiver(boost::asio::io_service& io_service, bool & _restartCounterEventFlag, std::list < Wsinfo > & _wsinfoList);
  void handle_receive_from(const boost::system::error_code& err, size_t length);
};


#endif // _broadcastreceiver_h
