#include <boost/bind.hpp>
#include <iostream>
#include "cleanuplist.h"
using namespace std;



// delete workstations from sinfod's buffer if they were not heard for 300 seconds
const long deleteoldTimer = 300;



CleanupList::CleanupList(boost::asio::io_service& io_service, std::list < Wsinfo > & _wsinfoList):ioservice(io_service), timer(io_service), wsinfoList(_wsinfoList)
{
  timer.expires_from_now(boost::posix_time::seconds(deleteoldTimer));
  timer.async_wait(boost::bind(&CleanupList::handle_timeout, this));
}


void CleanupList::timerEvent()
{
  // deleteOldEntries
  time_t now = time(0);
  std::list < Wsinfo > ::iterator it = wsinfoList.begin();
  while (it != wsinfoList.end())
  {
    if ((now - it->lastheard) >= deleteoldTimer)
    {
      it = wsinfoList.erase(it);
    }
    else
    {
      ++it;
    }
  }
}


void CleanupList::handle_timeout()
{
  //cout << "CleanupList::handle_timeout" << endl;
  timerEvent();

  timer.expires_from_now(boost::posix_time::seconds(deleteoldTimer));
  timer.async_wait(boost::bind(&CleanupList::handle_timeout, this));
}
