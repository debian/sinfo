#ifndef _cleanuplist_h
#define _cleanuplist_h


#include <boost/asio.hpp>
#include "sinfo.h"


class CleanupList
{
private:
  boost::asio::io_service & ioservice;
  boost::asio::deadline_timer timer;

  std::list < Wsinfo > & wsinfoList;

  void timerEvent();
  void handle_timeout();


public:
  CleanupList(boost::asio::io_service& io_service, std::list < Wsinfo > & _wsinfoList);

};


#endif // _cleanuplist_h
