/// demoninit.cc ///////////////////////////////////////////////////////////////

#include <iostream>
#include <signal.h>
#include <unistd.h> // location of daemon() on linux
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h> // location of daemon() on FreeBSD


// Detach ourself from the controlling terminal.
void demoninit()
{
#ifdef HAVE_DAEMON
  if (daemon(0, 0) == -1)
  {
    raise(SIGTERM);
  }
  umask(0);

#else
  int pid = 0;

  pid = fork();
  if (pid == -1)
    cout << "Cannot fork() to detach(1)";
  else if (pid)
    exit(0);

  // Close fds
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
#endif
}


/// EOF ////////////////////////////////////////////////////////////////////////
