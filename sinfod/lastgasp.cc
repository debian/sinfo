#include <signal.h>
#include <iostream>
#include "lastgasp.h"
#include "messagebasictypes.h"
#include "sinfobroadcastids.h"
using namespace std;
using namespace Msg;


LastGasp * LastGaspPtr=0; // FIXME: bad global


// FIXME implement as method?
void term_handler(int signal)
{
  Message message;
  pushFrontuint8(message,sigtermFlag);

  cout << "term_handler " << message.size() << "bytes" << endl;
  if (LastGaspPtr)
  {
    LastGaspPtr->sendMessageSignal(message);
  }
  exit(0);
}


LastGasp::LastGasp()
{
  // FIXME save old handler
  signal(SIGTERM, term_handler);
  signal(SIGQUIT, term_handler);
  signal(SIGINT, term_handler);
  LastGaspPtr=this; // FIXME: bad global
}


LastGasp::~LastGasp()
{
  LastGaspPtr=0; // FIXME: bad global
}
