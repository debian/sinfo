#ifndef _lastgasp_h
#define _lastgasp_h


#include <boost/signals2.hpp>
#include "message.h"


class LastGasp
{
public:
  LastGasp();
  ~LastGasp();

  boost::signals2::signal<void (Message&)> sendMessageSignal;
};


#endif // _lastgasp_h
