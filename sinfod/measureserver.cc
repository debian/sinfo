#include <boost/bind.hpp>
#include <iostream>
#include "messagebasictypes.h"
#include "messagecomplextypes.h"
#include "messagesinfotypes.h"
#include "messagestlcontainertypes.h"
#include "measureserver.h"
#include "meminfometer.h"
#include "loadavgmeter.h"
#include "cpuinfometer.h"
#include "unamemeter.h"
#include "uptimemeter.h"
#include "usersmeter.h"
#include "sinfobroadcastids.h"
using namespace std;
using namespace Msg;


// constants for the broadcasting timing
////////////////////////////////////////


// if sinfo produces to much net load for your purposes,
// you may twiddle with the following factors.


// don't send informations to sinfo if the computer was last heard
// for the last 25 seconds
// FIXME ? implement this again?
// const unsigned long dropoldTimer = 25;


// check the cpu-load every second....
// if the load has not changed (less 3%),
// broadcast every 10 seconds otherwise broadcast every second.
// if the CPU load has changed much (groeter 30%) trigger the
// sending of the process informations.
const unsigned long cpustatTimer = 1;
const unsigned long cpustatTimerMax = 10;
const float cpustatTimerFastThr = 0.05;      // fastBcast threshold in percentage of idle time
const float cpustatTimerProcinfoThr = 0.3;   // sendProcinfo threshold in percentage of idle time

const unsigned long loadavgTimer = 10;
const unsigned long uptimeTimer = 10;
const unsigned long procinfoTimer = 10;
const unsigned long netloadTimer = 10;
const unsigned long diskloadTimer = 10;
const unsigned long meminfoTimer = 30;
const unsigned long usersTimer = 30;
const unsigned long unamestrTimer = 900;
const unsigned long cpuinfoTimer = 60;
const unsigned long markerTimer = 900;

// if we received a restart flag we will send out ALL informations
// several times with a broadcasting intervall of 10 seconds.
const unsigned long restartTimer = 10;


MeasureServer::MeasureServer(boost::asio::io_service& io_service, const std::string & networkcard, const std::string & _marker, bool cmdlinemode, std::list < std::string > ignoreList, long listtop):ioservice(io_service), timer(io_service), procinfoMeter(cmdlinemode,ignoreList), listtop(listtop),  netloadMeter(networkcard), marker(_marker)
{
  timeCounter = 0;
  procinfonow = false;
  sendRestartFlag = true;
  lastidlepercent = 0.;

  timer.expires_from_now(boost::posix_time::milliseconds(500));
  timer.async_wait(boost::bind(&MeasureServer::handle_timeout, this));

  restartcounter=0;
}


MeasureServer::~MeasureServer()
{
}


void MeasureServer::timerEvent()
{
  Message message;

  if (0 == (timeCounter%meminfoTimer))
  {
    Meminfo meminfo;
    if (getMeminfo(meminfo))
    {
      pushFrontMeminfo(message, meminfo);
      pushFrontuint8(message, meminfoFlag);
    }
  }

  if (0 == (timeCounter%loadavgTimer))
  {
    Loadavg loadavg;
    if (getLoadavg(loadavg))
    {
      pushFrontLoadavg(message, loadavg);
      pushFrontuint8(message, loadavgFlag);
    }
  }

  if (0 == (timeCounter%cpustatTimer))
  {
    Cpustat cpustat;
    if (cpustatMeter.getCpustat(cpustat))
    {
      if ((0 == timeCounter)
          || (fabs(lastidlepercent - cpustat.idlep) >= cpustatTimerFastThr)
          || (0==(timeCounter%cpustatTimerMax)) )
      {

        if (fabs(lastidlepercent - cpustat.idlep) >= cpustatTimerProcinfoThr)
          procinfonow = true;

        pushFrontCpustat(message, cpustat);
        pushFrontuint8(message, cpustatFlag);

        lastidlepercent = cpustat.idlep;
      }
    }
  }

  if (0 == (timeCounter%cpuinfoTimer))
  {
    Cpuinfo cpuinfo;
    if (getCpuinfo(cpuinfo))
    {
      pushFrontCpuinfo(message, cpuinfo);
      pushFrontuint8(message, cpuinfoFlag);
    }
  }
  if (0 == (timeCounter%markerTimer))
  {
    pushFrontstring(message, marker);
    pushFrontuint8(message, markerFlag);
  }

  if (0 == (timeCounter%netloadTimer))
  {
    Netload netload;
    if (netloadMeter.getNetload(netload))
    {
      pushFrontNetload(message, netload);
      pushFrontuint8(message, netloadFlag);
    }
  }

  if (0 == (timeCounter%diskloadTimer))
  {
    Diskload diskload;
    if (diskloadMeter.getDiskload(diskload))
    {
      pushFrontDiskload(message, diskload);
      pushFrontuint8(message, diskloadFlag);
    }
  }

  if (0 == (timeCounter%unamestrTimer))
  {
    Unameinfo unameinfo = getUnameinfo();

    pushFrontUnameinfo(message, unameinfo);
    pushFrontuint8(message, unameinfoFlag);
  }

  if (0 == (timeCounter%uptimeTimer))
  {
    Uptime uptime;
    if (getUptime(uptime))
    {
      pushFrontUptime(message, uptime);
      pushFrontuint8(message, uptimeFlag);
    }
  }

  if (0 == (timeCounter%usersTimer))
  {
    Users users = getUsers();

    pushFrontUsers(message, users);
    pushFrontuint8(message, usersFlag);
  }

  if ( (0 == (timeCounter%procinfoTimer)) || (procinfonow) )
  {
    ProcinfoList procinfoList;
    if (procinfoMeter.getTopList(listtop, procinfoList))
    {
      pushFront(message, procinfoList);
      pushFrontuint8(message, procinfoFlag);
    }
    procinfonow = false;
  }

  if (sendRestartFlag)
  {
    pushFrontuint8(message, restartFlag);
    sendRestartFlag = false;
  }

  if (message.size()>0)
  {
    // try-catch to avoid stop of program when udp port is not available due to
    // reconfiguration (e.g. ipatbles)
    try
    {
      cout << "MeasureServer::timerEvent sending " << message.size() << "bytes" << endl;
      sendMeasurementSignal(message);
    }
    catch (std::exception& e)
    {
      cerr << "Exception: " << e.what() << endl;
    }
  }

  timeCounter++;

  if ( (restartcounter>0) &&  (0 == (timeCounter%restartTimer)) )
  {
    timeCounter = 0;
    restartcounter--;
  }
}


void MeasureServer::restartCounterEvent()
{
  restartcounter=3;
}


void MeasureServer::handle_timeout()
{
  cout << "MeasureServer::handle_timeout" << endl;
  timerEvent();

  timer.expires_from_now(boost::posix_time::milliseconds(1000));
  timer.async_wait(boost::bind(&MeasureServer::handle_timeout, this));
}

