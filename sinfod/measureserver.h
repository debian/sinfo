#ifndef _measureserver_h
#define _measureserver_h


#include <boost/asio.hpp>
#include "cpustatmeter.h"
#include "netloadmeter.h"
#include "diskloadmeter.h"
#include "procinfometer.h"
#include <boost/signals2.hpp>


class MeasureServer
{
private:
  boost::asio::io_service & ioservice;
  boost::asio::deadline_timer timer;

  unsigned long timeCounter;

  bool sendRestartFlag;

  ProcinfoMeter procinfoMeter;
  bool procinfonow;                 // flag that is set to send process infos if the load has changed dramatically
  long listtop;

  CpustatMeter cpustatMeter;
  float lastidlepercent;

  NetloadMeter netloadMeter;
  DiskloadMeter diskloadMeter;

  std::string marker;
  int restartcounter;


public:
  MeasureServer(boost::asio::io_service& io_service, const std::string & networkcard, const std::string & _marker, bool cmdlinemode, std::list < std::string > ignoreList, long listtop);
  ~MeasureServer();

  void timerEvent();
  void restartCounterEvent();

  boost::signals2::signal<void (Message&)> sendMeasurementSignal;

  void handle_timeout(); // asio part
};


#endif // _measureserver_h
