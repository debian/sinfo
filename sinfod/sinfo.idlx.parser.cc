/*******************************************************************************
** File gererated from sinfo.idlx
**
** Created: Tue Dec 16 15:03:43 2014
**      by: idlxcompiler version 0.0.1
**
** WARNING!
** All changes made in this file will be lost when recompiling idlx file.
*******************************************************************************/

#include <boost/bind.hpp>
#include "messagebasictypes.h"
#include "messagesinfotypes.h"
#include "messagecomplextypes.h"
#include "messagestlcontainertypes.h"
using namespace Msg;
#include "templateparser.h"
#include "sinfo.idlx.parser.h"
using namespace std;


enum
{
  GetSinfoListRequest=1,
  GetSinfoListReply=2,
  GetFilteredSinfoListRequest=3,
  GetFilteredSinfoListReply=4
};

SinfoParser::SinfoParser(boost::shared_ptr<SinfoWorker> sinfoWorkerPtr)
{
  TemplateParser_1ret_0< std::list < Wsinfo > > * getSinfoListParserPtr = new TemplateParser_1ret_0< std::list < Wsinfo > >;
  getSinfoListParserPtr->callWorkerSignal.connect(boost::bind(&SinfoWorker::getSinfoList, sinfoWorkerPtr, _1));
  addParser(GetSinfoListRequest, GetSinfoListReply, Parser::SPtr(getSinfoListParserPtr));

  TemplateParser_1ret_1< std::list < Wsinfo >, std::string > * getFilteredSinfoListParserPtr = new TemplateParser_1ret_1< std::list < Wsinfo >, std::string >;
  getFilteredSinfoListParserPtr->callWorkerSignal.connect(boost::bind(&SinfoWorker::getFilteredSinfoList, sinfoWorkerPtr, _1, _2));
  addParser(GetFilteredSinfoListRequest, GetFilteredSinfoListReply, Parser::SPtr(getFilteredSinfoListParserPtr));

};

