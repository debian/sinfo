#ifndef _sinfo_idlx_parser_h
#define _sinfo_idlx_parser_h

/*******************************************************************************
** File gererated from sinfo.idlx
**
** Created: Tue Dec 16 15:03:43 2014
**      by: idlxcompiler version 0.0.1
**
** WARNING!
** All changes made in this file will be lost when recompiling idlx file.
*******************************************************************************/

#include "sinfoworker.h"
#include "parser.h"

class SinfoParser: public CompositeParser
{
public:
  SinfoParser(boost::shared_ptr<SinfoWorker> sinfoWorkerPtr);
};

#endif
