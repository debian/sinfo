#ifndef _sinfobroadcastids_h
#define _sinfobroadcastids_h


enum SinfoBroadcastIDs
{ restartFlag = 1,
  meminfoFlag,
  loadavgFlag,
  cpustatFlag,
  uptimeFlag,
  procinfoFlag,
  unameinfoFlag,
  usersFlag,
  cpuinfoFlag,
  netloadFlag,
  sigtermFlag,
  diskloadFlag,
  markerFlag
};


#endif // _sinfobroadcastids_h
