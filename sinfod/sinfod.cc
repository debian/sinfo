#include <boost/asio.hpp>
#include <getopt.h>
#include <sys/wait.h>
#include <boost/bind.hpp>
#include <iostream>
#include "basicmessages.idlx.parser.h"
#include "sinfoworker.h"
#include "sinfo.idlx.parser.h"
#include "udpmessageserver.h"
#include "tcpmessageserver.h"
#include "cleanuplist.h"
#include "measureserver.h"
#include "udpmessagetransmitter.h"
#include "broadcastreceiver.h"
#include "udpmessagetransmitter.h"
#include "demoninit.h"
#include "sinfo.h"
#include "lastgasp.h"
#include "rpcserverconnectorfactory.h"
#include "parser.h"
using namespace std;


class MainClass
{
private:
  bool broadcastonlymode;
  bool cmdlinemode;
  long listtop;
  std::list < std::string > ignoreList;
  bool watchdogmode;
  string bcast_address;
  string networkcard;
  string marker;

  void mainLoopNoWatchdog();
  void mainLoopWithWatchdog();

  void printHelpAndExit(const char * plaint = "Usage:-");

public:
  MainClass(int argc, char * argv[]);

  int mainLoop();
};


MainClass::MainClass(int argc, char * argv[])
{
  bool demon = true;
  bool quiet = false;

  broadcastonlymode = false;
  cmdlinemode = false;
  listtop=5;
  watchdogmode = false;
  bcast_address=string("255.255.255.255");

  int nicevalue = 10; // start with nice 10 to ensure niceability in accidental cases.


#ifdef HAVE_GETOPT_LONG
  static const struct option long_options[] =
  {
    { "bcastaddress", required_argument, 0, 'b'},
    { "cmdline", no_argument, 0, 'c'},
    { "networkcard", required_argument, 0, 'n'},
    { "marker", required_argument, 0, 'm'},
    { "ignore", required_argument, 0, 'i' },
    { "top", required_argument, 0, 't' },
    { "nice", required_argument, 0, 'N' },
    { "foreground", no_argument, 0, 'F' },
    { "version", no_argument, 0, 'V' },
    { "quiet", no_argument, 0, 'q' },

    { "spymode", no_argument, 0, 's' },
    { "watchdog", no_argument, 0, 'W' },

    { "help", no_argument, 0, '?' },

    { 0, 0, 0, 0 }
  };
#endif


  int c;
#ifdef HAVE_GETOPT_LONG
  while ((c = getopt_long(argc, argv, "b:cB:VFi:t:n:N:qa:A:lsWh?", long_options, NULL)) != -1)
#else
  while ((c = getopt (argc, argv, "b:cB:VFi:t:n:N:qa:A:lsWh?")) != -1)
#endif
  {
    switch (c)
    {
    case 'b':
      bcast_address = string(optarg);
      break;
    case 'c':
      cmdlinemode = true;
      break;
    case 'n':
      networkcard = string(optarg);
      break;
    case 'm':
      marker = string(optarg);
      break;
    case 'i':
      ignoreList.push_back(string(optarg));
      break;
    case 't':
      listtop = atoi(optarg);
      break;
    case 'N':
      nicevalue = atoi(optarg);
      break;
    case 'F':
      demon = false;
      break;
    case 'q':
      quiet = true;
      break;
    case 'V':
      cout << argv[0] << " " << VERSION
           << "  (compiled " __DATE__ " " __TIME__ ")"
           << endl;
      exit(0);
      break;
    case 's':
      broadcastonlymode = true;
      break;
    case 'W':
      watchdogmode = true;
      break;
    case '?':
    case 'h':
      printHelpAndExit();
      break;

    default:
      printHelpAndExit("No such option");
    }
  }

  if (false == quiet)
    cout << argv[0] << " " VERSION "  (compiled " __DATE__ " " __TIME__ ")" << endl;


  nice(nicevalue);

  if (demon)
    demoninit();
}


void MainClass::printHelpAndExit(const char * plaint)
{
  cout << plaint << endl
       << endl
       << "sinfod [-F][-q][-V] [-b #.#.#.#] [-c] [-n card] [-N nicevalue ] [ -m marker ] [-i process] [-s] [-W]" << endl
       << endl
       << "OPTIONS (for full descriptions see \"man sinfod\")" << endl
       << "-F/--foreground" << endl
       << "  Do not detach from the terminal." << endl
       << "-q/--quiet" << endl
       << "  be quiet - don't display startup informations." << endl
       << "-V/--version" << endl
       << "  Print the version number and exit." << endl
       << "-b #.#.#.#/--bcastaddress=#.#.#.#" << endl
       << "  Set  broadcast address of sinfod." << endl
       << "-c/--cmdline" << endl
       << "  broadcast the full command line of a process instead of the command only." << endl
       << "-n card/--networkcard=card" << endl
       << "  Determine network load fron card." << endl
       << "-N nicevalue/--nice=nicevalue" << endl
       << "  Set the priority of this demon to nicevalue." << endl
       << "-m marker/--marker=marker" << endl
       << "  Mark the machine with a name." << endl
       << "-i/--ignore <process>" << endl
       << "  Don't broadcast any information on <process> ; --ignore may be set multiple times." << endl
       << "-t/--top <count>" << endl
       << "  Broadcast information on the top <count> processes; default: 5" << endl
       << "-s/--spymode" << endl
       << "  Deactivate TCP interface, broadcast only mode." << endl
       << "-W/--watchdog" << endl
       << "  start with simple watchdog." << endl
       << endl;
  exit(1);
}


void MainClass::mainLoopNoWatchdog()
{
  try
  {
    boost::asio::io_service ioservice;
    list < Wsinfo > wsinfoList;

    if (true==broadcastonlymode)
    {
      UDPMessageTransmitter udpMessageTransmitter(ioservice, boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(bcast_address), SINFO_BROADCAST_PORT));

      MeasureServer measureServer(ioservice, networkcard, marker, cmdlinemode, ignoreList, listtop);
      measureServer.sendMeasurementSignal.connect(boost::bind(&UDPMessageTransmitter::send, &udpMessageTransmitter, _1));

      LastGasp lastGasp;
      lastGasp.sendMessageSignal.connect(boost::bind(&UDPMessageTransmitter::send, &udpMessageTransmitter, _1));

      ioservice.run();
    }
    else
    {
      bool restartCounterEventFlag=true;

      // create worker
      boost::shared_ptr<BasicmessagesWorker> basicmessagesWorkerPtr=boost::shared_ptr<BasicmessagesWorker>(new BasicmessagesWorker);
      boost::shared_ptr<SinfoWorker> sinfoWorkerPtr=boost::shared_ptr<SinfoWorker>(new SinfoWorker(wsinfoList));

      // create parser tree
      CompositeParser topLevelParser;
      topLevelParser.addParser(1,1,Parser::SPtr(new BasicmessagesParser(basicmessagesWorkerPtr)));
      topLevelParser.addParser(2,2,Parser::SPtr(new SinfoParser(sinfoWorkerPtr)));


      UDPMessageServer udpMessageServerIPv4(ioservice,boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), SINFO_REQUEST_PORT));
      udpMessageServerIPv4.receiveMessageSignal.connect(boost::bind(&CompositeParser::parse, &topLevelParser, _1, _2));
#ifdef ENABLEIPv6
      UDPMessageServer udpMessageServerIPv6(ioservice,boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v6(), SINFO_REQUEST_PORT));
      udpMessageServerIPv6.receiveMessageSignal.connect(boost::bind(&CompositeParser::parse, &topLevelParser, _1, _2));
#endif

      RPCServerConnectorFactory rpcServerConnectorFactory;
      rpcServerConnectorFactory.receiveMessageSignal.connect(boost::bind(&CompositeParser::parse, &topLevelParser, _1, _2));
      TCPMessageServer tcpMessageServerIPv4(ioservice, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), SINFO_REQUEST_PORT), rpcServerConnectorFactory);
#ifdef ENABLEIPv6
      TCPMessageServer tcpMessageServerIPv6(ioservice, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), SINFO_REQUEST_PORT), rpcServerConnectorFactory);
#endif
      UDPMessageTransmitter udpMessageTransmitter(ioservice, boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(bcast_address), SINFO_BROADCAST_PORT));

      MeasureServer measureServer(ioservice, networkcard, marker, cmdlinemode, ignoreList, listtop);
      measureServer.sendMeasurementSignal.connect(boost::bind(&UDPMessageTransmitter::send, &udpMessageTransmitter, _1));

      LastGasp lastGasp;
      lastGasp.sendMessageSignal.connect(boost::bind(&UDPMessageTransmitter::send, &udpMessageTransmitter, _1));

      CleanupList cleanupList(ioservice, wsinfoList);

      BroadcastReceiver broadcastReceiver(ioservice, restartCounterEventFlag, wsinfoList);
      while (ioservice.run_one())
      {
        // while (messagepassingEventQueue.size()>0)
        // {
        //   deliverEvent(messagepassingEventQueue.pop());
        // }
        // FIXME : restartCounterEventFlag is much better than a global variable,
        // but the Event passing should be more generic and the classes shoud register a callback by themselve
        // e.g. by using an observer design pattern
        if (restartCounterEventFlag)
        {
          measureServer.restartCounterEvent();
          restartCounterEventFlag=false;
        }
      }
    }

    cout << "ioservice run exit" << endl;
  }
  catch (exception& e)
  {
    cerr << "Exception: " << e.what() << "\n";
  }
}


void MainClass::mainLoopWithWatchdog()
{
  while (1)
  {

    pid_t childpid = fork();
    cout << childpid << endl;
    if ( -1 == childpid)
    {
      cerr << "error forking child process" << endl;
      exit(1);
    }

    if (0 == childpid)
    {
      mainLoopNoWatchdog();
    }

    if (childpid > 0)
    {
      int status;
      waitpid(childpid, &status, 0);
    }

    sleep(1);
  }
}


int MainClass::mainLoop()
{
  // writing to a bad socket will abort the porgram if we dont ignore SIGPIPE
  signal(SIGPIPE, SIG_IGN);

  if (true==watchdogmode)
  {
    mainLoopWithWatchdog();
  }
  else
  {
    mainLoopNoWatchdog();
  }
  return 0;
}


int main(int argc, char * argv[])
{
  MainClass mainClass(argc,argv);

  return mainClass.mainLoop();
}
