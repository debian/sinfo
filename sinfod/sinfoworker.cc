#include "sinfoworker.h"
using namespace std;


SinfoWorker::SinfoWorker(std::list < Wsinfo > & wsinfoList) : wsinfoList(wsinfoList)
{
}


void SinfoWorker::getSinfoList(std::list < Wsinfo > & returnWsinfoList) const
{
  returnWsinfoList=wsinfoList; // this is a copy to returnWsinfoList
}


void SinfoWorker::getFilteredSinfoList( std::list < Wsinfo > & returnWsinfoList, const string & filterMarker) const
{
  for (std::list < Wsinfo > ::iterator wit = wsinfoList.begin();
       wit != wsinfoList.end();
       ++wit)
  {
    if ((*wit).marker == filterMarker)
    {
      returnWsinfoList.push_back(*wit);
    }
  }
}
