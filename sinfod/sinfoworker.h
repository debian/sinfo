#ifndef _sinfoworker_h
#define _sinfoworker_h

#include <string>
#include <list>
#include "sinfo.h"


class SinfoWorker
{
private:
  std::list < Wsinfo > & wsinfoList;

public:
  SinfoWorker(std::list < Wsinfo > & wsinfoList);

  void getSinfoList( std::list < Wsinfo > & returnWsinfoList) const;
  void getFilteredSinfoList( std::list < Wsinfo > & returnWsinfoList, const std::string & filterMarker) const;
};


#endif // _sinfoworker_h
